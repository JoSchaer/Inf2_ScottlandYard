import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import gameboard.GameBoard;
import gameboard.Knoten;
import regelwerk.Regelwerk;
import spieler.Detektiv;
import spieler.MrX;
import spieler.Spieler;
import spielleiter.Spielleiter;
import zusatsKlassen.Ringpuffer;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

public class Test_Regelwerk {
	
	static GameBoard game = new GameBoard();
	static ArrayList<String> spielernamen = new ArrayList<String>();
	static ArrayList<SpielerFarben> spielerFarben = new ArrayList<SpielerFarben>();
	Spielleiter spielleiter = new Spielleiter(6, spielernamen, spielerFarben, "Regelwerk", "GUI");
	Detektiv testSpieler = new Detektiv(SpielerFarben.BLAU, "Hans", 23);
	Regelwerk regeln = new Regelwerk((MrX)spielleiter.getRingpuffer().getMrX());
	static String[] namen = { "MrX", "Dete1", "Dete2", "Dete3", "Dete4", "Dete5" };
	static SpielerFarben[] alleFarben = SpielerFarben.values();
	static ArrayList<Knoten> besetzteKnoten = new ArrayList<Knoten>();
	static int[] knoten = { 23, 55, 60, 34, 12, 40 };

	/**
	 * Fuellt Arraylist mit Werten fuer spielernamen und farben
	 */
	@BeforeClass
	public static void befuellen() {
		for (int i = 0; i < namen.length - 1; i++) {
			spielernamen.add(namen[i]);
		}
		for (int j = 0; j < alleFarben.length - 1; j++) {
			spielerFarben.add(alleFarben[j]);
		}
		for (int k = 0; k < knoten.length - 1; k++) {
			besetzteKnoten.add(game.sucheKnotenMitID(knoten[k]));
		}
	}

	/**
	 * Testet ob ein Spieler fahren kann, da der Wert mit false initialisiert wird
	 * wird beim Test nach true gesucht um zu schauen dass alles Moeglichkeiten
	 * durchlaufen werden
	 */
	@Test
	public void testkannFahren() {
		assertTrue(regeln.kannFahren(testSpieler, game.sucheKnotenMitID(23), game.sucheKnotenMitID(12),
				VerkehrsFarben.WEISS));
	}

	/**
	 * Testet dass ein Spieler nicht fahren kann, zum Beispiel weil die beiden
	 * Knoten nicht verbunden sind
	 */
	@Test
	public void testkannFahrenfalse() {
		assertFalse(regeln.kannFahren(testSpieler, game.sucheKnotenMitID(23), game.sucheKnotenMitID(10),
				VerkehrsFarben.GRUEN));
	}

	@Test
	public void testkoennenAllFahren() {
		Ringpuffer ring = spielleiter.getRingpuffer();
		for (Spieler aktSpieler : ring.getAllSpieler()) {
			if (aktSpieler instanceof MrX) {
				while (aktSpieler.getBus().size() != 0) {
					aktSpieler.removeBus();
				}
				while (aktSpieler.getTaxi().size() != 0) {
					aktSpieler.removeTaxi();
				}
				while (aktSpieler.getuBahn().size() != 0) {
					aktSpieler.removeUBahn();
				}
				while (((MrX) aktSpieler).getBlackTickets().size() != 0) {
					((MrX) aktSpieler).removeBlackTicket();
				}
			} else {
				while (aktSpieler.getBus().size() != 0) {
					aktSpieler.removeBus();
				}
				while (aktSpieler.getTaxi().size() != 0) {
					aktSpieler.removeTaxi();
				}
				while (aktSpieler.getuBahn().size() != 0) {
					aktSpieler.removeUBahn();
				}
			}
		}
		assertTrue(regeln.koennenAllFahren(ring.getAllSpieler(), game.getAlleKnoten()));
	}

	@Test
	public void testkoennenAllFahrenfalse() {

		assertFalse(regeln.koennenAllFahren(spielleiter.getRingpuffer().getAllSpieler(), game.getAlleKnoten()));
	}

	@Test
	public void testfeldBelegt() {
		assertTrue(regeln.feldBelegt(spielleiter.getGameBoard().sucheKnotenMitID((spielleiter.getRingpuffer().getActualPlayer().getAktuellePosition()))));
	}

	
}
