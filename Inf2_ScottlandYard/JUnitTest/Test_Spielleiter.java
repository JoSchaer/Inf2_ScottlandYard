import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;

import regelwerk.Regelwerk;
import spielleiter.Spielleiter;
import zusatsKlassen.Ringpuffer;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

public class Test_Spielleiter {
	
	
	static ArrayList<String> spielernamen = new ArrayList<String>();
	static ArrayList<SpielerFarben> spielerfarben = new ArrayList<SpielerFarben>();
	
	static String[] namen = {"MrX","Dete1","Dete2","Dete3","Dete4","Dete5"};
	static SpielerFarben[] alleFarben = SpielerFarben.values();
	static Regelwerk regelwerk;
	
	Spielleiter leiter = new Spielleiter(5,spielernamen,spielerfarben, regelwerk.getClass().toString(), "GUI");
	
	/**
	 * bef�llt arraylisten die dem spielleiter �bergeben werden mit namen und Farben
	 */
	@BeforeClass
	public static void befuellen() {
		for(int i = 0; i< namen.length-1;i++) {
			spielernamen.add(namen[i]);
		}
		for(int j = 0; j< alleFarben.length-1; j++) {
			spielerfarben.add(alleFarben[j]);
		}	
	}
	
	/**
	 * Testet ob anzahl Spieler rictig �bergeen und gesetzt wurde
	 */
	@Test
	public void testGetAnzahlSpieler() {	
		assertTrue(leiter.getAnzahlSpieler() == 5  );
	}
	/**
	 * �berpr�ft ob richtige anzahl spieler im ringpuffer erstellt wurden
	 */
	@Test
	public void testRingpufferSpieler() {
		assertTrue(leiter.getRingpuffer().getSize()==5);
	}
	
	/**
	 * testet ob zugewiesene nummer am anfang eine der startnummern ist 
	 */
	@Test
	public void testSpielerStartnummer() {
		int[] startnummern = { 13, 26, 29, 34, 50, 53, 91, 94, 103, 112, 117, 132, 138, 141, 155, 174, 197, 198 };
		ArrayList<Integer> start = new ArrayList<Integer>();
		
		for(int i = 0; i < startnummern.length;i++) {
			start.add(startnummern[i]);
		}
		assertTrue(start.contains(leiter.getRingpuffer().getActualPlayer().getAktuellePosition()));
	}
	
	/**
	 * Teste MrX Taxikarten zu beginn
	 */
	@Test
	public void testTaxivorZug() {
		assertTrue(leiter.getRingpuffer().getMrX().getTaxi().size()==4);
	}
	
	/**
	 * testet ob MrX anhand seinen m�glichkeiten bewegt werden kann
	 */
	@Test
	public void testmoveMrX() {
		leiter.getRingpuffer().getMrX().setAktuellePosition(117);
		leiter.movePlayer(88, VerkehrsFarben.WEISS);
		assertTrue(leiter.getRingpuffer().getMrX().getAktuellePosition()==88);
	}
	
	/**
	 * Testet Kartenanzahl nach dem Zug 
	 */
	@Test
	public void testMrXTaxiKartenNachZug() { 
		leiter.getRingpuffer().getMrX().setAktuellePosition(117);
		leiter.movePlayer(88, VerkehrsFarben.WEISS);
		assertTrue(leiter.getRingpuffer().getMrX().getTaxi().size()==3);
	}
	
	
}
