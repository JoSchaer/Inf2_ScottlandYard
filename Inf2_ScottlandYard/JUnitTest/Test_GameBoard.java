import static org.junit.Assert.*;

import org.junit.Test;

import gameboard.GameBoard;
import gameboard.Knoten;

public class Test_GameBoard {
	GameBoard gameBoard = new GameBoard();
	Knoten testKnoten = new Knoten(555);
	Knoten testKnoten2 = new Knoten(234);

	/**
	 * Es wird getestet, ob alle 199 Knoten angelegt wurden, indem die Laenge, der
	 * Liste des Gameboards abgefragt wird.
	 */
	@Test
	public void alleKnotenErstellt() {
		assertTrue(gameBoard.getAlleKnoten().size() == 199);
	}

	/**
	 * Es wird der Knoten mit der ID 150 zur�ck gegeben.
	 */
	@Test
	public void getKnotenByIDTest() {
		assertNotNull(gameBoard.getAlleKnoten().get(150));
	}

	/**
	 * Es wird zweimal der Knoten mit der ID 234 als Nachbarknoten zu Knoten 555
	 * erstellt, dabei wird der Knoten 234 nur einmal in die Liste eingef�gt.
	 * Andersrum genau so.
	 */
	@Test
	public void testKnotenaufNachabar() {
		testKnoten.addKnotenBeiAddNachbarknoten(testKnoten2);
		testKnoten.addKnotenBeiAddNachbarknoten(testKnoten2);
		assertTrue(testKnoten.getNachbarknoten().size() == 1);
	}

	/**
	 * Es werden die Linienfarbe der ersten Knotens abgefragt mit dem Knoten 46. Diese m�ssen laut der
	 * CSV 2 bzw 4 sein.
	 */
	@Test
	public void anzLinienFarbeBeispielknoten() {
		Knoten k1 = gameBoard.sucheKnotenMitID(1);
		Knoten k2 = gameBoard.sucheKnotenMitID(46);
		assertTrue(k1.getMoeglicheFahrlinien(k2).size()==4);
	}

}
