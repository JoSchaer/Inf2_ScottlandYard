import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import gameboard.GameBoard;
import gameboard.Knoten;
import spieler.KI_MrX;
import spielleiter.Spielleiter;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

public class Test_KI_MrX {

	static ArrayList<String> spielernamen = new ArrayList<String>();
	static ArrayList<SpielerFarben> spielerFarben = new ArrayList<SpielerFarben>();
	Spielleiter spielleiter = new Spielleiter(6, spielernamen, spielerFarben, "Regelwerk_Erweiterung","GUI");
	KI_MrX kimrx = new KI_MrX(SpielerFarben.DURCHSICHTIG,"mrx", 5, 100, spielleiter);
	static GameBoard gameboard = new GameBoard();
	static String[] namen = { "MrX", "Dete1", "Dete2", "Dete3", "Dete4", "Dete5" };
	static SpielerFarben[] alleFarben = SpielerFarben.values();
	static ArrayList<Knoten> besetzteKnoten = new ArrayList<Knoten>();
	static int[] knoten = { 10, 11, 12, 15, 19, 9 };
	
	@BeforeClass
	public static void befuellen() {
		for (int i = 0; i < namen.length - 1; i++) {
			spielernamen.add(namen[i]);
		}
		for (int j = 0; j < alleFarben.length - 1; j++) {
			spielerFarben.add(alleFarben[j]);
		}
		for (int k = 0; k < knoten.length - 1; k++) {
			besetzteKnoten.add(gameboard.sucheKnotenMitID(knoten[k]));
		}
	}
	
	@Test
	public void testColor() {
		assertTrue(kimrx.whichColor(gameboard.sucheKnotenMitID(2), gameboard.sucheKnotenMitID(10)).equals(VerkehrsFarben.WEISS));
	}
	@Test
	public void testColorfalse() {
		assertFalse(kimrx.whichColor(gameboard.sucheKnotenMitID(2), gameboard.sucheKnotenMitID(10)).equals(VerkehrsFarben.SCHWARZ));
	}
	

}
