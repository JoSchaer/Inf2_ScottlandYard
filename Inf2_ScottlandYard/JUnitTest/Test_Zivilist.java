import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import regelwerk.Regelwerk_Erweiterung;
import spieler.MrX;
import spieler.Zivilist;
import spielleiter.Spielleiter;
import zusatsKlassen.SpielerFarben;

public class Test_Zivilist {

	static ArrayList<SpielerFarben> spielerFarben = new ArrayList<>();
	static ArrayList<String> spielernamen = new ArrayList<String>();
	Spielleiter spielleiter = new Spielleiter(6, spielernamen, spielerFarben, "Regelwerk_Erweiterung","GUI");
	Zivilist zivilist = new Zivilist(36, spielleiter);
	static String[] namen = {"Joni", "Dani", "Celina", "Eve" }; 
	static SpielerFarben[] alleFarben = SpielerFarben.values();
	Regelwerk_Erweiterung regeln = new Regelwerk_Erweiterung(mrx);
	static MrX mrx;
			
	@BeforeClass
	public static void befuellen() {
		for (int i = 0; i < namen.length - 1; i++) {
			spielernamen.add(namen[i]);
		}
		for (int j = 0; j < alleFarben.length - 1; j++) {
			spielerFarben.add(alleFarben[j]);
		}
	}
	
	@Test
	public void testHinweis2(){
		
		zivilist.setHinweis2(regeln.naechsteStation(11));
		assertTrue(zivilist.getHinweis2().contains("13"));
	}
	
	@Test
	public void testErzeugeHinweis() {
		zivilist.setManipuliert(true);
		zivilist.setHinweis1("SuedWest");
		assertFalse(zivilist.getHinweis1().equals("SuedWest"));
	}
	
	@Test
	public void testGetHinweis1() {
		assertNotNull(zivilist.getHinweis1());
	}
	
	@Test
	public void testIsManipuliert() {
		assertTrue(zivilist.isManipuliert() == false);
	}
	
	
	
	


}