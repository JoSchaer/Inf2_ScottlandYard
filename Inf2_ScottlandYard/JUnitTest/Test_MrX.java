import static org.junit.Assert.*;

import org.junit.Test;

import spieler.MrX;
import zusatsKlassen.SpielerFarben;

public class Test_MrX {

	MrX mrx = new MrX(SpielerFarben.DURCHSICHTIG, "MrX", 5, 145);
	
	/**
	 * Test ob Takxikarten korrekt hinzugefuegt werden
	 */
	@Test
	public void testTaxikarten() {
		assertTrue(mrx.getTaxi().size()==4);
	}
	/**
	 * Test ob Buskarten korrekt hinzugefuegt werden
	 */
	@Test
	public void testBuskarten() {
		assertTrue(mrx.getBus().size()==3);
	}
	/**
	 * Test ob UBahnikarten korrekt hinzugefuegt werden
	 */
	@Test
	public void testBahnkarten() {
		assertTrue(mrx.getuBahn().size()==3);
	}
	/**
	 * Test ob Blacktickets korrekt hinzugefuegt werden
	 */
	@Test
	public void testBlackTickets() {
		assertTrue(mrx.getBlackTickets().size()==5);
	}
	
	/**
	 * Test ob Methode getTaxiFromPlayer... funktioniert
	 */
	@Test
	public void testGetCardFromPlayer() {
		mrx.getTaxiFromPlayer();
		assertTrue(mrx.getTaxi().size()==5);
	}
	
	/**
	 * Testet die fahren Methode
	 */
	@Test
	public void testFahren() {
		mrx.fahren(6);
	}
	
	@Test
	public void deletCards() {
		while(mrx.getBus().size() != 0) {
			mrx.removeBus();
		}
		
		assertTrue(mrx.getBus().isEmpty());
	}
	

}
