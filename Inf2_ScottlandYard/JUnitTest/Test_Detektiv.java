import static org.junit.Assert.*;

import org.junit.Test;

import spieler.Detektiv;
import zusatsKlassen.SpielerFarben;

public class Test_Detektiv {

	Detektiv detektiv = new Detektiv(SpielerFarben.BLAU, "Detektiv1", 46);
		
	/**
	 * Test ob Takxikarten korrekt hinzugefuegt werden
	 */
	@Test
	public void testTaxikarten() {
		assertTrue(detektiv.getTaxi().size() == 10 );
	}

	/**
	 * Test ob Buskarten korrekt hinzugefuegt werden
	 */
	@Test
	public void testBuskarten() {
		assertTrue(detektiv.getBus().size() == 8);
	}
	/**
	 * Test ob UBahnikarten korrekt hinzugefuegt werden
	 */
	@Test
	public void testBahnkarten() {
		assertTrue(detektiv.getuBahn().size() == 8);
	}
}
