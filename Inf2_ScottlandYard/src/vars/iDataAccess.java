package vars;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface iDataAccess {
	
	public boolean save(Object object, String path) throws RuntimeException, IOException;
	
	public Object load(String path) throws FileNotFoundException, IOException, ClassNotFoundException;

}
