package vars;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;

public class GameBoardLoader implements iDataAccess, Serializable{
	/**
	 * Method is not implemented. Calling it will cause a RuntimeException
	 * @return 
	 * @throws RuntimeException if called
	 */
	@Override
	public boolean save(Object object, String path) throws RuntimeException{
		throw new RuntimeException("Not implemented");
		
	}

	/**
	 * Will load the GameBoard's CSV-file and return the content of said file as a StringBuffer-Object
	 * @param path - the path where the file is located. This will be passed onto FileReader-Constructor
	 * @throws FileNotFoundException In case the given file couldn't be found
	 * @throws IOException In case something went wrong while reading the file with a BufferedReader and its readLine()-method
	 */
	@Override
	public Object load(String path) throws FileNotFoundException, IOException {
		StringBuffer gameBoard = new StringBuffer();
		BufferedReader br = new BufferedReader(new FileReader(path));
		String line = br.readLine();
		while(line != null) {
			gameBoard.append(line);
			gameBoard.append("\n");
			line = br.readLine();
		}
		return gameBoard;
	}

}
