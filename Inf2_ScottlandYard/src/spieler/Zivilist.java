package spieler;

import java.io.Serializable;

import regelwerk.Regelwerk;
import regelwerk.Regelwerk_Erweiterung;
import spielleiter.Spielleiter;
import zusatsKlassen.Ringpuffer;

public class Zivilist implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private boolean manipuliert = false;
	private Spielleiter leiter;
	private String hinweis1 ="MrX befand sich vor 1 Runde Nord-Oestlich von dir";
	private String hinweis2 = "Vor 2 Rundne befand sich MrX in der Naehe von U-Bahn Station 123";
	private String hinweis3 = "Vor 3 Runden war MrX auf Feld 69";
	private int aktuellePosition;
	
	/**
	 * Position vom Zivilist auf dem Spielfeld
	 * @param position
	 */
	public Zivilist(int position, Spielleiter leiter) {
		this.aktuellePosition = position;
		this.leiter=leiter;
	}
	
	/**
	 * @return erster Hinweis mit der Region von MrX
	 */
	public String getHinweis1() {
		return hinweis1;
	}

	/**
	 * Je nach dem wo MrX ist wird er hier gesetzt
	 * kann aber auch Manipuliert sein
	 * @param hinweis1
	 */
	public void setHinweis1(String hinweis1) {
		if(manipuliert==false) {
			this.hinweis1 = hinweis1;
		} else {
			if(hinweis1.contains("Sued")) {
				this.hinweis1="MrX befand sich vor 1 Runde Nord Oestlich von dir";
			} else {
				this.hinweis1="MrX befand sich vor 1 Runde Sued Westlich von dir";
			}
		}
		
	}
	
	/**
	 * @return Hinweis mit der Bahnstation in der MrX in der Naehe war
	 */
	public String getHinweis2() {
		return hinweis2;
	}

	/**
	 * setzt den Hinweis und schaut ob der Manipuliert wurde oder nicht
	 * @param hinweis2
	 */
	public void setHinweis2(int hinweis2) {
		if(manipuliert==false) {
			this.hinweis2 = "MrX ist der U-Bahn Station " + hinweis2 + " am nsechsten";
		} else {
			leiter.getRingpuffer().getMrX().getFahrtafel().getAlleFelder().remove(hinweis2);
			int i = leiter.getRingpuffer().getMrX().getFahrtafel().getAlleFelder().get(((int)Math.random()*12)+1);
			this.hinweis2="MrX war vor 2 Runden der U-Bahn Station " + i + " am naechsten";
			leiter.getRingpuffer().getMrX().getFahrtafel().getAlleFelder().add(hinweis2);
		}
	}

	/**
	 * @return die genaue Position von MrX vor drei Runden
	 */
	public String getHinweis3() {
		return hinweis3;
	}

	/**
	 * Setzt den Hinweis oder manipuliert ihn
	 * @param hinweis3
	 */
	public void setHinweis3(int hinweis3) {
		if(manipuliert==false) {
			this.hinweis3 = "Mrx war vor 3 Runden an Position " + hinweis3;
		} else {
			int i = ((int)Math.random()*198)+1;
			this.hinweis3= "Mrx war vor 3 Runden an Position " + i;
		}
	}

	/**
	 * Getter und Setter
	 */
	public int getAktuellePosition() {
		return aktuellePosition;
	}


	public void setAktuellePosition(int aktuellePosition) {
		this.aktuellePosition = aktuellePosition;
	}

	
	/**
	 * Erstellt die Hinweise je nach Rundenzahl
	 */
	public void erzeugeHinweis() {
		
		if(leiter.getRingpuffer().getRounds()<=2) {
			
			getHinweis1();
			
		}else if(leiter.getRingpuffer().getRounds()==3) {
			int i=((int)(Math.random()*2)+1);
			if(i ==1) {
				
				getHinweis1();
				
			} else {
				
				getHinweis2();
				
			}
			
		}else {
			int i=((int)(Math.random()*3)+1);
			if(i==1) {
				
				getHinweis1();
				
			} else if(i==2) {
				
				getHinweis2();
				
			} else {
				
				getHinweis3();
				
			}
		}
		
	}

	/**
	 * Zivilist kann von MrX manipuliert werden
	 * @return true wenn er manipuliert ist
	 */
	public boolean isManipuliert() {
		return manipuliert;
	}

	/**
	 * Wenn er von MrX manipuliert wird, wird er auf true gesetzt
	 * @param manipuliert
	 */
	public void setManipuliert(boolean manipuliert) {
		this.manipuliert = manipuliert;
	}
	
	
}
