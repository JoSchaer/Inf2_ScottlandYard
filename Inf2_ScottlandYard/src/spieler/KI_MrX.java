package spieler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import gameboard.GameBoard;
import gameboard.Kanten;
import gameboard.Knoten;
import spielleiter.Spielleiter;
import zusatsKlassen.Ringpuffer;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

public class KI_MrX extends MrX implements Serializable {

	private GameBoard leiter;
	private Knoten node;

	/**
	 * Konsturktor
	 * 
	 * @param spielerFarben
	 * @param name
	 * @param anzahlSpieler
	 * @param startposition
	 */
	public KI_MrX(SpielerFarben spielerFarben, String name, int anzahlSpieler, int startposition,
			GameBoard aktSpielleiter) {
		super(spielerFarben, name, anzahlSpieler, startposition);
		leiter = aktSpielleiter;
	}

	/**
	 * Die KI sucht sich aus wo sie gerne hin ziehen moechte, dabei sucht sie sich
	 * den Knoten der am weitesten von den Detektiven entfernt ist aus
	 * 
	 * @param startposition
	 * @return ZielKnonten nummer wird zurueck gegeben
	 */
	public int movetoWhere(Knoten startposition) {
		/* We add all neighbors in a list to see where is it possible to move to */
		ArrayList<Knoten> neighbors = new ArrayList<>();
		ArrayList<Integer> distance = new ArrayList<>();
		/*
		 * Key : AnzahlKanten Value : Knotennummer
		 */
		HashMap<Integer, Integer> whereTo = new HashMap<>();

		neighbors.addAll(startposition.getNachbarknoten());

		for (int i = 0; i < neighbors.size(); i++) {
			distance.addAll(distancetoDete(neighbors.get(i)));

			whereTo.put(distance.get(i),neighbors.get(i).getKnotennummer());
		}

		ArrayList<Integer> listOfKeys = new ArrayList<>(whereTo.keySet());
		//Anhand von Psuedo Code aus dem Internet 
		for (int i = listOfKeys.size(); i > 1; i--) {
			for (int j = 0; j < i - 1; j++) {
				if (listOfKeys.get(j) > listOfKeys.get(j + 1)) {
					Collections.swap(listOfKeys, j, j + 1);
				}
			}
		}
		System.out.println(whereTo.toString());
		// Das letzte Element ist dass mit dem groessten Abstand zu den Detektiven und
		// da wollen wir hin
		return whereTo.get(listOfKeys.get(listOfKeys.size()-1));
	}

	/**
	 * Geht alle Verkehrsfarben durch die moeglich sind wir wollen lieber mit UBahn
	 * oder Boot fahren daher werden diese als erstes abgefragt und wenn sonst
	 * nichts anders geht dann erst werden Taxi und Bus genommen
	 * 
	 * @param position
	 * @param ziel
	 * @return
	 */
	public VerkehrsFarben whichColor(Knoten position, Knoten ziel) {
		if (position.getMoeglicheFahrlinien(ziel).equals(VerkehrsFarben.SCHWARZ)) {
			return VerkehrsFarben.SCHWARZ;
		} else if (position.getMoeglicheFahrlinien(ziel).equals(VerkehrsFarben.ROT)) {
			return VerkehrsFarben.ROT;
		} else if (position.getMoeglicheFahrlinien(ziel).equals(VerkehrsFarben.GRUEN)) {
			return VerkehrsFarben.GRUEN;
		} else if (position.getMoeglicheFahrlinien(ziel).equals(VerkehrsFarben.WEISS)) {
			return VerkehrsFarben.WEISS;
		}
		return VerkehrsFarben.WEISS;

	}

	/**
	 * schaut welcher Konten am weitesten von den Detektiven entfernt ist
	 * 
	 * @param neighbor
	 * @return ArrayList mit den Entfernungen der Detektive
	 */
	public ArrayList<Integer> distancetoDete(Knoten neighbor) {
		/*
		 * the one with the highest distance is where we want to move to - or get away
		 * from the one with the lowest
		 */
		int detepos = 0;
		ArrayList<Integer> farAway = new ArrayList<Integer>();
		
		ArrayList<Knoten> besetzt = new ArrayList<>();
		
		for(Knoten akt : leiter.getAlleKnoten()) {
			if(akt.feldBelegt()) {
				besetzt.add(akt);
			}
		}

		for (int i = 0; i < besetzt.size(); i++) {
			detepos = besetzt.get(i).getKnotennummer();
			// check for the distance between MrX and the Detective at the moment
			// adds it to the ArrayList which will contain all of the distances from MrX to
			// the detectives
			farAway.add(searchPath(neighbor, leiter.sucheKnotenMitID(detepos)));

		}
		return farAway;
	}

	/**
	 * Mit dem A* Algorithmus wird geschaut wie weit die Detektive weg sind anhand
	 * der kuerzesten Weges mit den kleinsten Kosten also wie schnell koennte ein
	 * Deketive bei MrX sein
	 * 
	 * @param mrX
	 * @param detective
	 * @return die Entfernung von MrX zu einem Detektiv
	 */
	public int searchPath(Knoten mrX, Knoten detective) {
		//Besuchte Knoten
		List<Knoten> visited = new LinkedList<Knoten>();
		//Noch zu besuchende Knoten
		List<Knoten> queue = new LinkedList<>();
		
		//Alle Nachbarknoten des Startknotens werden hinzugefügt
		queue.addAll(mrX.getNachbarknoten());

		// Wie wollen die Kanten mit ihren Gewichten durch gehen?
		Map<Knoten, Integer> fScore = new HashMap<>();
		fScore.put(mrX, heuristicCost(mrX, detective));

		Map<Knoten, Integer> gScore = new HashMap<>();
		gScore.put(mrX, 0);
		for(Knoten k : leiter.getAlleKnoten()) {
			for(Kanten kante : k.getVerkehrslinien()) {
				if(kante.getKnoten2().equals(k)&&kante.getKnoten1().equals(mrX)) {
					gScore.put(k, kante.getWeight());
				}
			}
		}

		boolean found = false;
		Knoten current;
		
		while (!queue.isEmpty() && !found) {
			for (int i = 0; i < queue.size(); i++) {
				for (Kanten edge : queue.get(i).getVerkehrslinien()) {
					// Wir wollen das kleinste Gewicht an der ersten Stelle
					if (edge.getWeight() == 1) {
						queue.set(0, queue.get(i));	
					}
				}
			}
			// Der Knoten mit dem kleinesten Gewicht wurde an die erste Stelle gesetzt und
			// den wollen wir jetzt
			current = queue.get(0);
			if(!visited.contains(current)) {
			int verbindungsCount = 1;
			gScore.put(current, 1);
			visited.add(current);
			queue.remove(current);

			// Detektiv gefunden
			if (current.equals(detective)) {
				found = true;
				return visited.size();
			}

			List<Knoten> neighbors = new LinkedList<>();
			neighbors.addAll(current.getNachbarknoten());

			for (int i = 0; i < neighbors.size(); i++) {
				Knoten child = neighbors.get(i);
				
				if (!queue.contains(child)) {
					System.out.println("bin in zeile 191");
					neighbors.set(heuristicCost(current, child), current);
					int moeglichGScore = gScore.get(current) + distance(current, child);
					gScore.put(child, moeglichGScore);
					queue.add(child);
				} else {

					System.out.println("\n\n\n");
					System.out.println(gScore.size());
					System.out.println("Current");
					System.out.println(current.getKnotennummer());
					System.out.println("Child");
					System.out.println(child.getKnotennummer());
					System.out.println("\n\n\n");
					if(gScore.get(current)<gScore.get(child)) {
					
					if (gScore.get(current) < gScore.get(child)) {
						neighbors.set(neighbors.lastIndexOf(child), current);
						queue.remove(child);
					}
				}
			}
			if (queue.isEmpty()) {
				return 0;
			}
			}
		}else {
			System.out.println(visited.size()-1);
			return visited.size()-1;
		}
		}
		return 0;
	}

	/**
	 * Gibt den gScore von einer Kante zurueck/ihre Gewichtung je nach dem mit
	 * welchem Verkehrsmittel gefahren werden kann
	 * 
	 * @param start
	 * @param naechstes
	 * @return die Gewichtung der Kante
	 */
	private int distance(Knoten start, Knoten naechstes) {
		for (Kanten edge : start.getVerkehrslinien()) {
			if (edge.getKnoten2().equals(naechstes)) {
				return edge.getWeight();
			}
		}
		return 0;
	}

	/**
	 * Es gibt nur einen heuristic von 1 also default, da er nicht in die Gewichtung
	 * faellt
	 * 
	 * @param start
	 * @param dete
	 * @return gibt allen einen default von 1
	 */
	private int heuristicCost(Knoten start, Knoten dete) {
		return 1;
	}
}

