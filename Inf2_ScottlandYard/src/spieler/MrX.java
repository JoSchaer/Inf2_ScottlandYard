package spieler;

import java.io.Serializable;
import java.util.ArrayList;

import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

/**
 * MrX ist ein Spieler der sich besonders verhalten darf
 * 
 * @author Eve
 *
 */
public class MrX extends Spieler implements Serializable {
	/**
	 * Attribute
	 */
	private ArrayList<VerkehrsFarben> blacktickets = new ArrayList<VerkehrsFarben>();
	private int doppelzugCounter = 2;
	Fahrtafel fahrtafel;

	/**
	 * Anfangswerte der Verkehrsfarben, MrX bekommt soviel Blackkarten wie Detektive
	 * mitspielen
	 * 
	 * @param spielerFarben
	 * @param name
	 * @param anzahlSpieler
	 */
	public MrX(SpielerFarben spielerFarben, String name, int anzahlSpieler, int startposition) {
		super(spielerFarben, name, startposition);
		fahrtafel = new Fahrtafel();
		for (int i = 0; i < 4; i++) {
			this.taxi.add(VerkehrsFarben.WEISS);
		}
		for (int i = 0; i < 3; i++) {
			this.bus.add(VerkehrsFarben.GRUEN);
		}
		for (int i = 0; i < 3; i++) {
			this.uBahn.add(VerkehrsFarben.ROT);
		}
		for (int i = 0; i < anzahlSpieler; i++) {
			this.blacktickets.add(VerkehrsFarben.SCHWARZ);
		}
	}

	/**
	 * Bootkarte wird geloescht, immer Index 0, Arraywerte ruecken immer nach vorne
	 */
	public void removeBlackTicket() {
		this.blacktickets.remove(0);
		fahrtafel.addTicket(VerkehrsFarben.SCHWARZ);
	}

	/**
	 * Neue Karte wird vom Detektiv uebergeben
	 */
	public void getTaxiFromPlayer() {
		taxi.add(VerkehrsFarben.WEISS);
	}

	/**
	 * Neue Karte wird vom Detektiv uebergeben
	 */
	public void getBusFromPlayer() {
		bus.add(VerkehrsFarben.GRUEN);
	}

	/**
	 * Neue Karte wird vom Detektiv uebergeben
	 */
	public void getUBahnFromPlayer() {
		uBahn.add(VerkehrsFarben.ROT);
	}

	/**
	 * getter DoppelzugCounter
	 * 
	 * @return
	 */
	public int getDoppelzugCounter() {
		return doppelzugCounter;
	}

	/**
	 * setter doppelZugCounter
	 * 
	 * @param doppelzugCounter
	 */
	public void setDoppelzugCounter(int doppelzugCounter) {
		this.doppelzugCounter -= doppelzugCounter;
	}

	/**
	 * Fahrtafel wird zur�ck gegeben
	 * 
	 * @return Fahrtafel von MrX
	 */
	public Fahrtafel getFahrtafel() {
		return this.fahrtafel;
	}

	/**
	 * Anzahl an BlackTickets
	 * 
	 * @return
	 */
	public ArrayList<VerkehrsFarben> getBlackTickets() {
		return this.blacktickets;
	}

	/**
	 * Overrides aus der Spielerklasse fuer seine bestimmten Beduerfnisse, wie viele
	 * Karten er hat, wo er ist etc.
	 */
	@Override
	public String toString() {
		return this.getName() + " befindet sich aktuell an Position " + this.getAktuellePosition()
				+ " und hat noch folgende Karten:\n" + this.getTaxi().size() + "x Taxi\n" + this.getBus().size()
				+ "x Bus\n" + this.getuBahn().size() + "x UBahn\n" + this.getBlackTickets().size()
				+ "x BlackTickets\nSie haben noch " + doppelzugCounter + " Doppelzuege zur verfuegung.";
	}

	/**
	 * Bekommt immer die Karten der Detektive in sein Ticketarray uebertragen
	 */
	@Override
	public void removeTaxi() {
		this.taxi.remove(0);
		fahrtafel.addTicket(VerkehrsFarben.WEISS);
	}

	@Override
	public void removeBus() {
		this.bus.remove(0);
		fahrtafel.addTicket(VerkehrsFarben.GRUEN);
	}

	@Override
	public void removeUBahn() {
		this.uBahn.remove(0);
		fahrtafel.addTicket(VerkehrsFarben.ROT);
	}

	public String getVerbrauchteTickets() {
		return fahrtafel.toString();
	}

	public VerkehrsFarben getTicketsForJSON(int ticket) {
		return fahrtafel.getAlleTickets().get(ticket);

	}

	public int getFieldIDforJSON(int field) {
		return fahrtafel.getmrXPosition().get(field);
	}

	// Innere Klasse
	public class Fahrtafel implements Serializable {
		private ArrayList<VerkehrsFarben> alleTickets = new ArrayList<>();
		private ArrayList<Integer> positionMrX = new ArrayList<>();
		private ArrayList<Integer> felderMrX = new ArrayList<>();

		public void addTicket(VerkehrsFarben ticket) {
			alleTickets.add(ticket);
		}

		public ArrayList<VerkehrsFarben> getAlleTickets() {
			return this.alleTickets;
		}

		public ArrayList<Integer> getAlleFelder() {
			return this.felderMrX;
		}

		public VerkehrsFarben getLastVerkehrsFarbe() {
			if (alleTickets.isEmpty()) {
				return VerkehrsFarben.LEER;
			} else {
				if (alleTickets.size() > 0) {
					return alleTickets.get(alleTickets.size() - 1);
				} else {
					return alleTickets.get(0);
				}
			}
		}

		public void addPosition(int position) {
			positionMrX.add(getAktuellePosition());
		}

		public ArrayList<Integer> getmrXPosition() {
			return this.positionMrX;
		}

		public void addFelder(int position) {
			felderMrX.add(position);
		}

		@Override
		public String toString() {
			String alleVerbrauchtenTickets = "";
			for (VerkehrsFarben farbe : alleTickets) {
				alleVerbrauchtenTickets += farbe.toString() + ",";
			}
			return alleVerbrauchtenTickets;
		}
	}

	/**
	 * Uberschreibt die aus dem Spielleiter von MrX gibt zusaetzlich noch
	 * blackTickets, doppelzugcounter und die fahrtafel
	 */
	@Override
	public String toCSV() {
		String mrX;
		// name, farbe, position, taxi, bus, subway, black, doppelcounter, log
		mrX = getName() + ";" + getSpielerFarben() + ";" + getAktuellePosition() + ";" + getTaxi().size() + ";"
				+ getBus().size() + ";" + getuBahn().size() + ";" + getBlackTickets().size() + ";"
				+ getDoppelzugCounter() + ";" + getVerbrauchteTickets();
		return mrX;
	}

	/**
	 * getter und setter der Blacktickets
	 * 
	 * @return
	 */
	public ArrayList<VerkehrsFarben> getBlacktickets() {
		return blacktickets;
	}

	public void setBlacktickets(int blacktickets) {
		while (blacktickets != 0) {
			this.blacktickets.add(VerkehrsFarben.SCHWARZ);
			blacktickets--;
		}
	}

	/**
	 * setzt die Fahrtafel beim laden
	 * 
	 * @param farbe
	 */
	public void setFahrtafel(VerkehrsFarben farbe) {
		fahrtafel.addTicket(farbe);
	}

}
