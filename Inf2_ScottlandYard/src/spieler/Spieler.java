package spieler;

import java.io.Serializable;
import java.util.ArrayList;


import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;
/**
 * Ein Spieler selbst ist abstarkt und definiert sich dadurch ob er einen
 * Detektiv spielt oder MrX
 * @author Eve
 *
 */
public abstract class Spieler implements Serializable {
	/**
	 * Attribute
	 */
	private SpielerFarben spielerFarben;
	private int aktuellePosition;
	private String name;
	ArrayList<VerkehrsFarben> taxi=new ArrayList<VerkehrsFarben>();;
	ArrayList<VerkehrsFarben> uBahn= new ArrayList<VerkehrsFarben>();;
	ArrayList<VerkehrsFarben> bus= new ArrayList<VerkehrsFarben>();;
	/**
	 * Konstruktor der die Spielerfarben und den Spielername uebergeben bekommt
	 * @param spielerFarben
	 * @param name
	 */
	public Spieler(SpielerFarben spielerFarben, String name,int startposition) {
		this.aktuellePosition = startposition;
		this.spielerFarben = spielerFarben;
		this.name = name;		
		
	}
	/**
	 * Taxikarte wird geloescht, immer Index 0, Arraywerte ruecken immer nach vorne
	 */
	public void removeTaxi() {
		taxi.remove(0);
	}
	/**
	 * Buskarte wird geloescht, immer Index 0, Arraywerte ruecken immer nach vorne
	 */
	public void removeBus() {
		bus.remove(0);
	}
	/**
	 * UBahnkarte wird geloescht, immer Index 0, Arraywerte ruecken immer nach vorne
	 */
	public void removeUBahn() {
		uBahn.remove(0);
	}
	/**
	 * Getter und Setter
	 * @return
	 */
	public SpielerFarben getSpielerFarben() {
		return spielerFarben;
	}

	public void setSpielerFarben(SpielerFarben spielerFarben) {
		this.spielerFarben = spielerFarben;
	}

	public ArrayList<VerkehrsFarben> getTaxi() {
		return taxi;
	}

	public ArrayList<VerkehrsFarben> getuBahn() {
		return uBahn;
	}

	public ArrayList<VerkehrsFarben> getBus() {
		return bus;
	}
	

	public void setTaxi(int taxi) {
		while(taxi !=0) {
		this.taxi.add(VerkehrsFarben.WEISS);
		taxi--;
		}
	}
	public void setuBahn(int uBahn) {
		while(uBahn !=0) {
			this.uBahn.add(VerkehrsFarben.ROT);
			uBahn--;
			}
	}
	public void setBus(int bus) {
		while(bus !=0) {
			this.bus.add(VerkehrsFarben.GRUEN);
			bus--;
			}
	}
	public int getAktuellePosition() {
		return aktuellePosition;
	}

	public void setAktuellePosition(int aktuellePosition) {
		this.aktuellePosition = aktuellePosition;
	}
	/**
	 * Es wird immer die aktuelle Position neu gesetzt
	 * @param i
	 */
	public void fahren(int i) {
			this.setAktuellePosition(i);	
	}
	/** 
	 * Name des Spielers
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Textuelle Ausabe der Position des Spielers und seiner Karten
	 */
	@Override 
	public String toString() {
		return this.getName() + " befindet sich aktuell an Position " + this.getAktuellePosition() + "\nEr  hat noch folgende Karten:\n" +this.getTaxi().size() + "x Taxi\n" + this.getBus().size() + "x Bus\n" + this.getuBahn().size() + "x UBahn\n";
	}
	/**
	 * Gibt Spieler im CSV Schema zurueck 
	 * @return name; spielerFarbe; aktuellePosition; taxi; bus; ubahn;
	 */
	public String toCSV() {
		String player;
		//name, farbe, position, taxi, bus, subway
		player ="\r" + getName()+";"+ getSpielerFarben()+";"+ getAktuellePosition()+ 
				";"+ getTaxi().size()+";"+ getBus().size()+";"+ getuBahn().size();
		return player;
	}
}
