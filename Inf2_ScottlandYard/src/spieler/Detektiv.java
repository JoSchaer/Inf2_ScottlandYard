package spieler;

import java.io.Serializable;

import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;
/**
 * Detektiv ist ein Spieler und erbt von diesem
 * @author Eve
 *
 */
public class Detektiv extends Spieler implements Serializable{
	/**
	 * am Anfang kann jeder Spieler seinen Namen nennen, eine Spielerfarbe waehlen
	 * und eine Startposition, dabei bekommt er 10 Weisse Taxikarten, 8 Gruene Buskarten
	 * 4 rote UBahn Karten 
	 * @param spielerFarben
	 * @param name
	 * @param startposition
	 */
	public Detektiv(SpielerFarben spielerFarben, String name,int startposition) {
		super(spielerFarben, name,startposition);
		for(int i =0; i<10;i++) {
			taxi.add(VerkehrsFarben.WEISS);
		}
		for(int i =0; i<8;i++) {
			bus.add(VerkehrsFarben.GRUEN);
		}
		for(int i =0; i<4;i++) {
			uBahn.add(VerkehrsFarben.ROT);
		}
	}
	
}