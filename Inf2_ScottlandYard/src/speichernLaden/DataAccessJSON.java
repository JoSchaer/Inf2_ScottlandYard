package speichernLaden;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;
import javax.swing.JFileChooser;

import gameboard.GameBoard;
import gameboard.Knoten;
import spieler.Detektiv;
import spieler.MrX;
import spieler.Spieler;
import spielleiter.Spielleiter;
import vars.iDataAccess;
import zusatsKlassen.SpielerFarben;

public class DataAccessJSON implements iDataAccess{
	JFileChooser fileChooser = new JFileChooser();
	FileInputStream fileIn; 
	FileOutputStream fileOut;
	
	/**
	 * Sichert Objekt als vergegebens JSON Schema
	 */
	
	@Override
	public boolean save(Object object, String path) throws RuntimeException, IOException {
		
			try{
				FileOutputStream fileOut = new FileOutputStream(path);
				JsonWriter writer = Json.createWriter(fileOut);
				writer.write(((JsonArrayBuilder)object).build());
				writer.close();			
			}catch (Exception e){
				e.printStackTrace();// assertFalse keinen StackTrace
		}
			return true;
	}

	/**
	 * laedt nach vorgegeben JSON Schema
	 */
	@Override
	public Object load(String path) throws FileNotFoundException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		JsonArray jArray = null;
		
		try{
			fileIn = new FileInputStream(path);
			jArray= Json.createReader(fileIn).readArray();
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return jArray;
	}

}
