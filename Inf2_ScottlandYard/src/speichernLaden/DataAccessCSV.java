package speichernLaden;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

import vars.iDataAccess;

public class DataAccessCSV implements iDataAccess {
/**
 * Speichert ein Objekt als CSV Dokument
 * gibt true zurueck wenn es richtig gespeichert wird 
 */
	@Override
	public boolean save(Object object, String path) throws RuntimeException, IOException {
		try{
			// wenn es false ist dann wird es immer neu gemacht und dass davor ueberschrieben (neues Spiel)
			FileWriter write = new FileWriter(path, false);
			BufferedWriter bw = new BufferedWriter(write);
			PrintWriter pr = new PrintWriter(bw);
			
			pr.write( object.toString());
			pr.flush();
			pr.close();
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return true;
		
	}
/**
 * Laedt das Objekt aus der CSV file
 */
	@Override
	public Object load(String path) throws FileNotFoundException, IOException, ClassNotFoundException {
		ArrayList<String[]> spieler = new ArrayList<>();
		FileReader read = new FileReader(path);
		BufferedReader br = new BufferedReader(read);
		int count = 0;
		try{
			
			String line = "";
			
			
			while((line = br.readLine()) != null) {
				String[] breaker = line.split(";");
				spieler.add(breaker);
				System.out.println(count+line);
				count++;
			}
			
			
			return spieler;
		}catch (Exception e){
			e.printStackTrace();
		}
		finally{
			if (br != null) {
				br.close();
			}
		}
		return null;
	}

}
