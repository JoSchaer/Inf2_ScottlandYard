package speichernLaden;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import gameboard.GameBoard;
import gameboard.Kanten;
import gameboard.Knoten;
import regelwerk.Regelwerk;
import spieler.Detektiv;
import spieler.MrX;
import spieler.Spieler;
import spielleiter.Spielleiter;
import vars.iDataAccess;
import zusatsKlassen.Ringpuffer;

public class DataAccessSER implements iDataAccess{
	FileOutputStream fileOut;// = new FileOutputStream (new /*Path FileChooser*/);
	ObjectOutputStream objectOutStream;// = new ObjectOutputStream(fileOut);
	
	FileInputStream fileIn;// = new FileInputStream(new /*Path FileChooser*/);
	ObjectInputStream objectInStream;// = new ObjectInputStream(fileIn);
	/**
	 * serialisert objekt und gibt true zurueck wenn es gespeichert wurde
	 */
	@Override
	public boolean save(Object object, String path) throws IOException {
		 //Serialisierung der Spielerleiter
		 try{
			 fileOut = new FileOutputStream(path);
			 objectOutStream = new ObjectOutputStream(fileOut);
			 objectOutStream.writeObject(object);
			 System.out.println(object);
			 objectOutStream.flush();
			 objectOutStream.close();
		 }catch(IOException e ){
			e.printStackTrace(); 
		 }
		 return true;
	}
	/**
	 * deserialisiert objekt
	 */

	@Override
	public Object load(String path) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		Object slgeladen = null;
		//Deserialsierung vom Spielleiter
		try{
			fileIn = new FileInputStream(path);
			objectInStream = new ObjectInputStream(fileIn);
			slgeladen = objectInStream.readObject();
			objectInStream.close();
			System.out.println(slgeladen.toString());
			return slgeladen;
		}catch(ClassNotFoundException e ){
			e.printStackTrace();
		}
		return slgeladen;
	}
	
}
