package spielleiter;

import java.util.ArrayList;

import gameboard.Kanten;
import gameboard.Knoten;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

public interface iControllerGUI extends iController {

	/**
	 * Alle informationen des Spielers. 0=Spielerfarbe, 1 = position, 2-n = Ticketanzahlen der einzelnen Verkejrsmittel.
	 * @return ArrayList
	 */
	public ArrayList<Object> spielerInfo();
	
	public ArrayList<VerkehrsFarben> getMoeglicheFahrlinien(int knotenID, int knoten2ID);
	
	/**
	 * Alle Startfelder, die zugewiesen wurden.
	 * @return ArrayList<Integer>
	 */
	public ArrayList<Integer> getStartfelder();
	/**
	 * Alle zugewiesenen Spielerfarben.
	 * @return ArrayList<SpielerFarben>
	 */
	public ArrayList<SpielerFarben> usedColors();
	
	/**
	 * Informationen wo welche Spieler steht und welche Tickets er noch bestitzt.
	 * @param spielerfarbe
	 * @return String
	 */
	public String spielergebundeneInformationen(SpielerFarben spielerfarbe);
	
	
	/**
	 * letztes Ticket dass MrX bekommen hat
	 * @return Verkehrsfarben
	 */
	public VerkehrsFarben getLastTicketMrX();
	
	/**
	 * rundencounter
	 * @return int
	 */
	public int rundencounter();

	/**
	 * Hinweise werden fuer detektive erstellt und an Zivilist gegeben
	 * @param xAktuellerSpieler
	 * @param yAktuellerSpieler
	 * @param xMrX
	 * @param yMrX
	 */
	
	public void erstelleHinweise(double xAktuellerSpieler, double yAktuellerSpieler, double xMrX, double yMrX);
	
	/**
	 * Erhalte zivlistenzahl fuer erstmaliges erstellen
	 * @param ziviName
	 * @return int
	 */
	
	public int erhalteZiviPositionen(String ziviName);
	
	/**
	 * Zahl auf die der Zivilist gestetzt wurde
	 * @return int
	 */
	public int getSpielfeldZahl();
	
	/**
	 * erhalte die Farbe des vorherigen Spielers
	 * @return SpielerFarben
	 */
	
	public SpielerFarben bekommeSpielerFarbeVonVorherigemSpieler();
	
	/**
	 * erhalte den Hinweis fuer den speziellen Zivilisten
	 * @param zivilistenName
	 * @return String
	 */
	public String bekommeHinweis(String zivilistenName);
	
	/**
	 * Wohin ist ki gezogen
	 * 
	 * @return int
	 */
	public int makeKiMove();
	
	/**
	 * Hinweis wird f�r sp�ter manipuliert
	 * 
	 * @param zivi
	 */
	
	public void manpuliereHinweis(String zivi);
	
	/**
	 * 
	 * @return Feld auf dem MrX steht
	 */
	public int getFieldMrX();
}
