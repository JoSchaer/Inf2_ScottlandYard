package spielleiter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;

import com.sun.javafx.scene.paint.GradientUtils.Parser;

import gameboard.GameBoard;
import gameboard.Kanten;
import gameboard.Knoten;
import gui.Main;
import regelwerk.Regelwerk;
import regelwerk.Regelwerk_Erweiterung;
import speichernLaden.DataAccessCSV;
import speichernLaden.DataAccessJSON;
import speichernLaden.DataAccessSER;
import spieler.Detektiv;
import spieler.KI_MrX;
import spieler.MrX;
import spieler.MrX.Fahrtafel;
import spieler.Spieler;
import spieler.Zivilist;
import vars.iDataAccess;
import zusatsKlassen.Ringpuffer;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

/**
 * implementiert iController kennt GameBoard, Ringpuffer, Regelwerk, anzahl der
 * Spieler und ein array mit startpositionen
 * 
 * @author Dani
 *
 */
public class Spielleiter implements iController, Serializable, iControllerGUI {
	/**
	 * Attribute
	 */
	private GameBoard gameBoard;
	private Ringpuffer ringpuffer;
	private Regelwerk regelwerk;
	private int anzahlSpieler;
	private ArrayList<Integer> position = new ArrayList<>();
	private int doppelzug = 2;
	private ArrayList<SpielerFarben> spielerfarben = new ArrayList<>();
	public Zivilist ziv1;
	public Zivilist ziv2;
	private int feldZiv1 = 0;
	private KI_MrX kimrx;
	private boolean kistatus = false;
	
	
	
	public void setRingpuffer(Ringpuffer ringpuffer) {
		this.ringpuffer = ringpuffer;
	}

	/**
	 * Spielleiter erstelllt im Konstruktor Ringpuffer, GameBoard und Regelwerk,
	 * prepareGaem() wird aufgerufen und bekommt die anzahl der Spieler uebergeben
	 * die mitspielen wollen. Allen spielern wird danach ein startfeld zugewiesen.
	 */
	public Spielleiter(int spieleranzahl, ArrayList<String> spielernamen, ArrayList<SpielerFarben> spielerfarben,String regelwerkTyp,String mainMethode) {
		try {
			if(mainMethode.equals("GUI")) {
				kistatus = Main.dc.getKIStatus();
			}else if(mainMethode.equals("Console")) {
				kistatus = false;
			}

			this.spielerfarben = spielerfarben;

			this.generiereSpielerfarbe();

			int blacktickets;
			if (spieleranzahl == 6) {
				blacktickets = 5;
			} else {
				blacktickets = 4;
			}
			position.add(13);
			position.add(26);
			position.add(29);
			position.add(34);
			position.add(50);
			position.add(53);
			position.add(91);
			position.add(94);
			position.add(103);
			position.add(112);
			position.add(117);
			position.add(132);
			position.add(138);
			position.add(141);
			position.add(155);
			position.add(174);
			position.add(197);
			position.add(198);
			setAnzahlSpieler(spieleranzahl);
			gameBoard = new GameBoard();
			ringpuffer = new Ringpuffer(blacktickets,this);

			prepareGame(spieleranzahl, spielernamen, this.spielerfarben);
			System.out.println("Ringpuffer: " + ringpuffer.getSize());
			
			if(regelwerkTyp.equalsIgnoreCase("Spezial Sonder Edition")) {
				regelwerk = new Regelwerk_Erweiterung((MrX) ringpuffer.getMrX());
				ziv1= new Zivilist(this.freiePos(),this);
				ziv2 = new Zivilist(this.freiePos(), this);
			}else {
				regelwerk = new Regelwerk((MrX) ringpuffer.getMrX());
			}
		
			// Startknoten werden als besetzt markiert
			for (Spieler aktSpieler : ringpuffer.getAllSpieler()) {
				gameBoard.sucheKnotenMitID(aktSpieler.getAktuellePosition()).setKnotenBelegt(true);
			}
		} catch (Exception e) {

		}

	}

	public Spielleiter() {
		// TODO Auto-generated constructor stub
		
	}
	
	public boolean getKIStatus() {
		return kistatus;
	}

	/**
	 * getter der anzahlSpieler zurueck gibt
	 * 
	 * @return anzahl der spieler
	 */
	public int getAnzahlSpieler() {
		return this.anzahlSpieler;
	}

	public void setAnzahlSpieler(int anzahl) {
		this.anzahlSpieler = anzahl;
	}
	
	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}

	/**
	 * Ueberprueft im regelwerk ob Zug gueltig, spieler werden nach ihren
	 * moeglichkeiten bewegt, Spieler verlieren dementsprechend tickets, MrX bekommt
	 * verwendete tickets
	 */
	@Override
	public boolean movePlayer(int zielNummer, VerkehrsFarben farbe) {
		try {
			int idActPlayer = ringpuffer.getActualPlayer().getAktuellePosition();
			if (regelwerk.checkZielnummer(idActPlayer, zielNummer, gameBoard.getAlleKnoten())
					&& regelwerk.checkVerbindungsfarbe(idActPlayer, zielNummer, farbe, ringpuffer.getActualPlayer(),
							gameBoard.getAlleKnoten())
					&& regelwerk.feldBelegt(gameBoard.sucheKnotenMitID(zielNummer)) == false) {
				if (ringpuffer.getActualPlayer() instanceof MrX) {
					
					return moveMrX(zielNummer, farbe);
				} else {

					if (regelwerk.kannFahren(ringpuffer.getActualPlayer(),
							gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition()),
							gameBoard.sucheKnotenMitID(zielNummer), farbe)) {
						// alter Knoten des Spielers wird als nicht besetzt gesetzt
						gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition())
								.setKnotenBelegt(false);
						ringpuffer.getActualPlayer().fahren(zielNummer);
						// neuer Knoten des Spielers wird als besetzt markiert ausser es ist MrX 
						// dann ed
						if(ringpuffer.getActualPlayer() instanceof MrX){
							gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition())
								.setKnotenBelegt(false);
						}else{
							gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition())
							.setKnotenBelegt(true);
						}
						if (farbe.equals(VerkehrsFarben.WEISS)) {
							ringpuffer.getActualPlayer().removeTaxi();
							((MrX) ringpuffer.getMrX()).getTaxiFromPlayer();
							ringpuffer.next();
							return true;
						} else if (farbe.equals(VerkehrsFarben.GRUEN)) {
							ringpuffer.getActualPlayer().removeBus();
							((MrX) ringpuffer.getMrX()).getBusFromPlayer();
							ringpuffer.next();
							return true;
						} else if (farbe.equals(VerkehrsFarben.ROT)) {
							ringpuffer.getActualPlayer().removeUBahn();
							((MrX) ringpuffer.getMrX()).getUBahnFromPlayer();
							ringpuffer.next();
							return true;
						}

					}

				}
			} else {
				return false;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * MrX wird bewegt, geheim, nur ticket wird gezeigt erst ueberpruefung im
	 * Regelwerk ob zug gueltig danach wird verwendetes ticket entfernt
	 */

	private boolean moveMrX(int zielNummer, VerkehrsFarben farbe) {
		try {
			if (regelwerk.kannFahren(ringpuffer.getActualPlayer(),
					gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition()),
					gameBoard.sucheKnotenMitID(zielNummer), farbe)) {
				gameBoard.sucheKnotenMitID(ringpuffer.getMrX().getAktuellePosition()).setKnotenBelegt(false);
				ringpuffer.getMrX().fahren(zielNummer);
				System.out.println(showMrX(zielNummer));
				gameBoard.sucheKnotenMitID(ringpuffer.getMrX().getAktuellePosition()).setKnotenBelegt(true);
				ringpuffer.getMrX().getFahrtafel().addFelder(zielNummer);
				if (farbe.equals(VerkehrsFarben.WEISS)) {
					ringpuffer.getMrX().removeTaxi();
					ringpuffer.next();
					return true;
				} else if (farbe.equals(VerkehrsFarben.GRUEN)) {
					ringpuffer.getMrX().removeBus();
					ringpuffer.next();
					return true;
				} else if (farbe.equals(VerkehrsFarben.ROT)) {
					ringpuffer.getMrX().removeUBahn();
					ringpuffer.next();
					return true;
				} else if (farbe.equals(VerkehrsFarben.SCHWARZ)) {
					((MrX) ringpuffer.getMrX()).removeBlackTicket();
					ringpuffer.next();
					return true;
				}
			}

			return false;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Ueberprueft ob MrX noch lebt, oder letzte runde gespielt/MrX erwischt wurde
	 */
	public int hasWon() {
		int whoWon = 0;
		try {
			if (ringpuffer.getActualPlayer() instanceof MrX) {
				if (ringpuffer.getRounds() > 22) {
					whoWon =1;//MrX
					return whoWon;
				} else {
					for (Spieler aktSpieler : ringpuffer.getAllSpieler()) {
						if (!(aktSpieler instanceof MrX)) {
							if (aktSpieler.getAktuellePosition() == ringpuffer.getMrX().getAktuellePosition()) {
								whoWon = 2;//Detektiv
								return whoWon;
							}
						} else {
							return whoWon;
						}
					}
				}
			} else {

				if (regelwerk.hasWon(ringpuffer.getAllSpieler(), ringpuffer.getRounds(),
						(Detektiv) ringpuffer.getActualPlayer(), ringpuffer.getMrX(), gameBoard.getAlleKnoten()) == 1) {
					whoWon =1;//MrX
					return whoWon;
				} else if (regelwerk.hasWon(ringpuffer.getAllSpieler(), ringpuffer.getRounds(),
						(Detektiv) ringpuffer.getActualPlayer(), ringpuffer.getMrX(), gameBoard.getAlleKnoten()) == 2) {
					whoWon=2;//Detektive
					return whoWon;
				} else {
					whoWon =2;
					return whoWon;
				}
			}
			return whoWon;
		} catch (Exception e) {
			return whoWon;
		}

	}
	/**
	 * Spiel wird vorbereitet, Anzahl spieler wird uebergeben, bei anzahl < 6 wird
	 * MrX + 4 spieler als standard erstellt Spieler und MrX werden Ringpuffer
	 * hinzugefuegt. MrX ist immer der erste Spieler im Ringpuffer!!
	 */
	public void prepareGame(int spieleranzahl, ArrayList<String> spielernamen, ArrayList<SpielerFarben> spielerfarben) {
		try {
			// pruefen, ob genug Spieler dabei sind
			if (spieleranzahl > 1 && spieleranzahl < 7) {
				// Ersatznamen wenn nicht genug Spieler als Detektive spielen
				String[] generierteNamen = { "ErsatzDetektiv1", "ErsatzDetektiv2", "ErsatzDetektiv3" };

				if (spieleranzahl < 6) {
					// Fuer jeden realen Spieler wird ein Spielerobjekt erstellt
					for (int i = 0; i < spieleranzahl; i++) {
						ringpuffer.addSpieler(spielernamen.get(i), this.spielerfarben.get(i),
								this.zuweisungStartfelder());
					}
					// Wenn zu wenige reale Spieler da sind werden die restlichen Spielerobjekte
					// generiert
					if (ringpuffer.getSize() < 5) {
						int verbliebeneSpieler = 5 - ringpuffer.getSize();
						for (int i = 0; i < verbliebeneSpieler; i++) {
							ringpuffer.addSpieler(generierteNamen[i], this.spielerfarben.get(spieleranzahl + i),
									this.zuweisungStartfelder());
						}
					}
				} else {
					/*
					 * Es sind alle Spieler eingeloggt, es muessen keinen bots erstellt werden.
					 */
					for (int i = 0; i < spieleranzahl; i++) {
						ringpuffer.addSpieler(spielernamen.get(i), this.spielerfarben.get(i),
								this.zuweisungStartfelder());
					}
				}

			}
		} catch (Exception e) {
			return;
		}

	}

	/**
	 * Hilfsmethode zum erstellen der Spielerfarben, nur ein Spieler kann eine Farbe
	 * haben und diese wird ihm hier zugeteilt
	 * 
	 * @param spielerfarben
	 * @return Spielerfarbe
	 */

	private void generiereSpielerfarbe() {

		// Alle Spielerfarben die es generell gibt
		SpielerFarben[] alleFarben = SpielerFarben.values();

		// Ueber alle moeglichen Farben drueber gehen
		for (int i = 0; i < alleFarben.length; i++) {
			/*
			 * wenn die aktuelle Farbe noch nicht verbraucht wurde, fuege sie den
			 * verbrauchten hinzu und gib sie dann zurueck
			 */
			if (!this.spielerfarben.contains(alleFarben[i])) {
				this.spielerfarben.add(alleFarben[i]);
			}
		}
	}

	/**
	 * gibt Rundenzahl und je nach spieler verschiedene infor zu kraten und je nach
	 * rundenzahl den standort von MrX
	 */
	@Override
	public String getAktuellenSpielstand() {
		if (ringpuffer.getActualPlayer() instanceof MrX) {
			String s = "Aktuelle Runde: " + ringpuffer.getRounds() + " \nInformationen zu MrX: "
					+ ringpuffer.getMrX().toString();

			return s;
		} else {
			if (ringpuffer.getRounds() == 3 || ringpuffer.getRounds() == 8 || ringpuffer.getRounds() == 13
					|| ringpuffer.getRounds() == 18) {
				String s = "Aktuelle Runde: " + ringpuffer.getRounds() + " Detektive im Spiel: "
						+ (ringpuffer.getSize() - 1) + "\n" + ringpuffer.toString()
						+ "\nAlle verbrauchten Tickets von MrX" + ringpuffer.getMrX().getVerbrauchteTickets()
						+ "\nMrX befand sich an folgender Position " + ringpuffer.getMrX().getAktuellePosition();
				return s;
			} else {
				String s = "Aktuelle Runde: " + ringpuffer.getRounds() + " Detektive im Spiel: "
						+ (ringpuffer.getSize() - 1) + "\n" + ringpuffer.toString()
						+ "\nAlle verbrauchten Tickets von MrX" + ringpuffer.getMrX().getVerbrauchteTickets();
				return s;
			}

		}

	}

	/**
	 * MrX zieht 2 mal hintereinander
	 */
	@Override
	public boolean doppelzugMrX(int zielNummer1, VerkehrsFarben farbeZug1, int zielNummer2, VerkehrsFarben farbeZug2) {
		try {
			int feldnummerMrX = ringpuffer.getActualPlayer().getAktuellePosition();
			if (doppelzug > 0) {

				if (feldnummerMrX == zielNummer2) {
					removeTicket(farbeZug1);
					removeTicket(farbeZug2);
					this.doppelzug -= 1;
					ringpuffer.getMrX().setDoppelzugCounter(1);
					ringpuffer.next();
					return true;
				} else if (regelwerk.checkZielnummer(feldnummerMrX, zielNummer1, gameBoard.getAlleKnoten())
						&& regelwerk.checkVerbindungsfarbe(feldnummerMrX, zielNummer1, farbeZug1,
								ringpuffer.getActualPlayer(), gameBoard.getAlleKnoten())
						&& regelwerk.checkZielnummer(zielNummer1, zielNummer2, gameBoard.getAlleKnoten())
						&& regelwerk.checkVerbindungsfarbe(zielNummer1, zielNummer2, farbeZug2,
								ringpuffer.getActualPlayer(), gameBoard.getAlleKnoten())
						&& regelwerk.feldBelegt(gameBoard.sucheKnotenMitID(zielNummer1)) == false
						&& regelwerk.feldBelegt(gameBoard.sucheKnotenMitID(zielNummer2)) == false) {

					gameBoard.sucheKnotenMitID(ringpuffer.getMrX().getAktuellePosition()).setKnotenBelegt(false);
					System.out.println(showMrX(zielNummer1));
					ringpuffer.setRounds(ringpuffer.getRounds() + 1);
					ringpuffer.getMrX().fahren(zielNummer2);
					System.out.println(showMrX(zielNummer2));
					ringpuffer.setRounds(ringpuffer.getRounds() - 1);
					gameBoard.sucheKnotenMitID(ringpuffer.getMrX().getAktuellePosition()).setKnotenBelegt(true);

					removeTicket(farbeZug1);
					removeTicket(farbeZug2);
					this.doppelzug -= 1;
					ringpuffer.getMrX().setDoppelzugCounter(1);
					ringpuffer.next();
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Entzieht dem Spieler(MrX) ein Ticket wenn er mit diesem gefahren ist
	 * 
	 * @param farbe
	 */
	private void removeTicket(VerkehrsFarben farbe) {
		if (farbe.equals(VerkehrsFarben.WEISS)) {
			ringpuffer.getMrX().removeTaxi();
		} else if (farbe.equals(VerkehrsFarben.GRUEN)) {
			ringpuffer.getMrX().removeBus();
		} else if (farbe.equals(VerkehrsFarben.ROT)) {
			ringpuffer.getMrX().removeUBahn();
		} else if (farbe.equals(VerkehrsFarben.SCHWARZ)) {
			((MrX) ringpuffer.getMrX()).removeBlackTicket();
		}
	}

	/**
	 * zuweisung am anfang, rausfinden wie viele spieler und jedem startfeld
	 * zuweisen Random zahl such startfeld aus array, �berpr�ft ob ausgeloster
	 * Knoten schon belegt ist und sollte er frei sein wird dem Spieler dieser
	 * Knoten als Startknoten zugeteilt. Sollte der Knoten bereits belegt sein wird
	 * erneut ausgelost bis ein freier startknoten gefunden wurde
	 */
	private int zuweisungStartfelder() {

		int zufall = (int) (Math.random() * 18);
		int startpos = position.get(zufall);
		position.remove(zufall);
		return startpos;
	}

	/**
	 * Getter fuer den Ringpuffer
	 * 
	 * @return
	 */
	public Ringpuffer getRingpuffer() {
		return this.ringpuffer;
	}

	/**
	 * String Ruckgaben: wie heisst der Spieler der ziehen darf, auf welchem Knoten
	 * ist der Spieler
	 */
	@Override
	public SpielerFarben darfZiehen() {
		boolean kannFahren = false;
		while (!kannFahren) {
			System.out.println(ringpuffer.toString());
			if (regelwerk.canMove(ringpuffer.getActualPlayer(),
					gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition()))) {
				kannFahren = true;
			} else {
				ringpuffer.next();
			}
		}
		return ringpuffer.getActualPlayer().getSpielerFarben();
		// return ringpuffer.getNextPlayerInQueue();
	}

	@Override
	public Object[] getSpielerKnoten(String methode) {
		if (methode.equalsIgnoreCase("doppelzug")) {
			return gameBoard.sucheKnotenMitID(ringpuffer.getMrX().getAktuellePosition()).doppelzugInfo();
		}

		else {
			if (ringpuffer.getActualPlayer() instanceof MrX) {
				return gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition()).moveInfoMrX();
			} else {
				return gameBoard.sucheKnotenMitID(ringpuffer.getActualPlayer().getAktuellePosition()).moveInfo();
			}
		}
	}

	/**
	 * Gibt die Position von MrX zurueck wenn er gezeigt werden soll.
	 * 
	 * @param zielnummer
	 * @return String
	 */
	public int showMrX(int zielnummer) {
		int i = getRingpuffer().getRounds();

		if (i == 3 || i == 8 || i == 13 || i == 18) {
			return zielnummer;
		} else {
			return 0;
		}
	}

	/**
	 * Getter fuer das Gameboard und die Spielerfarben
	 * 
	 * @return
	 */
	public GameBoard getGameBoard() {
		return this.gameBoard;
	}
	/**
	 * Holt die Spielerfarben
	 * @param findeFarbe
	 * @param spielerfarben2
	 * @return ArrayList an Spielerfarben
	 */
	private SpielerFarben getSpielerfarbe(int findeFarbe, ArrayList<SpielerFarben> spielerfarben2) {
		int i = 0;
		for (SpielerFarben aktFarbe : spielerfarben2) {
			if (i == findeFarbe) {
				return aktFarbe;
			}
			i++;
		}
		return null;
	}

	@Override
	public ArrayList<Object> spielerInfo() {
		ArrayList<Object> infos = new ArrayList<>();
		Spieler aktSpieler = ringpuffer.getActualPlayer();

		infos.add(aktSpieler.getSpielerFarben());
		infos.add(aktSpieler.getAktuellePosition());
		infos.add(aktSpieler.getTaxi().size());
		infos.add(aktSpieler.getBus().size());
		infos.add(aktSpieler.getuBahn().size());

		if (aktSpieler instanceof MrX) {
			infos.add(((MrX) aktSpieler).getBlackTickets());
			infos.add(((MrX) aktSpieler).getDoppelzugCounter());
		}
		return infos;
	}

	@Override
	public ArrayList<Integer> getStartfelder() {
		ArrayList<Integer> startfelder = new ArrayList<>();
		for (Spieler aktSpieler : ringpuffer.getAllSpieler()) {
			startfelder.add(aktSpieler.getAktuellePosition());
		}
		return startfelder;
	}

	@Override
	public ArrayList<SpielerFarben> usedColors() {
		ArrayList<SpielerFarben> usedColors = new ArrayList<>();
		for (Spieler aktSpieler : ringpuffer.getAllSpieler()) {
			usedColors.add(aktSpieler.getSpielerFarben());
		}
		return usedColors;
	}

	@Override
	public String spielergebundeneInformationen(SpielerFarben spielerfarbe) {
		for (Spieler aktSpieler : ringpuffer.getAllSpieler()) {
			if (aktSpieler.getSpielerFarben().equals(spielerfarbe)) {
				return aktSpieler.toString();
			}
		}
		return "Nicht sichtbare Informationen";
	}
	/**
	 * Speichern des Spiels als Serialisierung
	 * @param fileChooserPath
	 */
	// verknuepfen mit GUI ueber den FileChooserPath
	public void serializeSave(String fileChooserPath) {
		try {
			iDataAccess ser = new DataAccessSER();
			ser.save(this, fileChooserPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Serialisierung Laden
	 * @param path
	 */
	public Object serializeLoad(String path) {
		iDataAccess serLoad = new DataAccessSER();
		
		try {
			Spielleiter spiel = (Spielleiter) serLoad.load(path);
			System.out.println(serLoad.load(path));
			//this.gameBoard = ((Spielleiter) spiel).gameBoard;
			this.regelwerk = ((Spielleiter) spiel).regelwerk;
			this.ringpuffer = ((Spielleiter) spiel).ringpuffer;
			this.spielerfarben = ((Spielleiter) spiel).spielerfarben;
			this.anzahlSpieler = ((Spielleiter) spiel).anzahlSpieler;
			this.doppelzug = ((Spielleiter) spiel).doppelzug;
			this.position = ((Spielleiter) spiel).position;
			return spiel;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Spiel als JSON speicher
	 * @param path
	 */
	public void jsonSave(String path) {
		iDataAccess jsonSave = new DataAccessJSON();
		JsonArrayBuilder jArray = Json.createArrayBuilder();
		
		int p = ringpuffer.getLese();
		int position = p ;
		try {
			do {
				HashMap<String, JsonObjectBuilder> map = new HashMap<String, JsonObjectBuilder>();
				for (Spieler sp : ringpuffer.getAllSpieler()) {
					int i = 0;
					if(sp.equals(ringpuffer.getPlayer(position))) {
					JsonObjectBuilder spieler = Json.createObjectBuilder();
					spieler.add("name", sp.getName());
					spieler.add("color", sp.getSpielerFarben().toString());
					spieler.add("fieldID", sp.getAktuellePosition());
					spieler.add("taxi", sp.getTaxi().size());
					spieler.add("bus", sp.getBus().size());
					spieler.add("subway", sp.getuBahn().size());
					if(ringpuffer.getPlayer(position).equals(ringpuffer.getMrX())){
						//ab hier werden MrX seine zusaetzlichen Wert gesetzt
						spieler.add("color", "transparent");
						spieler.add("ferry", ringpuffer.getMrX().getBlackTickets().size());
						JsonObjectBuilder travelLog = Json.createObjectBuilder();
						JsonArrayBuilder fahrTafel = Json.createArrayBuilder();
						HashMap<Integer, JsonArrayBuilder> fahrTafelAid = new HashMap<Integer, JsonArrayBuilder>();
						//Fahrtafel fuegt eine 0 fuer Taxi, 1 fuer Bus, 2 fuer Bahn und 3 fuer black ticket
						// hinzu 
						for(int j = 0; j< ringpuffer.getRounds(); j++){
						JsonArrayBuilder rounds = Json.createArrayBuilder();
						Integer id = 0;
						if(ringpuffer.getMrX().getTicketsForJSON(j).equals(VerkehrsFarben.WEISS)){
							id =ringpuffer.getMrX().getFieldIDforJSON(j);
							rounds.add(0);
						}else if(ringpuffer.getMrX().getTicketsForJSON(j).equals(VerkehrsFarben.GRUEN)){
							id =ringpuffer.getMrX().getFieldIDforJSON(j);
							rounds.add(1);
						}else if(ringpuffer.getMrX().getTicketsForJSON(j).equals(VerkehrsFarben.ROT)){
							id= ringpuffer.getMrX().getFieldIDforJSON(j);
							rounds.add(2);
						}else if (ringpuffer.getMrX().getTicketsForJSON(j).equals(VerkehrsFarben.SCHWARZ)){
							id =ringpuffer.getMrX().getFieldIDforJSON(j);
							rounds.add(3);
						}
						fahrTafelAid.put(id, rounds);
						fahrTafel.add(fahrTafelAid.get(j)).build();				
						}
						travelLog.add("log", fahrTafel);
						spieler.add("travelLog", travelLog);
	
						spieler.add("doubleMoveTicket", ringpuffer.getMrX().getDoppelzugCounter());
						}
					map.put("spieler" +i, spieler);
					jArray.add((map.get("spieler"+i)).build());
					i++;
					
					}ringpuffer.getNextPlayerInQueue();
				position= ringpuffer.getLese();

					ringpuffer.getNextPlayerInQueue();
				position= ringpuffer.getLese();
					}

				
			}while(position != p);
				jsonSave.save(jArray, path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Spiel als JSON laden
	 * @param path
	 */
	public void jsonLoad(String path) {
		iDataAccess jsonLoad = new DataAccessJSON();
		JsonArray jArray;
		Spieler erster = null; 
		
		try {
			jArray = (JsonArray) jsonLoad.load(path);
			int spielerAnzahl =0;
			ringpuffer.clearRing();
			this.ringpuffer= new Ringpuffer(jArray.size(),this);
			for(int j=0; j<jArray.size()-1;j++) {
				ringpuffer.addSpieler("default", SpielerFarben.BLAU, 1);
			}
			for (int i = 0; i < jArray.size(); i++) {
				JsonObject jobj = jArray.getJsonObject(i);
				Spieler spieler = ringpuffer.getPlayer(++spielerAnzahl);
					spieler.setName(jobj.getString("name"));
					if(jobj.getString("color").equalsIgnoreCase("blau")) {
						spieler.setSpielerFarben(SpielerFarben.BLAU);
					}else if(jobj.getString("color").equalsIgnoreCase("geld")) {
						spieler.setSpielerFarben(SpielerFarben.GELB);
					}else if(jobj.getString("color").equalsIgnoreCase("rot")) {
						spieler.setSpielerFarben(SpielerFarben.ROT);
					}else if(jobj.getString("color").equalsIgnoreCase("gruen")) {
						spieler.setSpielerFarben(SpielerFarben.GRUEN);
					}else if(jobj.getString("color").equalsIgnoreCase("schwarz")) {
						spieler.setSpielerFarben(SpielerFarben.SCHWARZ);
					}else if (jobj.getString("color").equalsIgnoreCase("transparent")){
						spieler.setSpielerFarben(SpielerFarben.DURCHSICHTIG);
					}
					spieler.setAktuellePosition(gameBoard.sucheKnotenMitID(jobj.getInt("fieldID")-1).getKnotennummer());
					spieler.getTaxi().clear();
					for (int t = 0; t < jobj.getInt("taxi"); t++) {
						spieler.getTaxi().add(VerkehrsFarben.WEISS);
					}
					spieler.getBus().clear();
					for (int b = 0; b < jobj.getInt("bus"); b++) {
						spieler.getBus().add(VerkehrsFarben.GRUEN);
					}
					spieler.getuBahn().clear();
					for (int u = 0; u < jobj.getInt("subway"); u++) {
						spieler.getuBahn().add(VerkehrsFarben.ROT);
					}
				if(jobj.getString("color").equalsIgnoreCase("transparent")){
					MrX mrX = (MrX) spieler;
					mrX.getBlackTickets().clear();
					for (int bl = 0; bl < jobj.getInt("black"); bl++) {
						mrX.getBlackTickets().add(VerkehrsFarben.SCHWARZ);
					}
						mrX.setDoppelzugCounter(jobj.getInt("doubleMoveTicket"));
					//Fahrtafel
					JsonObject travelLog = jobj.getJsonObject("travelLog");
					if(travelLog.get("log").equals(0)){
						mrX.getFahrtafel().addTicket(VerkehrsFarben.WEISS);
						mrX.getFahrtafel().addPosition(i);
					}else if (travelLog.get("log").equals(1)){
						mrX.getFahrtafel().addTicket(VerkehrsFarben.GRUEN);
						mrX.getFahrtafel().addPosition(i);
					}else if(travelLog.get("log").equals(2)){
						mrX.getFahrtafel().addTicket(VerkehrsFarben.ROT);
						mrX.getFahrtafel().addPosition(i);
					}else if (travelLog.get("log").equals(3)){
						mrX.getFahrtafel().addTicket(VerkehrsFarben.SCHWARZ);
						mrX.getFahrtafel().addPosition(i);
					}
				}
				if(spielerAnzahl == 1) {
					erster = spieler;
					for(int x=0; x< ringpuffer.getAllSpieler().size();x++) {
						if(ringpuffer.getActualPlayer().equals(erster)) {
							ringpuffer.setLese(x);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Speichern des Spiels als CSV
	 * @param path
	 */
	public void saveCSV(String path) {
		iDataAccess CSVsave = new DataAccessCSV();
		
		try {
			CSVsave.save(toCSV(), path);
		} catch (RuntimeException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Ladet den aktuellen Spielstand aus einer CSV
	 * @param path
	 * @return
	 */
	public Object loadCSV(String path) {
		iDataAccess CSVload = new DataAccessCSV();
		try {
			ArrayList<String[]> spielerListe = (ArrayList<String[]>) CSVload.load(path);
			
			this.anzahlSpieler = spielerListe.size();
			Ringpuffer ring = new Ringpuffer(Integer.parseInt(spielerListe.get(spielerListe.size()-1)[3]),this);
					ring.setLese(Integer.parseInt(spielerListe.get(spielerListe.size()-1)[0]));
					ring.setSchreiben(Integer.parseInt(spielerListe.get(spielerListe.size()-1)[1]));
					ring.setRounds(Integer.parseInt(spielerListe.get(spielerListe.size()-1)[2]));
			ArrayList<Spieler> spielerlisteRingpuffer = new ArrayList<>();	
			for(int i =0; i<spielerListe.size()-2;i++) {
				
				if(i ==0 ) {
					MrX mrX = new MrX(SpielerFarben.DURCHSICHTIG, "MrX", spielerListe.size()-1, Integer.parseInt((spielerListe.get(0)[2])));
					mrX.setTaxi((Integer.parseInt(spielerListe.get(0)[3])));
					mrX.setBus(Integer.parseInt(spielerListe.get(0)[4]));
					mrX.setuBahn(Integer.parseInt(spielerListe.get(0)[5]));
					mrX.setBlacktickets(Integer.parseInt(spielerListe.get(0)[6]));
					mrX.setDoppelzugCounter(Integer.parseInt(spielerListe.get(0)[7]));
					System.out.println(spielerListe.get(0).length);
						
					String[] fahrtafel = spielerListe.get(0)[8].split(",");
					for(int j=0; j<fahrtafel.length;j++) {
							if(fahrtafel[j].equals(VerkehrsFarben.WEISS)) {
								mrX.setFahrtafel(VerkehrsFarben.WEISS);
							}else if(fahrtafel[j].equals(VerkehrsFarben.GRUEN)) {
								mrX.setFahrtafel(VerkehrsFarben.GRUEN);
							}else if(fahrtafel[j].equals(VerkehrsFarben.ROT)) {
								mrX.setFahrtafel(VerkehrsFarben.ROT);
							}else if (fahrtafel[j].equals(VerkehrsFarben.SCHWARZ)) {
								mrX.setFahrtafel(VerkehrsFarben.SCHWARZ);
							}
					}
					spielerlisteRingpuffer.add(mrX);
					
				}else if (0 <i&& i<spielerListe.size()-1) {
					
					Detektiv dete = new Detektiv(farbeVonString(spielerListe.get(i)[1]),spielerListe.get(i)[0], Integer.parseInt(spielerListe.get(i)[2]));
					dete.setTaxi((Integer.parseInt(spielerListe.get(i)[3])));
					dete.setBus((Integer.parseInt(spielerListe.get(i)[4])));
					dete.setuBahn((Integer.parseInt(spielerListe.get(i)[5])));
					
					spielerlisteRingpuffer.add(dete);
				}
			}
			ring.setSpielerList(spielerlisteRingpuffer);
			this.setRingpuffer(ring);
			this.setGameBoard(new GameBoard());
			this.regelwerk= new Regelwerk(ringpuffer.getMrX());
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (NullPointerException e) {
			e.printStackTrace();
		}
	return  null;	
	}
		/**
		 * Sucht die Strings aus der CSV passend zu den SpielerFarben
		 * @param farbe
		 * @return gibt die Farbe zurueck
		 */
	private SpielerFarben farbeVonString(String farbe) {
		switch (farbe) {
		case "ROT":
			return SpielerFarben.ROT;
		case "GRUEN":
			return SpielerFarben.GRUEN;
		case "GELB":
			return SpielerFarben.GELB;
		case "BLAU":
			return SpielerFarben.BLAU;
		case "SCHWARZ":
			return SpielerFarben.SCHWARZ;
		default:
			return null;
		}
	}
	/**
	 * Spieler werden als String gespeichert fuer das CSV Schema
	 * @return zusammen gesetzter String mit allen Spielern
	 */
	public String toCSV() { 
		String data = "";
		for(Spieler sp : ringpuffer.getAllSpieler()) {
			data += sp.toCSV();	
		}
		data += ringpuffer.toCSV();
		return data+"\n";
	}
	/**
	 * Gibt die letzten Tickets von MrX zurueck
	 */
	@Override
	public VerkehrsFarben getLastTicketMrX() {
		VerkehrsFarben farbe = ringpuffer.getMrX().getFahrtafel().getLastVerkehrsFarbe();
		return farbe;
	}
	/**
	 * die Anzahl der Runden wird aus dem Ringpuffer geholt
	 */
	@Override
	public int rundencounter() {
		return ringpuffer.getRounds();
	}
	/**
	 * Moegliche Fahrlinien der Knoten die gewaehlt wurden
	 */
	@Override
	public ArrayList<VerkehrsFarben> getMoeglicheFahrlinien(int knotenId, int knoten2ID) {
		ArrayList<VerkehrsFarben> moeglicheFarben = new ArrayList<>();
		for(Kanten aktKante :this.gameBoard.sucheKnotenMitID((int)knotenId).getMoeglicheFahrlinien(gameBoard.sucheKnotenMitID((int)knoten2ID))) {
			moeglicheFarben.add(aktKante.getFarbe());
		}
		
		return moeglicheFarben;
		
	}
	/**
	 * Erstellt die Hinweise aus dem zweiten Regelwerk
	 */
	@Override
	public void erstelleHinweise(double xAktuellerSpieler, double yAktuellerSpieler, double xMrX, double yMrX) {
		if(ringpuffer.getRounds()<=2) {
			
			ziv1.setHinweis1(((Regelwerk_Erweiterung)regelwerk).checkRegion(xAktuellerSpieler, yAktuellerSpieler, xMrX, yMrX));
			ziv2.setHinweis1(((Regelwerk_Erweiterung)regelwerk).checkRegion(xAktuellerSpieler, yAktuellerSpieler, xMrX, yMrX));
			
		} else if(ringpuffer.getRounds()==3) {
			
			ziv1.setHinweis1(((Regelwerk_Erweiterung)regelwerk).checkRegion(xAktuellerSpieler, yAktuellerSpieler, xMrX, yMrX));
			ziv2.setHinweis1(((Regelwerk_Erweiterung)regelwerk).checkRegion(xAktuellerSpieler, yAktuellerSpieler, xMrX, yMrX));
			
			ziv1.setHinweis2(((Regelwerk_Erweiterung)regelwerk).checkLokal(ringpuffer.getRounds()));
			ziv2.setHinweis2(((Regelwerk_Erweiterung)regelwerk).checkLokal(ringpuffer.getRounds()));
			
		} else {
		
		ziv1.setHinweis1(((Regelwerk_Erweiterung)regelwerk).checkRegion(xAktuellerSpieler, yAktuellerSpieler, xMrX, yMrX));
		ziv2.setHinweis1(((Regelwerk_Erweiterung)regelwerk).checkRegion(xAktuellerSpieler, yAktuellerSpieler, xMrX, yMrX));
		
		ziv1.setHinweis2(((Regelwerk_Erweiterung)regelwerk).checkLokal(ringpuffer.getRounds()));
		ziv2.setHinweis2(((Regelwerk_Erweiterung)regelwerk).checkLokal(ringpuffer.getRounds()));
		
		ziv1.setHinweis3(((Regelwerk_Erweiterung)regelwerk).checkPraezise(ringpuffer.getRounds()));
		ziv2.setHinweis3(((Regelwerk_Erweiterung)regelwerk).checkPraezise(ringpuffer.getRounds()));
		
		}
	}
	
	/**
	 * Hilfsmethode fuer die KI
	 * Geht durch alle Spieler und speichert ihre Aktuelle Position ein 
	 * @return  ArrayList mit allen Positionen der Detektive 
	 */
	public ArrayList<Integer> detectiveNodes(){
		ArrayList<Integer> locationsofDetes = new ArrayList<>();
		
		for( Spieler dete : ringpuffer.getAllSpieler()){
			locationsofDetes.add(dete.getAktuellePosition());
			if(dete instanceof MrX){
				locationsofDetes.remove(dete);
			}
		}
		return locationsofDetes;
	}
	
	/**
	 * Sucht ein freies, random ausgesuchtes Feld auf das sich der Zivilist stellen kann
	 * @return moegliches Feld
	 */
	public int freiePos() {
		
		int j = (int)((Math.random()*198)+1);
		while(j!=0) {
			if (!gameBoard.sucheKnotenMitID(j).feldBelegt()&& j != feldZiv1) {
				feldZiv1=j;
				return j;
			} else {
				j = (int)((Math.random()*198)+1);
			}
		}
		return 0;
	}
	/**
	 * Holt die Feldzahl des vorherigen Spielers
	 */
	@Override
	public int getSpielfeldZahl() {
		return ringpuffer.getSpielerDerGradDranWar().getAktuellePosition();
	}
	/**
	 * Gibt die Spielerfarbe des vorherigen Spielers zurueck
	 */
	@Override
	public SpielerFarben bekommeSpielerFarbeVonVorherigemSpieler() {
		return ringpuffer.getSpielerDerGradDranWar().getSpielerFarben();
	}
	/**
	 * Holt den Hinweis fuer den Detektiv
	 */
	@Override
	public String bekommeHinweis(String zivilistenName) {
		int hinweis = (int) Math.random()*2;
		
		switch (hinweis) {
		case 0:
			if(zivilistenName.equalsIgnoreCase("zivi1")) {
				return ziv1.getHinweis1();
			}
			else {
				return ziv2.getHinweis1();
			}

		case 1:
			if(zivilistenName.equalsIgnoreCase("zivi1")) {
				return ziv1.getHinweis2();
			}
			else {
				return ziv2.getHinweis2();
			}
		case 2:
			if(zivilistenName.equalsIgnoreCase("zivi1")) {
				return ziv1.getHinweis3();
			}
			else {
				return ziv2.getHinweis3();
			}
		}
		return "Fehlgeschlagen wir haben Fehler";
	}
	/**
	 * Die Positionen von den Zivilisten
	 */
	@Override
	public int erhalteZiviPositionen(String ziviName) {
		if(ziviName.equalsIgnoreCase("Zivi1")) {
			ziv1.setAktuellePosition(freiePos());
			return ziv1.getAktuellePosition();
		}
		else {
			ziv2.setAktuellePosition(freiePos());
			return ziv2.getAktuellePosition();
		}
	}

	/**
	 * Wird geschaut wo Ki hinziehen will
	 * @return zielnummer der KI
	 */
	@Override
	public int makeKiMove() {
		
		
			int zielKI=0;
			VerkehrsFarben farbeKI= VerkehrsFarben.WEISS;
			
			zielKI = ((KI_MrX)ringpuffer.getMrX()).movetoWhere(gameBoard.sucheKnotenMitID(ringpuffer.getMrX().getAktuellePosition()));
			//zielKI = kimrx.movetoWhere(gameBoard.getAlleKnoten().get(ringpuffer.getActualPlayer().getAktuellePosition()));
			//farbeKI = kimrx.whichColor(gameBoard.getAlleKnoten().get(ringpuffer.getActualPlayer().getAktuellePosition()),
			//		gameBoard.getAlleKnoten().get(zielKI));
			
			//Die KI muss noch eine Farbe aussuchen mit der sie fahren moechte
			moveMrX(zielKI, VerkehrsFarben.WEISS);
			
			return zielKI;
		
	}
	/**
	 * �ndert boolean f�r Hinweise, ob von MrX manipuliert oder nicht
	 */
	@Override
	public void manpuliereHinweis(String zivi) {
		if(zivi.equalsIgnoreCase("Zivi1")) {
			ziv1.setManipuliert(true);
		}
		else {
			ziv2.setManipuliert(true);
		}
	}

	/**
	 * Gibt Pos von MrX zur�ck
	 * @return pos MrX
	 */
	@Override
	public int getFieldMrX() {
		
		return ringpuffer.getMrX().getAktuellePosition();
	}
	
}