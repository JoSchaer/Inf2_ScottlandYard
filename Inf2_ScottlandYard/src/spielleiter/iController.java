package spielleiter;

import java.util.ArrayList;

import gameboard.Knoten;
import spieler.Spieler;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

/**
 * 
 * @author Evelyn
 *
 */
public interface iController {
	// Rueckgabeparameter fuer movePlayer
	/**
	 * movePlayer erlaubt einem Spieler seinen Spielzug auszufuehren wenn dieser mit
	 * dem uebergeben Verkehrsmittel moeglich ist
	 * 
	 * @param feldzahl
	 * @param verkehrsfarbe
	 * @return Zug erfolgreich oder nicht
	 */
	public boolean movePlayer(int feldzahl, VerkehrsFarben verkehrsfarbe);

	/**
	 *Welcher spieler ist als naechstes an der Reihe
	 * 
	 * @return Spieler der an der Reihe ist.
	 */
	public SpielerFarben darfZiehen();

	/**
	 * Schaut ob ein Spieler gewonnen hat oder nicht syso wer gewonnen hat
	 * 
	 * @return Spieler der gewonnen hat.
	 */
	public int hasWon();

	/**
	 * wo alle Spieleraktuell sind, wer wie viele Karten hat, wie viele Runden
	 * gespielt werden koenne
	 * 
	 * @return String mit Informationen
	 */
	public String getAktuellenSpielstand();

	/**
	 * MrX darf zweimal hintereinander ziehen ohne Pause zu machen dabei gibt er
	 * beide Felder ueber die er ziehen moechte
	 * 
	 * @param feldzahl1
	 * @param f1
	 * @param feldzahl2
	 * @param f2
	 */
	public boolean doppelzugMrX(int feldzahl1, VerkehrsFarben verkehrsfarbe1, int feldzahl2, VerkehrsFarben verkehrsfarbe2);

	
	/**
	 * 
	 * @return Array mit ArrayLists. Index 0 Infos Startknoten,Index 1 Infos Nachbarknoten
	 * @param String 
	 */
	public Object[] getSpielerKnoten(String methode);
	
}
