package test;

import java.util.ArrayList;
import java.util.HashSet;
import spieler.Spieler;
import spielleiter.Spielleiter;
import zusatsKlassen.Ringpuffer;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;


/**
 * Main Klasse um die Methoden zum Spielen zu testen
 * @author Evelyn
 *
 */

public class MainClass {

	/**
	 * Main Methode in der alle Methoden aufgerufen werden, die zum Spielen ben�tigt werden.
	 * Am Anfang werden die Spielernamen in eine ArrayList gespeichert, danach die Spielerfarben.
	 * Es wird ein Spielleiter erzeugt �ber den das ganze Spiel gespielt werden kann.
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		String[] namen = {"MrX", "Dete", "Dete1", "Dete2"};
		
		ArrayList<String> spielerNamen = new ArrayList<String>();
			for(int i = 0; i < namen.length; i++) {
				spielerNamen.add(namen[i]);
			}
		
		SpielerFarben [] farben = SpielerFarben.values();
		
		HashSet<SpielerFarben> alleFarben = new HashSet<SpielerFarben>();
		for(int i = 0; i < farben.length; i++) {
			alleFarben.add(farben[i]);
		}
		
		//Spielleiter wird erzeugt
		Spielleiter spielleiter = new Spielleiter(4, spielerNamen, alleFarben);
	
		//Liste mit allen Spielern wird erzeugt und durch das Ringpuffer nacheinander jedem eine Startposition gegeben
		ArrayList<Spieler> alleSpieler = new ArrayList<Spieler>();
		alleSpieler = (ArrayList<Spieler>) spielleiter.getRingpuffer().getAllSpieler();
		alleSpieler.get(0).setAktuellePosition(88);
		alleSpieler.get(1).setAktuellePosition(60);
		alleSpieler.get(2).setAktuellePosition(13);
		alleSpieler.get(3).setAktuellePosition(198);
		alleSpieler.get(4).setAktuellePosition(70);
		
		// erster Spieler ( MrX ) faehrt von 88 nach 117 mit dem Taxi
		System.out.println(spielleiter.darfZiehen());
		if(spielleiter.movePlayer(117, VerkehrsFarben.WEISS)) {
			System.out.println("(Probe: MrX faehrt)");
		}
		else {
			System.out.println("(Probe: MrX faehrt nicht)");
		}
		System.out.println(spielleiter.getRingpuffer().getMrX().toString());
		System.out.println("\n\n\n");
		
		// erster Dete faehrt mit dem Taxi zu 76
		System.out.println("\n\n");
		System.out.println(spielleiter.darfZiehen());
		System.out.println("\n\n");
		spielleiter.movePlayer(76, VerkehrsFarben.WEISS);
		System.out.println(spielleiter.getAktuellenSpielstand());
		
		
		
		//Dete1 faehrt mit dem Bus zu 52
		System.out.println("\n\n");
		System.out.println(spielleiter.darfZiehen());
		System.out.println("\n\n");
		spielleiter.movePlayer(52, VerkehrsFarben.GRUEN);
		System.out.println(spielleiter.getAktuellenSpielstand());
		
		
		
		//Dete2 faehrt mit dem Taxi zu 186
		System.out.println("\n\n");
		System.out.println(spielleiter.darfZiehen());
		System.out.println("\n\n");
		spielleiter.movePlayer(186, VerkehrsFarben.WEISS);
		System.out.println(spielleiter.getAktuellenSpielstand());
		
		
		
		//Ersatzdete faehrt mit Taxi zu 87
		System.out.println("\n\n");
		System.out.println(spielleiter.darfZiehen());
		System.out.println("\n\n");
		spielleiter.movePlayer(87, VerkehrsFarben.WEISS);
		System.out.println(spielleiter.getAktuellenSpielstand());
		
		System.out.println(spielleiter.darfZiehen());
		if(spielleiter.doppelzugMrX(108, VerkehrsFarben.WEISS, 115, VerkehrsFarben.SCHWARZ)) {
			System.out.println("(Probe: Doppelzug klappt)");
		}else {
			System.out.println("(Probe: Doppelzug klappt nicht)");
		}
		System.out.println(spielleiter.getRingpuffer().getMrX().toString());
		
		//naechster Spieler bis man wieder beim MrX ist
				spielleiter.getRingpuffer().next();
				spielleiter.getRingpuffer().next();
				spielleiter.getRingpuffer().next();
				spielleiter.getRingpuffer().next();
				
				//nochmal Doppelzug
				if(spielleiter.doppelzugMrX(126, VerkehrsFarben.WEISS, 140, VerkehrsFarben.WEISS)) {
					System.out.println("(Probe: Doppelzug klappt)");
				}else {
					System.out.println("(Probe: Doppelzug klappt nicht)");
				}
				System.out.println(spielleiter.getRingpuffer().getMrX().toString());
				
		//naechster Spieler bis man wieder beim MrX ist
		spielleiter.getRingpuffer().next();
		spielleiter.getRingpuffer().next();
		spielleiter.getRingpuffer().next();
		spielleiter.getRingpuffer().next();
		
		//nochmal Doppelzug, der Doppelzug wird nicht ausgef�hrt, da MrX schon zwei davor verbraucht hat
				if(spielleiter.doppelzugMrX(82, VerkehrsFarben.GRUEN, 100, VerkehrsFarben.GRUEN)) {
					System.out.println("(Probe: Doppelzug klappt)");
				}else {
					System.out.println("(Probe: Doppelzug klappt nicht)");
				}
				System.out.println(spielleiter.getRingpuffer().getMrX().toString());
				
		//Detektive gewinnen, Dete steht auf dem Selben Feld wie MrX zur Probe
				System.out.println(spielleiter.darfZiehen());
				spielleiter.getRingpuffer().next();
				System.out.println(spielleiter.darfZiehen());
				spielleiter.getRingpuffer().getActualPlayer().fahren(140);
				System.out.println("\nGewonnen hat die Gruppe, welche: "+spielleiter.hasWon().getName()+" beinhaltet.");
				
		//Spiel wird bei 23 Runden begonnen, damit man die Methode hasWon testen kann
		//Der Name des Spielers der gewonnen hat wird ausgegeben
		spielleiter.getRingpuffer().setRounds(23);
		System.out.println("\n\n");
		System.out.println("Gewonnen hat "+spielleiter.hasWon().getName()+ ".");
		
	}
	
	
}
