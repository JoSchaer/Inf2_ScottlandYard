package zusatsKlassen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.scene.shape.SVGPath;

public class ErzeugeButtons {

	private ArrayList<String[]> buttonInfos = new ArrayList<>();
	
	

	public ArrayList<String[]> erzeugeButtons() {
		BufferedReader bufferedReader = null;
		String line = "";
		
		try {
			bufferedReader = new BufferedReader(new FileReader("ButtonInfo.csv"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while ((line = bufferedReader.readLine()) != null) {
				String[] feld = line.split(",");
				buttonInfos.add(feld);
			}
			return this.buttonInfos;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return buttonInfos;
	}
}
