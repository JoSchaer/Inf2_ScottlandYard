package zusatsKlassen;

import java.io.Serializable;

/**
 * moegliche Farben der Spieler
 * @author  Evelyn
 *
 */
public enum SpielerFarben implements Serializable{
	DURCHSICHTIG, GRUEN, ROT, BLAU, GELB, SCHWARZ
}
