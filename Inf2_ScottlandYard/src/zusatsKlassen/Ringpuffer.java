package zusatsKlassen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import gui.Main;
import spieler.Detektiv;
import spieler.KI_MrX;
import spieler.MrX;
import spieler.Spieler;
import spielleiter.Spielleiter;

/**
 * Spieler werden in einem Ringpuffer gespeichert, dabei koennen Runden gezaehlt
 * werden MrX wird immer am index[0] gesetzt und ist immer der erste Spieler
 * 
 * @author celinaronjaschreiner
 *
 */
public class Ringpuffer implements Serializable {
	/**
	 * Attribute
	 */
	private ArrayList<Spieler> spielerList = new ArrayList<Spieler>();
	private int lese = 0;
	private int schreiben = 0;
	private int rounds = 1;
	private int size;
	private Spielleiter spielleiter;

	/**
	 * Default Konstruktor
	 */
	public Ringpuffer() {

	}

	/**
	 * Konstruktor ueberprueft die Anzahl der Spieler Sie muss zwischen 2 und 6 sein
	 * 
	 * @param size
	 */
	public Ringpuffer(int size, Spielleiter aktSpielleiter) {
		spielleiter = aktSpielleiter;
		this.size = size;
		if (size < 3) {
			throw new RuntimeException("Mindestspieleranzahl nicht erreicht");
		} else if (size > 7) {
			throw new RuntimeException("Zu viele Mitspieler");
		}
	}

	/**
	 * Sind alle Spieler da/ist die Liste voll
	 * 
	 * @return
	 */
	public boolean isFull() {
		boolean full = true;
		for (int i = 0; i < spielerList.size(); i++) {
			if (spielerList.get(i) == null) {
				full = false;
			}
		}
		return full;
	}

	/**
	 * schaut ob kein Spieler vorhanden ist
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return spielerList.isEmpty();
	}

	/**
	 * erhoeht die Lese variable
	 */
	private void incLese() {
		if (lese < this.spielerList.size() - 1) {
			lese++;
		} else {
			lese = 0;
		}
	}

	/**
	 * erhoeht die Schreibe variable
	 */
	private void incSchreiben() {
		if (schreiben < this.spielerList.size() - 1) {
			schreiben++;
		} else {
			schreiben = 0;
		}
	}

	/**
	 * Fuegt neue Spieler in die Liste hinein und erhoeht danach den Schreiber um
	 * eins
	 * 
	 * @param o
	 */
	public void addSpieler(String name, SpielerFarben color, int startposition) {
		if (spielerList.size() == 6) {
			throw new RuntimeException("Es sind maximal 6 Spieler erlaubt!");
		} else {
			if (spielerList.isEmpty()) {

				if (spielleiter.getKIStatus()) {
					spielerList.add(0, new KI_MrX(color, name, this.size - 1, startposition, spielleiter.getGameBoard()));
				} else {
					spielerList.add(0, new MrX(color, name, this.size - 1, startposition));
				}
			} else {
				spielerList.add(new Detektiv(color, name, startposition));
			}
		}
		incSchreiben();
	}

	/**
	 * Gibt die Position des Spielers zurueck
	 * 
	 * @return
	 */
	public Spieler getActualPlayer() {
		return spielerList.get(lese);
	}

	/**
	 * Gibt alle Spieler und ihre Positionen zurueck
	 */
	public ArrayList<Spieler> getAllSpieler() {
		return spielerList;
	}

	/**
	 * Gibt den naechsten Spieler aus
	 */
	public Spieler next() {
		if (lese == spielerList.size() - 1) {
			lese = 0;
			this.countRounds();
			return spielerList.get(lese);
		} else {
			incLese();
			return spielerList.get(lese);
		}
	}

	/**
	 * Gibt den naechsten Spieler in de Reihe zurueck unde den Leser weiter zu
	 * stellen
	 * 
	 * @return naechsten Spieler
	 */
	public Spieler getNextPlayerInQueue() {
		if (lese == spielerList.size() - 1) {
			return spielerList.get(0);
		} else {
			return spielerList.get(lese + 1);
		}
	}

	/**
	 * Zaehlt die Spielrunde
	 */
	private void countRounds() {
		if (lese == 0) {
			rounds++;
		}
	}

	/**
	 * Getter und Setter
	 * 
	 * @return
	 */
	public MrX getMrX() {
		return (MrX) spielerList.get(0);
	}

	public int getSize() {
		return spielerList.size();
	}

	public ArrayList<Spieler> getSpielerList() {
		return spielerList;
	}

	public void setSpielerList(ArrayList<Spieler> spielerList) {
		this.spielerList = spielerList;
	}

	public int getLese() {
		return lese;
	}

	public void setLese(int lese) {
		this.lese = lese;
	}

	public int getSchreiben() {
		return schreiben;
	}

	public void setSchreiben(int schreiben) {
		this.schreiben = schreiben;
	}

	public int getRounds() {
		return rounds;
	}

	public void setRounds(int rounds) {
		this.rounds = rounds;
	}

	public Spieler getSpielerDerGradDranWar() {
		if (lese - 1 < 0) {
			return spielerList.get(spielerList.size() - 1);
		} else {
			return spielerList.get(lese - 1);
		}
	}

	public Spieler getPlayer(int player) {
		return this.spielerList.get(player);
	}

	public void clearRing() {
		this.spielerList = null;
	}

	@Override
	public String toString() {
		String s = "";
		for (int i = 1; i < spielerList.size(); i++) {
			String s2 = spielerList.get(i).toString();
			s += s2;
		}
		return s;
	}

	public String toCSV() {
		String ringpuffer = "\r" + lese + ";" + schreiben + ";" + rounds + ";" + size;
		return ringpuffer;
	}
}
