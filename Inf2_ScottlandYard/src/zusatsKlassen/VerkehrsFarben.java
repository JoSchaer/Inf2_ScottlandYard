package zusatsKlassen;

import java.io.Serializable;

/**
 * Farben der verschiedenen Verkehrsmittel
 * @author  Evelin
 */
public enum VerkehrsFarben implements Serializable {
	//Taxi,Bus,U-Bahn,Boot
	WEISS, GRUEN, ROT, SCHWARZ,LEER
}
