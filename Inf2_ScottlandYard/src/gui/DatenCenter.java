package gui;

import java.lang.reflect.Array;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;

import javafx.animation.Animation.Status;
import javafx.animation.PathTransition;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Line;
import javafx.util.Duration;
import spielleiter.Spielleiter;
import spielleiter.iController;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;


public class DatenCenter {

	/**
	 * Variablen
	 */
	
	private iController controller;
	private ArrayList<SpielerFarben> farbenFuerSpielerstellung = new ArrayList<>();
	private ArrayList<String> spielernamenFuerSpielerstellung = new ArrayList<>();
	private int realeSpieler;
	private Media hauptMusik = new Media(Paths.get("Media/frametraxx_Space-Trip.mp3").toUri().toString());
	private Media startMusik = new Media(Paths.get("Media/frametraxx_Light-People.mp3").toUri().toString());
	private MediaPlayer start;
	private MediaPlayer mp;
	private boolean play_pause = false;
	private ArrayList<Object> spielerInfo;
	private Object [] knotenInfo;
	private int gewonnenerSpieler = 0;
	private String methode;
	private double zieKnotenX;
	private double zielKnotenY;
	private double startKnotenX;
	private double startKnotenY;
	private VerkehrsFarben verkehrsmittel;
	private Spielleiter spielleiter;
	private String regelwerkTyp ="";
	private ArrayList<Object> nachbarknotenInfosZug1 = new ArrayList<>();
	private ArrayList<Object> nachbarknotenInfosZug2 = new ArrayList<>();
	private boolean kiStatus = false;
	

	
	
	/**
	 * Objekte f�r Musik werden erzeugt
	 */
	public DatenCenter() {
		start = new MediaPlayer(startMusik);
		mp = new MediaPlayer(hauptMusik);
		start.setVolume(0);
		start.play();
	}
	/**
	 * getter Methode
	 * @return methode
	 */
	public String getMethode() {
		return methode;
	}

	/**
	 * setter
	 * @param methode
	 */
	public void setMethode(String methode) {
		this.methode = methode;
	}
	
	/**
	 * Musik, Pause und Weiter
	 */
	public void play_pause() {
		if(!play_pause) {
			mp.play();
			play_pause=true;
		}
		else {
			mp.pause();
			play_pause = false;
		}
	}
	
	public void mute() {
		if(mp.isMute()) {
			mp.setMute(false);
		}
		else {
			mp.setMute(true);
		}
	}
	
	/**
	 * Lautstaerke 
	 * @param value
	 */
	public void lautstaerke(double value) {
		mp.setVolume(value);
	}
	
	/**
	 * Musik die nach dem Login im Hauptspiel kommt
	 */
	public void wechselZuHauptMusik() {
		start.stop();
		mp.setVolume(0.5);
		mp.play();
		play_pause = true;
	}

	/**
	 * @return the farbenFuerSpielerstellung
	 */
	public ArrayList<SpielerFarben> getFarbenFuerSpielerstellung() {
		return farbenFuerSpielerstellung;
	}

	/**
	 * @param farbenFuerSpielerstellung the farbenFuerSpielerstellung to set
	 */
	public void setFarbenFuerSpielerstellung(ArrayList<SpielerFarben> farbenFuerSpielerstellung) {
		this.farbenFuerSpielerstellung = farbenFuerSpielerstellung;
	}

	/**
	 * @return the spielernamenFuerSpielerstellung
	 */
	public ArrayList<String> getSpielernamenFuerSpielerstellung() {
		return spielernamenFuerSpielerstellung;
	}

	/**
	 * @param spielernamenFuerSpielerstellung the spielernamenFuerSpielerstellung to set
	 */
	public void setSpielernamenFuerSpielerstellung(ArrayList<String> spielernamenFuerSpielerstellung) {
		this.spielernamenFuerSpielerstellung = spielernamenFuerSpielerstellung;
	}

	/**
	 * @return the realeSpieler
	 */
	public int getRealeSpieler() {
		return realeSpieler;
	}

	/**
	 * @param realeSpieler the realeSpieler to set
	 */
	public void setRealeSpieler(int realeSpieler) {
		this.realeSpieler = realeSpieler;
	}
	/**
	 * @return spielerInfo
	 */
	public ArrayList <Object> getSpielerinfo(){
		return spielerInfo;
	}
	/**
	 * 
	 * @param spielerInfo
	 */
	public void  setSpielerinfo(ArrayList<Object> spielerInfo){
		this.spielerInfo = spielerInfo;
	}
	/**
	 * 
	 * @return knotenInfo
	 */
	public Object[] knotenInfo(){
		return knotenInfo;
	}
	/**
	 * 
	 * @param knotenInfo
	 */
	public void setKnotenInfo(Object[] knotenInfo){
		this.knotenInfo = knotenInfo;
	}
	/**
	 * @return gewonnenSpieler
	 */
	public int getGewonnenerSpieler() {
		return gewonnenerSpieler;
	}
	/**
	 * 
	 * @param gewonnenerSpieler
	 */
	public void setGewonnenerSpieler(int gewonnenerSpieler) {
		this.gewonnenerSpieler = gewonnenerSpieler;
	}
	/**
	 * Koordinaten des Zielknotens werden gesetzt
	 */
	public void setZielKnoten(double xCoord, double yCoord){
		
		 this.zieKnotenX= xCoord;
		 this.zielKnotenY= yCoord;
		
	}
	/**
	 * Startkoordinaten werden gesetzt
	 */
	public void setStartKnoten(double xCoord, double yCoord){
		startKnotenX=xCoord;
		startKnotenY=yCoord;
	
	}
	/**
	 * @return coordsStart
	 */
	public double[] getStartKnoten() {
		double[] coordsStart = {startKnotenX,startKnotenY};
		return coordsStart;
	}
	/**
	 * @return coordsZiel
	 */
	public double[] getZielKnoten() {
		double[] coordsZiel = {zieKnotenX,zielKnotenY};
		return coordsZiel;
	}
	/**
	 * @return verkehrsmittel
	 */
	public VerkehrsFarben getVerkehrsmittel(){
		return verkehrsmittel;
	}
	/**
	 * 
	 * @param verkehrsmittel
	 */
	public void  setVerkehrsmittel(VerkehrsFarben verkehrsmittel){
		this.verkehrsmittel = verkehrsmittel;
	}
	
	public Spielleiter getSpielleiter() {
		return spielleiter;
	}
	public void setSpielleiter(Object spielleiter) {
		this.spielleiter = (Spielleiter)spielleiter;
	}
	public String getRegelwerkTyp() {
		return regelwerkTyp;
	}
	public void setRegelwerkTyp(String regelwerkTyp) {
		this.regelwerkTyp = regelwerkTyp;
	}
	public ArrayList<Object> getNachbarknotenInfosZug1() {
		return nachbarknotenInfosZug1;
	}
	public void setNachbarknotenInfosZug1(ArrayList<Object> nachbarknotenInfosZug1) {
		this.nachbarknotenInfosZug1 = nachbarknotenInfosZug1;
	}
	public ArrayList<Object> getNachbarknotenInfosZug2() {
		return nachbarknotenInfosZug2;
	}
	public void setNachbarknotenInfosZug2(ArrayList<Object> nachbarknotenInfosZug2) {
		this.nachbarknotenInfosZug2 = nachbarknotenInfosZug2;
	}
	
	public boolean getKIStatus() {
		return kiStatus;
	}
	
	public void setKIStatus(boolean kiStatus) {
		this.kiStatus = kiStatus;
	}

}
