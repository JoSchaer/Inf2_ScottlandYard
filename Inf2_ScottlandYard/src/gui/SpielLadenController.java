package gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import speichernLaden.DataAccessCSV;
import speichernLaden.DataAccessJSON;
import speichernLaden.DataAccessSER;
import spielleiter.Spielleiter;
import vars.iDataAccess;

public class SpielLadenController implements Initializable {
/**
 * FXML Komponente
 */
	@FXML 
	Button dateiAuswaehlen;
	
	@FXML 
	Button laden;
	
	@FXML 
	Button zurueck;
	
	@FXML 
	Button beenden;
	
	private iDataAccess data;
	private File f;
	private Spielleiter spielleiter = new Spielleiter();
		
	/**
	 * Ueberschreibt die Methode zum Oeffnen des Laden Fensters
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		dateiAuswaehlen.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				FileChooser fileChooser = new FileChooser();
			    fileChooser.setTitle("Zu Ladendes Spiel auswaehlen");
				f = fileChooser.showOpenDialog(Main.stage);
				System.out.println(f.toString());
				if(!f.getPath().equals( null)){
					
					if(f.getPath().endsWith(".ser")){
						Main.dc.setSpielleiter(spielleiter.serializeLoad(f.getPath()));
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ladeGame();
					}else if(f.getPath().endsWith(".json")){
						spielleiter.jsonLoad(f.getPath());
						Main.dc.setSpielleiter(spielleiter);
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ladeGame();
					}else if(f.getPath().endsWith(".csv")){
						spielleiter.loadCSV(f.getPath());
						Main.dc.setSpielleiter(spielleiter);
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ladeGame();
					}
				}
//				else if(f.getPath().equals(null)) {
//					System.out.println("Fuck this!");
//				}
				
			}
		});
	
		beenden.setOnAction(new EventHandler<ActionEvent>() {
		//	@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});	
	
		zurueck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					//VBox ist das Pane, welches die naechste Scene als Root hat
					VBox root = (VBox)FXMLLoader.load(getClass().getResource("StartBildschirm.fxml"));
					//Neue Scene wird erzeugt und das neue Fenster als Anzeige uebergeben
					Scene sc = new Scene(root);
					/*
					 * Selbe Stage wie aus der Main, hier wird einfsch nur die neue Scene rein geladen.
					 */
					Main.stage.setScene(sc);
					Main.stage.setMaximized(true);
					Main.stage.initStyle(StageStyle.UNDECORATED);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
	}
	
	public void ladeGame() {
		try {
							BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
							Scene sc = new Scene(root);
							Main.stage.setScene(sc);
							Main.stage.setFullScreen(true);
						//	Main.stage.initStyle(StageStyle.UNDECORATED);
							Main.dc.wechselZuHauptMusik();
						} catch (IOException e) {
							e.printStackTrace();
						}
	}

}
