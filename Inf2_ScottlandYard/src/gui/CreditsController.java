package gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class CreditsController implements Initializable {
	/**
	 * Label und zwei Buttons 
	 * Label : wer hat das Spiel erstellt
	 * Button: zurueck zum Mainfester, beenden des ganzen Spiels
	 */
	@FXML
	Label creditsText;
	@FXML
	Button zurueck;
	@FXML
	Button beenden;

	/**
	 * Text des Labels (wer hat dieses Spiel erstellt
	 */
	private String credits = "Dieses sagenhafte Spiel wurde eintwickelt von:\r\n" + 
			"Celina Schreiner MKIB2\r\n" + 
			"Daniel Melnicki MKIB3\r\n" + 
			"Evelyn Krebes MKIB3\r\n" + 
			"Jonathan Schaertel MKIB3\r\n" + 
			"\r\n" + 
			"Das Copyright obliegt den oben genannten Personen!\r\n" + 
			"November 2017."+
			"\r\n"+
			"Die Musik stammt von:"+
			"\r\n"+
			"https://www.frametraxx.de/info/kostenlose-gemafreie-musik.html";

	/**
	 * Initialisiert das Fenster mit dem Label text und die buttons mit
	 * ihren Funktionen
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		creditsText.setText(credits);
		
		beenden.setOnAction(new EventHandler<ActionEvent>() {
			//Wird geklickt wird das Fenster geschlossen
			@Override
			public void handle(ActionEvent event) {
			System.exit(0);
				
			}
		});
		
		zurueck.setOnAction(new EventHandler<ActionEvent>() {

			/*
			 * Hier wird die Scene gewechselt.
			 */
			@Override
			public void handle(ActionEvent event) {
				try {
					//VBox ist das Pane, welches die naechste Scene als Root hat
					VBox root = (VBox)FXMLLoader.load(getClass().getResource("StartBildschirm.fxml"));
					//Neue Scene wird erzeugt und das neue Fenster als Anzeige uebergeben
					Scene sc = new Scene(root);
					/*
					 * Selbe Stage wie aus der Main, hier wird einfsch nur die neue Scene rein geladen.
					 */
					Main.stage.setScene(sc);
					//Stage bleibt im Vollbildmodus
					Main.stage.setFullScreen(true);
					//Unterdr�ckung, dass immer der Satz Mit ESC k�nnen sie den Vollbildmodus verlassen angezeigt wird.
					Main.stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
