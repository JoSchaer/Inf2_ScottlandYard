package gui;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import gui.MainController;
import spieler.Detektiv;
import spieler.MrX;
import spielleiter.Spielleiter;
import spielleiter.iController;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;
/**
 * Durchlauf des kompletten Spiels
 * @author Jonathan
 *
 */
public class ConsoleUI {
	/**
	 * Attribute
	 */
	private static String moeglicheEingaben = "Die moeglichen Eingaben sind:\n"
			+ "- help: 						Welche Eingaben koennen Sie alles machen.\r\n"
			+ "- x/exit/close/soeinmuell : 		Beendet das Spiel.\r\n" + "\r\n"
			+ "- move: 						Bewegt den Spieler.\r\n"
			+ "- spielstand: 					Gibt Ihnen den aktuellen Spielstand, soweit dieser fuer Sie sichtbar ist.\r\n"
			+ "- doppelzug: 					Mr X fuehrt einen Doppelzug aus(nur fuer Mr X).\r\n"
			+ "- gewonnen: 					Erfahren Sie ob jemand das Spiel gewonnen hat.\r\n" + "";
	private static ArrayList<String> spielernamen = new ArrayList<>();
	private static ArrayList<SpielerFarben> spielerfarben = new ArrayList<>();
	private static int spieleranzahl = 0;
	private static SpielerFarben[] spielerFarben = SpielerFarben.values();
	private static HashSet<String> alleFarbenInArrayList = new HashSet<>();
	private static HashSet<String> genutzteFarben = new HashSet<>();

	/**
	 * Main zum Anfangen des Spiels
	 * @param args
	 */
	public static void main(String[] args) {
		startGame();
	}
	/**
	 * Eroeffnet das Spiel. Begruessung, Spielerauswahl: Namen, Farben, Zuweisung der
	 * Spielfelder 
	 * Der erste Spieler ist immer MrX danach koennen die anderen Spieler
	 * ihren Namen eingeben und eine Spielerfarbe waehlen 
	 * danach wird ihnene ein Startfeld zugewiesen. 
	 * Spieler koennen auf der Console unterschiedliche Eingaben machen um 
	 * zu spielen, diese werden angezeigt.
	 */
	public static void startGame() {
		for (int g = 0; g < spielerFarben.length; g++) {
			alleFarbenInArrayList.add(spielerFarben[g].toString().toUpperCase());
		}
		System.out.println("Wilkommen bei der ScottlandYard SuperSonderSpecial Consolen Edition\n Das naechtliche London erwartet Sie.");
		String spieleranzahlString = readIn("Bitte geben Sie die Anzahl der Spieler ein.\nEs muessen 2 bis 6 Spieler sein.");
		
		ArrayList<String> nummern = new ArrayList<>();
		nummern.add("2");
		nummern.add("3");
		nummern.add("4");
		nummern.add("5");
		nummern.add("6");
		//Wenn der String nich eine der Nummern ist, muss die Eingabe wiederholt werden.
		while (!nummern.contains(spieleranzahlString)) {
			spieleranzahlString =readIn("Fehlerhafte Eingabe, bitte erneut Spieleranzahl eingeben.");
		}
		spieleranzahl= Integer.parseInt(spieleranzahlString);
		
		System.out.println("\n!!!DER ERSTE SPIELER IST IMMER MR_X!!!\n");
		String name = "";
		for (int i = 1; i <= spieleranzahl; i++) {
			if (i == 1) {
				System.out.println("Spieler " + i + ": Sie sind MrX deshalb bekommen Sie automatisch den Namen MrX");
				name = "MrX";
				spielernamen.add(name);
			} else {
				System.out.println("\nSpieler " + i + ":");
				
				name = readIn("Bitte geben Sie ihren Wunschnamen ein. Mindestens 2 Zeichen!!");
				checkIfX(name);
				boolean acceptName = false;
				boolean notMrX = checkName(name.toLowerCase());
				do {
					/*
					 * Solange der Name in irgend einer Form MrX enth�lt muss ein neuer Name
					 * eingegeben werden
					 */
					while (!notMrX) {
						name = readIn("Es kann nur einen MrX geben. Bitte einen anderen Namen eingeben.");
						checkIfX(name);
						notMrX = checkName(name.toLowerCase());
					}

					while (name.length() < 2) {
						name = readIn("Name zu kurz. Bitte erneut eingeben.");
						checkIfX(name);
						notMrX = checkName(name.toLowerCase());
					}
					if (notMrX) {
						acceptName = true;
					}
				} while (!acceptName);

				spielernamen.add(name);
				System.out.println("Name hinzugefuegt");

			}

			if (i == 1) {
				System.out.println(
						"\nSie sind Mr X. Ihnen wird automatisch die Farbe DURCHSICHTIG zugewiesen, da Sie somit unsichtbar sind.");
				spielerfarben.add(SpielerFarben.DURCHSICHTIG);
				genutzteFarben.add(SpielerFarben.DURCHSICHTIG.toString());
			} else {
				
				boolean farbeInOrdnung = false;
				System.out.println("\nFarbe waehlen:");
				System.out.println("Sie koennen zwischen folgenden Farben waehlen:");
				for (int k = 0; k < spielerFarben.length; k++) {
					if (!spielerfarben.contains(spielerFarben[k])) {
						System.out.println("-" + spielerFarben[k]);
					}
				}
				String farbe = readIn("\nBitte jetzt eine der Farben eingeben:").toUpperCase();
				checkIfX(farbe);

				while(!checkIfColorOK(farbe)) {
					farbe = readIn("Falsche Farbe oder Farbe wird schon genutzt. Bitte erneut eingeben").toUpperCase();
					checkIfX(farbe);
				}
				
				int d = 0;
				while(d<spielerFarben.length) {
					if (farbe.equalsIgnoreCase(spielerFarben[d].toString())) {
					spielerfarben.add(spielerFarben[d]);
					System.out.println("Farbe erfolgreich hinzugefuegt!");
					}
					d++;
				}
				/*for (int d = 0; d < spielerFarben.length; d++) {
					if (farbe.equalsIgnoreCase(spielerFarben[d].toString())) {
						spielerfarben.add(spielerFarben[d]);
						System.out.println("Farbe erfolgreich hinzugefuegt!");
					}
				}*/
			}
		}
		gameLoop();
	}
	/**
	 * Hilfsmethode zum check ob die Farbe die eingegeben wurde schon von einem 
	 * der andere Spieler gewaehlt wurde oder ob es ueberhaupt eine meogliche
	 * Spielerfarbe ist
	 * @param color
	 * @return true wenn diese Farbe moeglich ist
	 */
	private static boolean checkIfColorOK(String color) {
		
		if (alleFarbenInArrayList.contains(color) && genutzteFarben.contains(color)== false) {
			genutzteFarben.add(color);
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * Beschreibt den Spielablauf:
	 * Ein Spieler bekommt immer seine momentane Position sowie die meoglichen 
	 * Verkehrsmittel. Durch die Eingabe der Feldzahl und Verkehrsfarbe kann
	 * er sich auf dem Spielfeld bewegen.
	 */
	public static void gameLoop() {
		System.out.println("\n\n\n\n");
		iController controller = new Spielleiter(spieleranzahl, spielernamen, spielerfarben, "Standard", "Console");
		boolean exit = false;
		String eingabe = "";
		

		/*
		 * Der Spielablauf beginnt
		 */
		while (!exit) {
			
			System.out.println("\n!!!!" + controller.darfZiehen() + " ist am Zug.!!!\n");
			System.out.println(controller.getAktuellenSpielstand());
			System.out.println(moeglicheEingaben);
			eingabe = readIn("Bitte machen sie Ihre Eingabe");
			//checkIfX(eingabe);

			// Alle moeglichen EIngaben werden angezeigt
			if (eingabe.equalsIgnoreCase("help")) {
				System.out.println(moeglicheEingaben);
			}
			/*
			 * Spiel wird beendet
			 */
			else if (eingabe.equalsIgnoreCase("x") || eingabe.equalsIgnoreCase("exit")
					|| eingabe.equalsIgnoreCase("close") || eingabe.equalsIgnoreCase("soeinmuell")) {
				exit = true;
			}
			// Bewege den Spieler
			else if (eingabe.equalsIgnoreCase("move")) {
				Object[] feldinfo = controller.getSpielerKnoten("move");
				
				ArrayList<Object> startknotenInfo = (ArrayList<Object>) feldinfo[0];
				ArrayList<Object> nachbarknoteninfo = (ArrayList<Object>) feldinfo[1];
				HashSet<VerkehrsFarben> farbenStartknoten = new HashSet<>();
				
				String startInfo = "Sie sind gerade auf Feld: "+ startknotenInfo.get(0)+ ".\nSie k�nnen mit folgenden Farben fahren: ";
				for(Object farbe : startknotenInfo) {
					if(farbe instanceof VerkehrsFarben) {
						farbenStartknoten.add((VerkehrsFarben) farbe);
						startInfo+= ((VerkehrsFarben)farbe).toString() + " | ";
					}
				}
				
				String nachbarInfo = "Sie koennen auf folgende Felder fahren:\n";
				for(Object o : nachbarknoteninfo) {
					if(o instanceof Integer) {
						nachbarInfo += "Feld "+ (Integer) o +" | ";
					}
				}
				System.out.println(startInfo);
				System.out.println(nachbarInfo);
				
				String zielFeldString = "";
				int zielfeld = 0;
				String zugFarbeString = "";
				int wdhCount = 0;
				do {
					wdhCount++;
					if (wdhCount > 1) {
						System.out.println("\n\nIhre Eingabe war Fehlerhaft bitte versuchen Sie es erneut.\n");
					}

					zielFeldString = readIn("Zielfeld eingeben:");
					checkIfHelp(zugFarbeString);
					if(checkIfX(zugFarbeString)) {
						break;
					}
					
					while (!checkInteger(zielFeldString)) {
						zielFeldString = readIn("Eingabe keine Ganzzahl. Bitte erneut eingeben");
					}
					zielfeld = Integer.parseInt(zielFeldString);

					zugFarbeString = readIn("Farbe eingeben:");
					checkIfHelp(zugFarbeString);
					if(checkIfX(zugFarbeString)) {
						break;
					}
					
				} while (!controller.movePlayer(zielfeld, erzeugeVerkehrsfarbe(zugFarbeString)));
				eingabe= "gewonnen";
			}
			// Doppelzug
			else if (eingabe.equalsIgnoreCase("doppelzug")) {
				String zielFeld1String = "";
				String zielFeld2String = "";
				int zielfeld1 = 0;
				int zielfeld2 = 0;
				String farbeZug1 = "";
				String farbeZug2 = "";
				int wdhCount = 0;
				
				
				
				

				if (controller.darfZiehen().equals(SpielerFarben.DURCHSICHTIG)) {
					Object[] knotenInfo = controller.getSpielerKnoten("doppelzug");
					ArrayList<Object> startknotenInfo = (ArrayList<Object>) knotenInfo[0];
					ArrayList<Object> nachbarknotenInfo = (ArrayList<Object>) knotenInfo[1];
					ArrayList<Object[]> nachbarnachbarInfo = (ArrayList<Object[]>) knotenInfo[2];
					HashSet<VerkehrsFarben> farbenStartknoten = new HashSet<>();
					
					String startInfo = "|| ERSTER ZUG  ||\n\nSie sind gerade auf Feld: "+ startknotenInfo.get(0)+ ".\nSie k�nnen mit folgenden Farben fahren: ";
					for(Object farbe : startknotenInfo) {
						if(farbe instanceof VerkehrsFarben) {
							farbenStartknoten.add((VerkehrsFarben) farbe);
							startInfo+= ((VerkehrsFarben)farbe).toString() + " | ";
						}
					}
					
					String nachbarInfo = "Sie koennen auf folgende Felder fahren:\n";
					for(Object o : nachbarknotenInfo) {
						if(o instanceof Integer) {
							nachbarInfo += "Feld "+ (Integer) o +" | ";
						}
					}
					
					String zweiterZug = "\n\n|| FUER DEN ZWEITEN ZUG ||\n";
					ArrayList<String> nachbarText = new ArrayList<>();
					
					for(Object[] objectarray : nachbarnachbarInfo) {
						ArrayList<Object> start = (ArrayList<Object>) objectarray[0];
						ArrayList<Object> nachbar = (ArrayList<Object>) objectarray[1];
						HashSet<VerkehrsFarben> verkehrsmittel = new HashSet<>();
						int feldID = (int) start.get(0);
						String info = "\n\nDas Feld mit der ID: " +feldID +" ,kann mit folgenden Farben befahren werden:\n";
						
						for(Object color : start) {
							if (color instanceof VerkehrsFarben) {
								verkehrsmittel.add((VerkehrsFarben) color);
								info += ((VerkehrsFarben)color).toString()+" | ";
							}
						}
						
						 info += "\nSie koennen im zweiten Zug auf folgende Felder fahren:\n";
						for(Object o : nachbar) {
							if(o instanceof Integer) {
								info += "Feld "+ (Integer) o +" | ";
							}
						}
						
						nachbarText.add(info);
						
					}
					
					System.out.println(startInfo);
					System.out.println(nachbarInfo);
					System.out.println(zweiterZug);
					for(String secondField : nachbarText) {
						System.out.println(secondField);
					}
					
					
					
					do {
						wdhCount++;
						if (wdhCount > 1) {
							System.out.println("\n\nIhre Eingabe war Fehlerhaft. Bitte versuchen Sie es erneut.\n");
						}
						
						
						
						
						
						
						
						
						System.out.println("\nBitte geben Sie folgende Daten ein:");

						// Zielfeld 1 abfragen
						zielFeld1String = readIn("-Zielfeld 1:");
						if(checkIfX(zielFeld1String)) {
							break;
						}
						checkIfHelp(zielFeld1String);
						while (!checkInteger(zielFeld1String)) {
							zielFeld1String=readIn("Ihre Eingabe is keine Zahl. Bitte erneut eingeben.");
						}
						zielfeld1 = Integer.parseInt(zielFeld1String);

						// Farbe fuer den ersten Zug abfragen
						farbeZug1 = readIn("Verkehrsmittel Farbe 1:");
						if(checkIfX(farbeZug1)) {
							break;
						}
						while(checkIfHelp(farbeZug1)) {
							farbeZug1 = readIn("Sie haben help eingegeben, Bitte geben Sie nun eine Farbe ein");
						}

						// Zielfeld 2 abfragen
						zielFeld2String = readIn("-Zielfeld 2:");
						if(checkIfX(zielFeld2String)){
							break;
						}
						checkIfHelp(zielFeld2String);
						while (!checkInteger(zielFeld2String)) {
							zielFeld2String=readIn("Ihre Eingabe is keine Zahl. Bitte erneut eingeben.");
						}
						zielfeld2 = Integer.parseInt(zielFeld2String);

						farbeZug2 = readIn("Verkehrsmittel Farbe 2:");
						if(checkIfX(farbeZug2)) {
							break;
						}
						while(checkIfHelp(farbeZug2)) {
							farbeZug2 = readIn("Sie haben help eingegeben, Bitte geben Sie nun eine Farbe ein");
						}

					} while (!controller.doppelzugMrX(zielfeld1, erzeugeVerkehrsfarbe(farbeZug1), zielfeld2,
							erzeugeVerkehrsfarbe(farbeZug2)));
					eingabe= "gewonnen";
				}

				else {
					System.out.println("Sie sind nicht MrX. Doppelzug nicht moeglich");
				}

			} else if (eingabe.equalsIgnoreCase("gewonnen")) {
				if (controller.hasWon() == 1) {
					System.out.println("MrX hat gewonnen. Glueckwunsch London zerstoert. Die Detektive waren zu dumm.BREXIT!!!");
					exit = true;
				} else if (controller.hasWon() ==2) {
					System.out.println("Die Detektive haben gewonnen. London wurde gerettet.\nEs waren die Schlauesten von Scotland Yard am Werk!");
					exit = true;
				} else {
					System.out.println("Bisher hat noch niemand gewonnen. Strengt euch an!");
				}

			} else if (eingabe.equalsIgnoreCase("spielstand")) {
				System.out.println(controller.getAktuellenSpielstand());
			}

		}

		System.out.println(
				"Sie haben das Spiel beendet.\nVielen Dank fuer Ihr Vertrauen in *HIER BELIEBIGEN NAMEN EINFUEGEN*.");
		System.out.println("Wir hoffem es hans Ihnen wemigstems 1 Bischen gefallt");
	}
	/**
	 * Hilfsmethode zum einlesen von dem was auf der Console geschrieben wird
	 * @param outputMessage
	 * @return
	 */
	public static String readIn(String outputMessage) {
		Scanner einlesen = new Scanner(System.in);
		System.out.println(outputMessage);
		String wert = "";

		wert = einlesen.next();
		// einlesen.close();
		return wert;
	}

	/**
	 * Ueberprueft, ob der Name in irgend einer Weise MrX enthaelt.
	 * 
	 * @param nameString
	 *            Name der vom Spieler eingegeben wurde.
	 * @return gueltig ja oder nein
	 */
	public static boolean checkName(String nameString) {
		String nachM = "";
		String nachR = "";
		if (nameString.contains("m")) {
			String[] splitWithM = nameString.split("m");
			if (splitWithM.length > 1) {
				nachM = splitWithM[1];
			} else {
				return true;
			}
			if (nachM.contains("r")) {
				String[] splitWithR = nachM.split("r");
				if (splitWithR.length > 1) {
					nachR = splitWithR[1];
				} else {
					return true;
				}
				if (nachR.contains("x")) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Hilfsemthode zur Erzeugung aller Verkehrsfarben die im Spiel verwendet werden
	 * 
	 * @param farbe
	 * @return alle Verkehrsfarben
	 */
	private static VerkehrsFarben erzeugeVerkehrsfarbe(String farbe) {
		VerkehrsFarben[] alleVerkehrsFarben = VerkehrsFarben.values();

		for (int f = 0; f < alleVerkehrsFarben.length; f++) {
			if (farbe.equalsIgnoreCase(alleVerkehrsFarben[f].toString())) {
				return alleVerkehrsFarben[f];

			}
		}
		return null;
	}
	/**
	 * Checkt ob eine Eingabe zum Ende des Spiels fuerht
	 * @param eingabe
	 */
	private static boolean checkIfX(String eingabe) {
		if (eingabe.equalsIgnoreCase("x") || eingabe.equalsIgnoreCase("exit") || eingabe.equalsIgnoreCase("close")
				|| eingabe.equalsIgnoreCase("soeinmuell")) {
			System.out.println(
					"\nSie haben das Spiel beendet.\nVielen Dank fuer Ihr Vertrauen in *HIER BELIEBIGEN NAMEN EINFUEGEN*.");
			System.out.println("Wir hoffem es hans Ihnen wemigstems 1 Bischen gefallt\n\n");
			System.exit(0);
			return true;
		}
		
		
		return false;
	}
	
	private static boolean checkIfHelp(String stringToCheck) {
		if(stringToCheck.equalsIgnoreCase("help")) {
			System.out.println("Programm wurde unterbrochen.");
			System.out.println(moeglicheEingaben);
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * Check ob die Eingabe eine Ganzzahl ist
	 * @param eingeleseneZahl
	 * @return true wenn dem so ist
	 */
	public static boolean checkInteger(String eingeleseneZahl) {
		try {
			int value = Integer.parseInt(eingeleseneZahl);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

}