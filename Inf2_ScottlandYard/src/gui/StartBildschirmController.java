package gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class StartBildschirmController implements Initializable {
	/**
	 * FXML Komponenten
	 */
	@FXML
	StackPane stackPane;
	@FXML
	Button neuesSpiel;
	@FXML
	Button spielLaden;
	@FXML
	Button spielBeenden;
	@FXML
	Button credits;
	@FXML
	Button willkommen;
	
	
	/**
	 * Auf dem StartBildschirm kann man ein neues Spiel laden 
	 * ein Spielladen dass noch nicht zu Ende gespielt wurde
	 * Beenden um komplett raus zu kommen 
	 * die Credits anschauen(wer das Spiel erstellt hat)
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		neuesSpiel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				generateNewScene("LoginBildschirm.fxml", "Sie koennen nun die Spieleranzahl eingeben!");
			}
		});

		spielLaden.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				generateNewScene("SpielLadenBildschirm.fxml", "Waehlen Sie nun Ihren zu ladenen Spielstand");
			}
		});

		spielBeenden.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});

		credits.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				try {
					BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("CreditsBildschirm.fxml"));
					Scene sc = new Scene(root);
					Main.stage.setFullScreen(true);
					Main.stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
					Main.stage.initStyle(StageStyle.UNDECORATED);
					Main.stage.setScene(sc);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

	}

	private void generateNewScene(String fileName, String anzeigeNachricht) {
		try {
			VBox root = (VBox) FXMLLoader.load(getClass().getResource(fileName));
			Scene sc = new Scene(root);
			Main.stage.setScene(sc);
			Main.stage.setFullScreen(true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
