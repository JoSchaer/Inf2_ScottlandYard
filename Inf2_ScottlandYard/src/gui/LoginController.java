package gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.sun.javafx.application.LauncherImpl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.SplitPane;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LoginController implements Initializable {
	/**
	 * Labels und Buttons
	 */
	@FXML
	private Button weiter;
	@FXML
	private Label mrXText;
	@FXML
	private ComboBox<Integer> spieleranzahl;
	@FXML
	private ComboBox<String> regelwerk;
	@FXML
	private Button beenden;
	@FXML
	private Button zurueck;
	
	@FXML
	private Label error;
	
	@FXML
	private CheckBox ki_Ja_Nein;
	
	private boolean checkbox_geklickt = false;

	public ComboBox<Integer> getSpieleranzahl() {
		return spieleranzahl;
	}
	/**
	 * Initializiert das LoginFenster:
	 * Anzahl der Spieler als Auswahl 
	 * weiter Button
	 * zurueck Button
	 * beenden Button (vollstaendiges Verlassen des Spiels)
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		weiter.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (spieleranzahl.getValue() != null && regelwerk.getValue() != null) {
					Main.spielerAnzahl = ((Integer) spieleranzahl.getValue());
					Main.dc.setRegelwerkTyp(regelwerk.getValue());
					System.out.println(Main.spielerAnzahl);
					try {
						BorderPane root = (BorderPane) FXMLLoader
								.load(getClass().getResource("SpielerLoginBildschirm.fxml"));
						Scene sc = new Scene(root);
						Main.stage.setScene(sc);
						Main.stage.setFullScreen(true);
						Main.stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
						Main.dc.setRealeSpieler(Main.spielerAnzahl);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					error.setText("Fehlerhafte Eingabe!\n Nochmal versuchen.");
					error.setVisible(true);
				}
			}
		});

		beenden.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});

		zurueck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					VBox root = (VBox) FXMLLoader.load(getClass().getResource("StartBildschirm.fxml"));
					Scene sc = new Scene(root);
					Main.stage.setScene(sc);
					Main.stage.setFullScreen(true);
					Main.stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
					Main.stage.initStyle(StageStyle.UNDECORATED);
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		});
		
		ki_Ja_Nein.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent arg0) {
				if(!checkbox_geklickt) {
					ki_Ja_Nein.setStyle("-fx-background-color: rgb(0,64,0);");
					ki_Ja_Nein.setText("MrX wird von der KI gespielt.");
					checkbox_geklickt = true;
					Main.dc.setKIStatus(true);
				}
				else {
					ki_Ja_Nein.setStyle("-fx-background-color: rgb(63,63,63)");
					ki_Ja_Nein.setText("MrX wird vom Spieler gespielt.");
					checkbox_geklickt = false;
					Main.dc.setKIStatus(false);
				}
				
			}
		});

		// ComboBox
		spieleranzahl.setPromptText("Spieleranzahl");
		spieleranzahl.getItems().addAll(3,4,5,6);
	
		//Regelwerke auswahl
		regelwerk.getItems().addAll("Standard","Spezial Sonder Edition");

	}

}
