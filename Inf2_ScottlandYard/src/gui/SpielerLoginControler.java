package gui;

import java.awt.BorderLayout;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.StageStyle;
import zusatsKlassen.SpielerFarben;

public class SpielerLoginControler implements Initializable {

	/**
	 * FXML Komponente
	 */
	@FXML
	private Label deteText;
	@FXML
	private TextField spielername;
	@FXML
	private ComboBox<SpielerFarben> spielerfarbe;
	@FXML
	private Button bestaetigen;
	@FXML
	private Button zurueck;
	@FXML
	private Button beenden;
	@FXML
	private Label eingeloggt;
	@FXML
	private Label error;
	@FXML
	private Label spieler;
	@FXML
	private Label mrx;
	@FXML
	private VBox vbox;

	private int spieleranzahl = Main.spielerAnzahl;

	private ObservableList<SpielerFarben> spielerfarben = FXCollections.observableArrayList();
	
	
	/**
	 * Diese initialisiert den BestaetigenButton, dieser wird gedr�ckt, nachdem Name und Farbe eingegeben wurde.
	 * Drueckt man auf bestaetigen, nachdem alle Spieler eingelogt sind, geht es weiter zum Spielfeld.
	 * Wenn kein Name oder falsche Zeichen dabei sind oder keine Farbe ausgew�hlt wurde, kommt eine Fehlermeldung.
	 * Initialisiert einen zurueck Button, der auf die vorherige Seite springt.
	 * Initialisiert einen beenden Button, dieser beendet das Fenster/Spiel.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		
		spielerfarben.addAll(SpielerFarben.values());
		Main.dc.getSpielernamenFuerSpielerstellung().add("MrX");
		Main.dc.getFarbenFuerSpielerstellung().add(SpielerFarben.DURCHSICHTIG);
		spielerfarbe.setPromptText("Farbe w�hlen");
		spielerfarbe.getItems().addAll(spielerfarben);
		spielerfarbe.getItems().remove(SpielerFarben.DURCHSICHTIG);
		spielername.setPromptText("Name eingeben");

		bestaetigen.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				error.setVisible(false);
				if (spieleranzahl > 1) {
					if ((spielername.getText().matches("\\w{2,}"))) {
						Main.dc.getSpielernamenFuerSpielerstellung().add(spielername.getText());
						if (spielerfarbe.getValue() != null) {
							Main.dc.getFarbenFuerSpielerstellung().add(spielerfarbe.getValue());
							
							Label label = new Label();
							label.setText(spielername.getText() + " ist " + spielerfarbe.getValue());
							label.setVisible(true);
							label.getStyleClass().add("general");
							spielerfarbe.getItems().remove(spielerfarbe.getValue());
							vbox.getChildren().add(label);
							label.setId("einlogglabel");

							if(spieleranzahl == 2) {
								try {
									BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
											
									Scene sc = new Scene(root);
									Main.stage.setScene(sc);
									Main.stage.setFullScreen(true);
									//Main.dc.wechselZuHauptMusik();
									
								} catch (IOException e) {
									e.printStackTrace();
								}
								
							}else {
								spieleranzahl--;
								spielername.setText("");
								spielerfarbe.setValue(null);
							}
						} else {
							error.setText("Falsche Eingabe, bitte erneut eingeben!");
							error.setVisible(true);
						}
					} else {
						error.setText("Falsche Eingabe, bitte erneut eingeben!");
						error.setVisible(true);

					}
				} else {
					try {
						BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
						Scene sc = new Scene(root);
						Main.stage.setScene(sc);
						Main.stage.setFullScreen(true);
						Main.stage.initStyle(StageStyle.UNDECORATED);
						Main.dc.wechselZuHauptMusik();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		});

		zurueck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					VBox root = (VBox) FXMLLoader.load(getClass().getResource("LoginBildschirm.fxml"));
					Scene sc = new Scene(root);
					Main.stage.setScene(sc);
					Main.stage.setFullScreen(true);
					Main.stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		beenden.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
			}
		});

		
		
		
	}

}
