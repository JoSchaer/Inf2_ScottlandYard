package gui;

import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import spielleiter.Spielleiter;
import spielleiter.iControllerGUI;
import vars.iDataAccess;
import zusatsKlassen.ErzeugeButtons;
import zusatsKlassen.SpielerFarben;
import zusatsKlassen.VerkehrsFarben;

public class MainController implements Initializable {
	/**
	 * Variable
	 */
	private iControllerGUI controller;
	private iDataAccess data;
	private ArrayList<String> spielernamen = Main.dc.getSpielernamenFuerSpielerstellung();
	private ArrayList<SpielerFarben> spielerfarben = Main.dc.getFarbenFuerSpielerstellung();
	private HashSet<VerkehrsFarben> alleKonotenfarben;

	private Button[] felder = new Button[199];
	private Button[] spielfiguren;
	private VBox[] labelcontainer = new VBox[11];
	private Button zivilist1 = new Button();
	private Button zivilist2 = new Button();

	private double zielButtonX = 0;
	private double zielButtonY = 0;
	private double heightGes = 0;
	private double widthGes = 0;
	private int zielfeld = 0;
	private Button start;
	private String methode;
	private SpielerFarben aktuellerSpieler;
	private Button startpunkt2;
	private boolean startpunkt2schongesetzt = false;
	private FadeTransition ft;
	private ScaleTransition st;
	private RotateTransition rt;

	/**
	 * Alle FXML Komponenten
	 */
	@FXML
	private MenuBar menuBar;
	@FXML
	private Menu sounds;
	@FXML
	private Pane spielfeld;
	@FXML
	private BorderPane root;
	@FXML
	private HBox footer;
	@FXML
	private Button informaTiger;
	@FXML
	private ButtonType tigerClose;
	@FXML
	private Menu spiel;
	@FXML
	private Menu help;
	@FXML
	private StackPane spielfeldContainer;
	@FXML
	private Label spielerfarbe;
	@FXML
	private HBox fahrtafel;

	/**
	 * Komponenten des Credits PopUp
	 */
	private String credits = "Dieses sagenhafte Spiel wurde eintwickelt von:\r\n" + "Celina Schreiner MKIB2\r\n"
			+ "Daniel Melnicki MKIB3\r\n" + "Evelyn Krebes MKIB3\r\n" + "Jonathan Schaertel MKIB3\r\n" + "\r\n"
			+ "Das Copyright obliegt den oben genannten Personen!\r\n" + "November 2017." + "\r\n"
			+ "Die Musik stammt von:" + "\r\n" + "https://www.frametraxx.de/info/kostenlose-gemafreie-musik.html";

	@FXML
	private ButtonType creditsClose;
	/**
	 * Zusatz Komponenten
	 */

	private Slider lautstaerke = new Slider(0, 1, 0.5);
	private Label leise = new Label();
	private Label laut = new Label();
	private RadioButton mute = new RadioButton("Stumm schalten");
	private Button back = new Button();
	private Button next = new Button();
	private Button play_pause = new Button();

	private HBox buttonControll = new HBox(back, next, play_pause);
	private HBox lautstaerkeContainer = new HBox(leise, lautstaerke, laut);

	private CustomMenuItem muteItem = new CustomMenuItem(mute);
	private CustomMenuItem sliderItem = new CustomMenuItem(lautstaerkeContainer);
	// private CustomMenuItem musicControll = new CustomMenuItem(buttonControll);

	/**
	 * Klassen zum erzeugen/verwalten der Buttons
	 */
	private ErzeugeButtons buttonerzeuger = new ErzeugeButtons();
	private ArrayList<Button> buttons = new ArrayList<>();

	/**
	 * Ueberschreibung der initialize Methode, laden der eigenen MenuBar Items
	 * Erscheinen des Willkommens Pop Up beim Oeffnen des Spielfelds
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		System.out.println(Main.dc.getRealeSpieler());

		for (SpielerFarben akt : Main.dc.getFarbenFuerSpielerstellung()) {
			System.out.println("Farbe ist: " + akt.toString());
		}
		for (String name : Main.dc.getSpielernamenFuerSpielerstellung()) {
			System.out.println("Name ist: " + name);
		}

		if (Main.dc.getSpielleiter() == null) {
			controller = new Spielleiter(Main.dc.getRealeSpieler(), Main.dc.getSpielernamenFuerSpielerstellung(),
					Main.dc.getFarbenFuerSpielerstellung(), Main.dc.getRegelwerkTyp(), "GUI");
		} else {
			controller = Main.dc.getSpielleiter();
		}

		try {
			Thread.sleep(200);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		// Hier wird die Fahrtafel erstellt
		int[][] ids = { { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21 }, { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22 } };
		for (int i = 0; i < labelcontainer.length; i++) {
			labelcontainer[i] = new VBox();
			Label[] labels = new Label[2];
			for (int j = 0; j < labels.length; j++) {
				labels[j] = new Label(ids[j][i] + ": ");
				labels[j].setId("" + ids[j][i]);
				if (j == 0) {
					labels[j].getStyleClass().add("ungeradeFahrt");
				} else {
					labels[j].getStyleClass().add("geradeFahrt");
				}

			}
			labelcontainer[i].getChildren().addAll(labels);
		}

		fahrtafel.getChildren().addAll(labelcontainer);

		for (MenuItem item : spiel.getItems()) {
			if (item.getText().equalsIgnoreCase("...beenden")) {
				item.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.exit(0);
					}
				});
			} else if (item.getText().equalsIgnoreCase("...neues Spiel")) {
				// Fragen, ob wirklich ein neues Spiel erstellt werden soll und dann machen was
				// gesagt wurde
			} else if (item.getText().equalsIgnoreCase("...speichern")) {
				item.setOnAction(new EventHandler<ActionEvent>() {

					public void handle(ActionEvent event) {
						FileChooser fileChooser = new FileChooser();
						fileChooser.setTitle("speichern");
						File file = fileChooser.showSaveDialog(Main.stage);
						fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Serialize", "*.ser*"),
								new FileChooser.ExtensionFilter("JSON", "*.json"),
								new FileChooser.ExtensionFilter("CVS", "*.cvs"));
						if (file != null) {
							try {
								if (file.getPath().endsWith(".ser")) {
									((Spielleiter) controller).serializeSave(file.getPath());
								} else if (file.getPath().endsWith(".json")) {
									((Spielleiter) controller).jsonSave(file.getPath());
								} else if (file.getPath().endsWith(".csv")) {
									((Spielleiter) controller).saveCSV(file.getPath());
								}
							} catch (Exception e) {
								e.printStackTrace();

							}
						}
					}
				});
			}
		}

		for (MenuItem item : help.getItems()) {
			if (item.getText().equalsIgnoreCase("Credits")) {
				item.setOnAction(new EventHandler<ActionEvent>() {
					public void handle(ActionEvent event) {
						creditsPopUp();
					}
				});
			} else if (item.getText().equalsIgnoreCase("Spielregeln")) {
				item.setOnAction(new EventHandler<ActionEvent>() {
					public void handle(ActionEvent event) {
						hilfePopUp();
					}
				});
			}
		}

		informaTiger.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				hilfePopUp();
			}
		});

		mute.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Main.dc.mute();
			}
		});
		lautstaerke.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				Main.dc.lautstaerke(lautstaerke.getValue());
			}
		});

		// Menu Punkt Sounds mit selbst erstellten MunuItems bestuecken
		// sounds.getItems().addAll(muteItem, sliderItem, musicControll);
		sounds.getItems().addAll(muteItem, sliderItem);
		muteItem.getStyleClass().add("items");
		sliderItem.getStyleClass().add("items");
		// musicControll.getStyleClass().add("items");
		leise.getStyleClass().add("leiser");
		laut.getStyleClass().add("lauter");

		willkommenPopUp();
	}

	/**
	 * Durch geht den Spielablauf: - Dialog: Alle muessen weg schaun damit MrX
	 * spielen kann - Dialog: Mrx kann waehlen zwischen einem Einfachen oder
	 * Doppelten Zug - sucht sich aus wo er hin moechte und kann dies dann mit dem
	 * Verkehrsmittel fahren - Detetik kann auch jeweils seinen Zug machen mit der
	 * Auswahl des Zielfeldes
	 */
	public void spielablauf() {
		aktuellerSpieler = controller.darfZiehen();
		spielerfarbe.setStyle("-fx-background-color:rgb(" + erzeugeFarbeRGB(aktuellerSpieler) + ");");
		if (Main.dc.getRegelwerkTyp().equalsIgnoreCase("Spezial Sonder Edition")) {
			controller.erstelleHinweise(getSpielfigurMitFarbe(aktuellerSpieler).getLayoutX(),
					getSpielfigurMitFarbe(aktuellerSpieler).getLayoutY(),
					getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutX(),
					getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutY());
		}
		if (aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG)) {
			Alert lookaway = new Alert(AlertType.WARNING);
			lookaway.initModality(Modality.APPLICATION_MODAL);
			lookaway.initOwner(Main.stage);
			lookaway.setHeaderText("It's your turn, MrX.");
			lookaway.setContentText(
					"Detektive weg schauen! MrX ist an der Reihe." + "\r\n Vorsicht, nicht dass Sie erwischt werden!");

			lookaway.showAndWait();
			showMrX();

			ArrayList<Object> spielerinfo = controller.spielerInfo();
			// Abfrage ob Doppelzug noch geht
			if (!Main.dc.getKIStatus()) {
				if ((Integer) spielerinfo.get(spielerinfo.size() - 1) > 0) { // Alert Fenster von MrX Alert

					Alert alert = new Alert(AlertType.NONE);
					alert.setContentText("MrX,\r\n" + "Waehlen Sie Ihre Zug Art:\r\n" + "");
					ButtonType move = new ButtonType("Einfacher Zug");
					ButtonType doppel = new ButtonType("Doppelzug");
					alert.getButtonTypes().clear();
					alert.getButtonTypes().addAll(move, doppel);
					alert.initOwner(Main.stage);

					if (alert.showAndWait().get().getText().equalsIgnoreCase("Einfacher Zug")) {
						Main.dc.setMethode("move");
						methode = Main.dc.getMethode();
						makeTheMove(methode);
					}

					else {
						Main.dc.setMethode("doppelzug");
						methode = Main.dc.getMethode();
						makeTheMove(methode);

					}
				} else {
					methode = "move";
					this.makeTheMove(methode);

				}
			} else {
				methode = "move";
				this.makeTheMove(methode);
			}
		} else

		{
			methode = "move";
			this.makeTheMove(methode);

		}
	}

	/**
	 * MrX ist immer nicht sichtbar Hilfsmethode so dass er auftaucht mit einer
	 * Animation
	 */
	public void makeMrXVisible() {
		Button mrX = getSpielfigurMitFarbe(aktuellerSpieler);
		ft = new FadeTransition(Duration.millis(500), mrX);
		st = new ScaleTransition(Duration.millis(500), mrX);
		rt = new RotateTransition(Duration.millis(500), mrX);
		int wdh = 60;

		ft.setFromValue(1.0);
		ft.setToValue(0.5);
		ft.setCycleCount(wdh);
		ft.setAutoReverse(true);

		st.setByX(2);
		st.setByY(2);
		st.setCycleCount(wdh);
		st.setAutoReverse(true);

		rt.setByAngle(360);
		rt.setCycleCount(wdh);
		// rotate.setAutoReverse(true);

		ft.play();
		st.play();
		rt.play();

		getSpielfigurMitFarbe(aktuellerSpieler).setVisible(true);

	}

	public void showMrX() {
		Button mrX = getSpielfigurMitFarbe(aktuellerSpieler);
		FadeTransition enter = new FadeTransition(Duration.seconds(3), mrX);
		enter.play();
		mrX.setVisible(true);
	}

	/**
	 * Hilsmethode um den Move eines Spielers zu bestimmen Die moeglichen Zielfelder
	 * koennen an geklickt werden und koenne dann befahren werden von dem Spieler
	 * sollte es ein doppelzug sein werden auch die weiteren moeglichen Felder
	 * 
	 * @param methode
	 */
	public void makeTheMove(String methode) {
		if (Main.dc.getKIStatus() && aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG)) {
			// Backend Moved die KI
			int gezogenesFeld = controller.makeKiMove();

			// Visuelle Verschiebung

			Button ziel = sucheButtonMitID(gezogenesFeld);
			// Ticketinfo aktuallisieren
			getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getTooltip()
					.setText(controller.spielergebundeneInformationen(aktuellerSpieler));

			// Fahrtafel aktualisieren
			Label fahrt = sucheFahrtafelMitRundencounter(controller.rundencounter());
			fahrt.setText(fahrt.getText() + getVerkehrsmittelVonFarbe(controller.getLastTicketMrX()));

			getSpielfigurMitFarbe(aktuellerSpieler).setLayoutX(ziel.getLayoutX() - 5);
			getSpielfigurMitFarbe(aktuellerSpieler).setLayoutY(ziel.getLayoutY() - 50);

			if (controller.rundencounter() == 3 || controller.rundencounter() == 8 || controller.rundencounter() == 13
					|| controller.rundencounter() == 18) {
				makeMrXVisible();
				getSpielfigurMitFarbe(aktuellerSpieler).setVisible(true);

			} else {
				getSpielfigurMitFarbe(aktuellerSpieler).setVisible(false);
			}

			spielablauf();

		} else {
			Object[] knoteninfo = controller.getSpielerKnoten(methode);
			ArrayList<Object> startknotenInfo = (ArrayList<Object>) knoteninfo[0];
			ArrayList<Object> nachbarknotenInfo = (ArrayList<Object>) knoteninfo[1];
			Main.dc.setNachbarknotenInfosZug1(nachbarknotenInfo);
			alleKonotenfarben = new HashSet<>();
			start = sucheButtonMitID((int) startknotenInfo.get(0));
			Button nachbar = null;
			ArrayList<VerkehrsFarben> farbenNachbar = new ArrayList<>();
			if (methode.equalsIgnoreCase("doppelzug")) {
				for (Object o : startknotenInfo) {
					if (o instanceof VerkehrsFarben) {
						alleKonotenfarben.add((VerkehrsFarben) o);
					}
				}

				for (Object o : nachbarknotenInfo) {

					if (o instanceof Integer) {
						if (farbenNachbar.size() > 0) {
							erzeugeGradient(nachbar, farbenNachbar);
							farbenNachbar.clear();
						}

						nachbar = sucheButtonMitID((Integer) o);
						nachbar.setDisable(false);
					} else if (o instanceof VerkehrsFarben) {
						if (alleKonotenfarben.contains(o)) {
							farbenNachbar.add((VerkehrsFarben) o);
						}
					}
				}
				// Wenn der zweite Knoten angeklickt wurde
				if (startpunkt2schongesetzt) {
					System.out.println("ICH BIN BEIM ZWEITEN MAL");
					resetValues();
					ArrayList<Object[]> nachbarnachbar = (ArrayList<Object[]>) knoteninfo[2];
					Object[] startknoten2Infos = null;
					HashSet<VerkehrsFarben> farbenZweiterKnoten = new HashSet<>();
					Button nachbarZwei = null;
					ArrayList<VerkehrsFarben> farbenDesNachbars = new ArrayList<>();

					// NahcbarknotenInfos, des zweiten Startknotens finden

					sucheNachbarknotenInfos: for (Object[] aktNachbar : nachbarnachbar) {
						Integer nachbarid = (Integer) ((ArrayList<Object>) aktNachbar[0]).get(0);
						if (nachbarid == Integer.parseInt(startpunkt2.getId())) {
							startknoten2Infos = aktNachbar;
							break sucheNachbarknotenInfos;
						}
					}

					ArrayList<Object> start = (ArrayList<Object>) startknoten2Infos[0];
					ArrayList<Object> ziel = (ArrayList<Object>) startknoten2Infos[1];
					Main.dc.setNachbarknotenInfosZug2(ziel);
					for (Object o : start) {
						if (o instanceof VerkehrsFarben) {
							farbenZweiterKnoten.add((VerkehrsFarben) o);
						}
					}

					for (Object o : ziel) {

						if (o instanceof Integer) {
							if (farbenDesNachbars.size() > 0) {
								erzeugeGradient(nachbarZwei, farbenDesNachbars);
								farbenDesNachbars.clear();
							}

							nachbarZwei = sucheButtonMitID((Integer) o);
							nachbarZwei.setDisable(false);
						} else if (o instanceof VerkehrsFarben) {
							if (farbenZweiterKnoten.contains(o)) {
								farbenDesNachbars.add((VerkehrsFarben) o);
							}
						}
					}

				}
			} else if (methode.equals("move")) {
				for (Object o : startknotenInfo) {
					if (o instanceof VerkehrsFarben) {
						alleKonotenfarben.add((VerkehrsFarben) o);
					}
				}

				for (Object o : nachbarknotenInfo) {

					if (o instanceof Integer) {
						if (farbenNachbar.size() > 0) {
							erzeugeGradient(nachbar, farbenNachbar);
							farbenNachbar.clear();
						}

						nachbar = sucheButtonMitID((Integer) o);
						nachbar.setDisable(false);
					} else if (o instanceof VerkehrsFarben) {
						if (alleKonotenfarben.contains(o)) {
							farbenNachbar.add((VerkehrsFarben) o);
						}
					}
				}
			}
		}
	}

	/**
	 * Gibt den moeglichen Zielfeldern die Farbe mit welchem Verkehrsmittel sie
	 * befahren werden koennen und faerbt diese mit einem Gradient
	 * 
	 * @param nachbar
	 * @param farbenZumFahren
	 */
	public void erzeugeGradient(Button nachbar, ArrayList<VerkehrsFarben> farbenZumFahren) {
		String prozentWeiss = "0%";
		String prozentRot = "0%";
		String prozentGruen = "0%";
		String prozentSchwarz = "0%";

		boolean weiss = false;
		boolean gruen = false;
		boolean rot = false;
		// wollen das die buttons einen gradient haben mit den farben mit denen man es
		// befahren kann
		// geht noch nicht ganz
		for (VerkehrsFarben farbe : farbenZumFahren)
			switch (farbe) {
			case WEISS:
				weiss = true;
				prozentWeiss = "100%";
				break;
			case GRUEN:
				if (weiss) {
					prozentWeiss = "50%";
					prozentGruen = "50%";
				} else {
					prozentGruen = "100%";
				}
				gruen = true;
				break;
			case ROT:
				if (weiss && gruen) {
					prozentWeiss = "34%";
					prozentGruen = "33%";
					prozentRot = "33%";
				} else if (gruen) {
					prozentRot = "50%";
					prozentGruen = "50%";
				} else if (weiss) {
					prozentWeiss = "50%";
					prozentRot = "50%";
				} else {
					prozentRot = "100%";
				}
				rot = true;

				break;
			case SCHWARZ:
				if (weiss && gruen) {
					prozentWeiss = "34%";
					prozentGruen = "33%";
					prozentSchwarz = "33%";
				} else if (weiss && rot) {
					prozentWeiss = "34%";
					prozentRot = "33%";
					prozentSchwarz = "33%";
				} else if (weiss && gruen & rot) {
					prozentWeiss = "25%";
					prozentRot = "25%";
					prozentSchwarz = "25%";
					prozentGruen = "25%";
				} else if (weiss) {
					prozentWeiss = "50%";
					prozentSchwarz = "50%";
				} else if (gruen) {
					prozentGruen = "50%";
					prozentSchwarz = "50%";
				} else if (rot) {
					prozentRot = "50%";
					prozentSchwarz = "50%";
				} else {
					prozentSchwarz = "10%";
				}

			}

		String gradient = "-fx-background-color: linear-gradient(#ffffff " + prozentWeiss + ", #FF0000 " + prozentRot
				+ ", #000000 " + prozentSchwarz + ", #008000 " + prozentGruen + ");";
		nachbar.setStyle(gradient);

	}

	/**
	 * Erstellt den Dialog fuer die moeglichen Verkehrsmittel erkennt welches
	 * gewaehlt wurde und zieht mit diesem und setzt dieses Verkehrsmittel MrX seine
	 * Verkehrsmittel werden zur Fahrtafel hinzugefuegt
	 */

	public void buttonIsPressed() {
		Main.dc.setStartKnoten(start.getLayoutX(), start.getLayoutY());
		Main.dc.setZielKnoten(zielButtonX, zielButtonY);
		Main.dc.setKnotenInfo(controller.getSpielerKnoten(methode));
		Main.dc.setSpielerinfo(controller.spielerInfo());

		// Move Pop Up als Dialog

		if (methode.equalsIgnoreCase("doppelzug")) {
			System.out.println("Zeile 596");
			erzeugeVerkehrsmittelWahl(Main.dc.getNachbarknotenInfosZug1());
			VerkehrsFarben verkehrsmittel1 = Main.dc.getVerkehrsmittel();
			System.out.println("Zeile 599");
			makeTheMove(methode);
			System.out.println("Zeile 601");
			erzeugeVerkehrsmittelWahl(Main.dc.getNachbarknotenInfosZug2());
			VerkehrsFarben verkehrsmittel2 = Main.dc.getVerkehrsmittel();
			System.out.println("Zeile 604");

			if (controller.doppelzugMrX(Integer.parseInt(start.getId()), verkehrsmittel1, zielfeld, verkehrsmittel2)) {
				getSpielfigurMitFarbe(aktuellerSpieler).getTooltip()
						.setText(controller.spielergebundeneInformationen(aktuellerSpieler));
				if (aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG)) {
					Label fahrt = sucheFahrtafelMitRundencounter(controller.rundencounter());
					fahrt.setText(fahrt.getText() + getVerkehrsmittelVonFarbe(controller.getLastTicketMrX()));
				}
				getSpielfigurMitFarbe(aktuellerSpieler).setLayoutX(zielButtonX - 5);
				getSpielfigurMitFarbe(aktuellerSpieler).setLayoutY(zielButtonY - 50);

				if (aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG)) {
					if (controller.rundencounter() == 3 || controller.rundencounter() == 8
							|| controller.rundencounter() == 13 || controller.rundencounter() == 18) {
						makeMrXVisible();
						getSpielfigurMitFarbe(aktuellerSpieler).setVisible(true);

					} else {
						getSpielfigurMitFarbe(aktuellerSpieler).setVisible(false);
					}
				}
			}
		} else if (methode.equalsIgnoreCase("move")) {
			erzeugeVerkehrsmittelWahl(Main.dc.getNachbarknotenInfosZug1());
			if (controller.movePlayer(zielfeld, Main.dc.getVerkehrsmittel())) {
				getSpielfigurMitFarbe(aktuellerSpieler).getTooltip()
						.setText(controller.spielergebundeneInformationen(aktuellerSpieler));
				if (aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG)) {
					Label fahrt = sucheFahrtafelMitRundencounter(controller.rundencounter());
					fahrt.setText(fahrt.getText() + getVerkehrsmittelVonFarbe(controller.getLastTicketMrX()));
				}
				getSpielfigurMitFarbe(aktuellerSpieler).setLayoutX(zielButtonX - 5);
				getSpielfigurMitFarbe(aktuellerSpieler).setLayoutY(zielButtonY - 50);

				if (aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG)) {
					if (controller.rundencounter() == 3 || controller.rundencounter() == 8
							|| controller.rundencounter() == 13 || controller.rundencounter() == 18) {
						makeMrXVisible();
						getSpielfigurMitFarbe(aktuellerSpieler).setVisible(true);

					} else {
						getSpielfigurMitFarbe(aktuellerSpieler).setVisible(false);
					}
				}
			}
		}

		resetValues();
		if (Main.dc.getRegelwerkTyp().equalsIgnoreCase("Spezial Sonder Edition")) {
			checkZivilist();
		}
		spielablauf();
	}

	// TODO Text muss noch hinzugef�gt werden, Zivilisten m�ssen verschwinden.

	public void checkZivilist() {
		if (controller.getSpielfeldZahl() == Integer.parseInt(zivilist1.getId().split(",")[1])) {
			Button sprechblase = new Button();

			if (controller.bekommeSpielerFarbeVonVorherigemSpieler().equals(SpielerFarben.DURCHSICHTIG)) {
				System.out.println("Hinweis wurde manipuliert bei Zivi 1");
			} else {

				sprechblase.setLayoutX(zivilist1.getLayoutX() + zivilist1.getWidth());
				sprechblase.setLayoutY(zivilist1.getLayoutY());
				// sprechblase.getStyleClass().add("sprechblase");
				controller.erstelleHinweise(zivilist1.getLayoutX(), zivilist1.getLayoutY(),
						getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutX(),
						getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutY());
				sprechblase.setText(controller.bekommeHinweis("zivi1"));
				sprechblase.setTextAlignment(TextAlignment.CENTER);
				sprechblase.setVisible(true);
				sprechblase.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						spielfeld.getChildren().remove(zivilist1);

						spielfeld.getChildren().remove(sprechblase);
						generiereNeuenZivilisten(zivilist1);

					}
				});
				spielfeld.getChildren().add(sprechblase);
			}
		} else if (controller.getSpielfeldZahl() == Integer.parseInt(zivilist2.getId().split(",")[1])) {
			Button sprechblase = new Button();
			if (controller.bekommeSpielerFarbeVonVorherigemSpieler().equals(SpielerFarben.DURCHSICHTIG)) {
				System.out.println("Hinweis wurde manipuliert bei Zivi 2");
				spielfeld.getChildren().remove(zivilist2);
			} else {

				sprechblase.setLayoutX(zivilist2.getLayoutX() + zivilist2.getWidth());
				sprechblase.setLayoutY(zivilist2.getLayoutY());
				// sprechblase.getStyleClass().add("sprechblase");
				controller.erstelleHinweise(zivilist1.getLayoutX(), zivilist1.getLayoutY(),
						getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutX(),
						getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutY());
				sprechblase.setText(controller.bekommeHinweis("zivi2"));
				sprechblase.setTextAlignment(TextAlignment.CENTER);
				sprechblase.setVisible(true);
				sprechblase.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent event) {
						spielfeld.getChildren().remove(zivilist2);
						spielfeld.getChildren().remove(sprechblase);
						generiereNeuenZivilisten(zivilist2);

					}

				});
				spielfeld.getChildren().add(sprechblase);
			}

		}
	}

	public void erzeugeVerkehrsmittelWahl(ArrayList<Object> nachbarknoteninfos) {
		Alert move = new Alert(AlertType.INFORMATION);
		// Dialog steht ueber der Main ohne dass der Fullscreen verlassen wird
		move.initOwner(Main.stage);
		move.initModality(Modality.APPLICATION_MODAL);
		move.initStyle(StageStyle.UNDECORATED);
		move.getButtonTypes().clear();
		// Buttons neu definieren
		ArrayList<ButtonType> erzeugteButtons = erzeugeButtontypes(nachbarknoteninfos);
		move.getButtonTypes().addAll(erzeugteButtons);
		move.setContentText("Waehlen Sie ein Verkehrsmittel");
		ButtonType pressed = move.showAndWait().get();
		if (pressed.getText().equalsIgnoreCase("Taxi")) {
			Main.dc.setVerkehrsmittel(VerkehrsFarben.WEISS);
		} else if (pressed.getText().equalsIgnoreCase("Bus")) {
			Main.dc.setVerkehrsmittel(VerkehrsFarben.GRUEN);
		} else if (pressed.getText().equalsIgnoreCase("Bahn")) {
			Main.dc.setVerkehrsmittel(VerkehrsFarben.ROT);
		} else if (pressed.getText().equalsIgnoreCase("BlackTicket")) {
			Main.dc.setVerkehrsmittel(VerkehrsFarben.SCHWARZ);
		}
	}

	public void whoWon(Button spielerButton) {
		if (!aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG)) {
			if (spielerButton.getLayoutX() == getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutX() + 5
					&& spielerButton.getLayoutY() == getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).getLayoutY()
							+ 50) {
				try {
					Alert mrX = new Alert(AlertType.INFORMATION);
					mrX.setContentText("MrX hat hart verkackt. LUTSCHER!!!");
					mrX.setHeaderText("Herzlichen Glueckwunsch!");
					ButtonType neuesSpiel = new ButtonType("Neues Spiel");
					ButtonType exit = new ButtonType("Spiel Beenden");
					if (mrX.showAndWait().get().getText().equals("Neues Spiel")) {
						VBox root = (VBox) FXMLLoader.load(getClass().getResource("LoginBildschirm.fxml"));
						Scene sc = new Scene(root);
						Main.stage.setScene(sc);
						Main.stage.setFullScreen(true);
						mrX.close();
					} else if (mrX.showAndWait().get().getText().equals("Spiel Beenden")) {
						System.exit(0);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

		if (controller.hasWon() != 0) {
			if (aktuellerSpieler.equals(SpielerFarben.DURCHSICHTIG) && controller.hasWon() == 1) {
				try {
					Alert mrX = new Alert(AlertType.INFORMATION);
					mrX.setContentText("Sie wurden nicht geschnappt!");
					mrX.setHeaderText("Herzlichen Glueckwunsch!");
					ButtonType neuesSpiel = new ButtonType("Neues Spiel");
					ButtonType exit = new ButtonType("Spiel Beenden");
					if (mrX.showAndWait().get().getText().equals("Neues Spiel")) {
						VBox root = (VBox) FXMLLoader.load(getClass().getResource("LoginBildschirm.fxml"));
						Scene sc = new Scene(root);
						Main.stage.setScene(sc);
						Main.stage.setFullScreen(true);
						mrX.close();
					} else if (mrX.showAndWait().get().getText().equals("Spiel Beenden")) {
						System.exit(0);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (controller.hasWon() == 2) {
				try {
					Alert dete = new Alert(AlertType.INFORMATION);
					dete.setHeaderText("Herzlichen Glueckwunsch");
					dete.setContentText("Sie haben erfolgreich MrX geschnappt!!");
					ButtonType neuesSpiel = new ButtonType("Neues Spiel");
					ButtonType exit = new ButtonType("Spiel Beenden");
					if (dete.showAndWait().get().getText().equals("Neues Spiel")) {
						VBox root = (VBox) FXMLLoader.load(getClass().getResource("LoginBildschirm.fxml"));
						Scene sc = new Scene(root);
						Main.stage.setScene(sc);
						Main.stage.setFullScreen(true);
						dete.close();
					} else if (dete.showAndWait().get().getText().equals("Spiel Beenden")) {
						System.exit(0);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Gibt die Verkehrsfarben als String wieder zurueck
	 * 
	 * @param letzteGenutzteFarbe
	 * @return Verkehrsfarbe die genutzt wurde als String
	 */
	public String getVerkehrsmittelVonFarbe(VerkehrsFarben letzteGenutzteFarbe) {
		switch (letzteGenutzteFarbe) {
		case WEISS:

			return "Taxi";
		case GRUEN:

			return "Bus";
		case ROT:

			return "Bahn";
		case SCHWARZ:

			return "Black";
		}
		return "Not Vislible";
	}

	/**
	 * Zaehlt die Runden
	 * 
	 * @param rundencounter
	 * @return Ladel der Fahrtafel
	 */
	public Label sucheFahrtafelMitRundencounter(int rundencounter) {
		for (int i = 0; i < labelcontainer.length; i++) {
			for (int j = 0; j < labelcontainer[i].getChildren().size(); j++) {
				if (labelcontainer[i].getChildren().get(j).getId().equals("" + rundencounter)) {
					return (Label) labelcontainer[i].getChildren().get(j);
				}
			}
		}

		return null;
	}

	/**
	 * Erzeugt alle Buttons (Knoten des Graphens als Buttons)
	 * 
	 * @return alle Buttons auf denen die Spieler sitzen koennen
	 */
	public ArrayList<ButtonType> erzeugeButtontypes(ArrayList<Object> nachbarknoteninfos) {

		ArrayList<ButtonType> fahrButtons = new ArrayList<>();
		ArrayList<Object> nachbarknotenInfo = nachbarknoteninfos;
		int indexVonZielknoten = 0;
		{
			/*
			 * Move des Jahrtausends sucheGeklicktenKnoten ist der Name der Schleife
			 */
			sucheGeklicktenKnoten: for (int i = 0; i < nachbarknotenInfo.size(); i++) {
				if (nachbarknotenInfo.get(i) instanceof Integer) {
					if (((Integer) nachbarknotenInfo.get(i)) == zielfeld) {
						indexVonZielknoten = i;
						break sucheGeklicktenKnoten;
					}
				}
			}

			ArrayList<VerkehrsFarben> passendeFarben = new ArrayList<>();
			int j = ++indexVonZielknoten;
			while (nachbarknotenInfo.get(j) instanceof VerkehrsFarben) {
				if (alleKonotenfarben.contains(nachbarknotenInfo.get(j))) {
					passendeFarben.add((VerkehrsFarben) nachbarknotenInfo.get(j));
				}
				j++;
			}

			for (VerkehrsFarben farbe : passendeFarben) {
				fahrButtons.add(new ButtonType(getVerkehrsmittelVonFarbe(farbe)));
			}
		}
		if (startpunkt2schongesetzt) {
			System.out.println("Ich bin in Zeile 775");
		}

		return fahrButtons;

	}

	/**
	 * Jede Spielfigur kann mit ihrer Farbe gesucht werden
	 * 
	 * @param farbe
	 * @return die gesuchte Spielfigur durch ihre Farbe
	 */
	public Button getSpielfigurMitFarbe(SpielerFarben farbe) {
		for (int i = 0; i < spielfiguren.length; i++) {
			if (spielfiguren[i].getId().equalsIgnoreCase(farbe.toString())) {
				return spielfiguren[i];
			}
		}
		return null;
	}

	/**
	 * Setzt alle Felder auf disable
	 */
	public void resetValues() {
		for (int i = 0; i < felder.length; i++) {
			felder[i].setDisable(true);
			felder[i].setStyle("-fx-background-color:transparent;");
		}
	}

	/**
	 * Erzeugt die Farben der Spieler im RGB Farbfeld
	 * 
	 * @param farbeVomSpieler
	 * @return die jeweilige RGB als String
	 */
	private String erzeugeFarbeRGB(SpielerFarben farbeVomSpieler) {
		switch (farbeVomSpieler) {
		case DURCHSICHTIG:
			return "255,255,255";
		case ROT:
			return "204, 0, 0";
		case GRUEN:
			return "0, 153, 0";
		case BLAU:
			return "0, 102, 255";
		case GELB:
			return "255, 204, 0";
		case SCHWARZ:
			return "0,0,0";
		default:
			return "63.0,63.0,63.0";
		}
	}

	/**
	 * Willkommens Benachrichtigung, erklaert kurz das Spiel wenn es weg geklickt
	 * wird werden die Figuren erstellt, erstellt die Buttons muss weggeklickt
	 * werden sonst kann man nicht anfangen
	 */
	@FXML
	private void willkommenPopUp() {
		try {
			Stage popup = new Stage(StageStyle.UNDECORATED);

			popup.initOwner(Main.stage);
			popup.initModality(Modality.APPLICATION_MODAL);
			popup.setTitle("PopupWindow");
			VBox root = (VBox) FXMLLoader.load(getClass().getResource("WillkommenBildschirm.fxml"));
			Scene sc = new Scene(root, 650, 250);
			popup.setScene(sc);
			popup.show();
			popup.setOnHidden(new EventHandler<WindowEvent>() {

				@Override
				public void handle(WindowEvent event) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					erzeugeButtons();
					erstelleSpielfiguren();
					getSpielfigurMitFarbe(SpielerFarben.DURCHSICHTIG).setVisible(false);

				}
			});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Hilfsmethdoe fuer Credits und Hilfe werden als Dialog geoeffnet und koennen
	 * jeweils wieder geschlossen werden
	 * 
	 * @param buttontext
	 * @return AlertBox
	 */

	private Alert erzeugeHilfeUndCredits(String buttontext) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.getButtonTypes().clear();
		alert.getButtonTypes().add(new ButtonType("Danke InformaTiger"));
		alert.initStyle(StageStyle.UNDECORATED);
		alert.initOwner(Main.stage);
		alert.initModality(Modality.APPLICATION_MODAL);
		return alert;
	}

	/**
	 * Hilfsmethode fuer das Credits Dialog Fenster
	 */

	@FXML
	private void creditsPopUp() {
		Alert alert = erzeugeHilfeUndCredits("Beste Gruppe. #SHEESH");
		alert.showAndWait();
	}

	/**
	 * Erstellt das Hilfe Dialog Fenster mit dem gesetzten Text
	 */

	private void hilfePopUp() {

		Alert alert = erzeugeHilfeUndCredits("Danke InformaTiger!");
		alert.setContentText("Hilfe? Kommt sofort:\r\n" + "\r\n" + "Wenn Sie ihre Spielfigur bewegen moechten,\r\n"
				+ "klicken Sie auf eines der markierten Felder\r\n"
				+ "und waehlen anschliessend Ihre Verbindung [Taxi,Bus,Bahn,Schiff(Nur MrX)].");
		alert.show();

	}

	/**
	 * Erzeugt alle Knoten als Buttons mit ihren jeweiligen Verbindungen
	 */
	public void erzeugeButtons() {
		double heightPane = spielfeld.getHeight();
		double widthPane = spielfeld.getWidth();

		double heightSVG = 1080;
		double widthSVG = 1920;

		heightGes = heightPane / heightSVG;
		widthGes = widthPane / widthSVG;
		System.out.println(spielfeld.getHeight() + " | " + spielfeld.getWidth());

		// krasser Move von mir #Lit
		ArrayList<String[]> buttonInfos = new ErzeugeButtons().erzeugeButtons();

		for (int i = 0; i < felder.length; i++) {
			felder[i] = new Button();
			String[] feldInfo = buttonInfos.get(i);
			String id = feldInfo[0];
			Double x = Double.parseDouble(feldInfo[1]) * widthGes;
			Double y = Double.parseDouble(feldInfo[2]) * heightGes;
			felder[i].setId(id);
			felder[i].setLayoutX(x);
			felder[i].setLayoutY(y);
			felder[i].getStyleClass().add("felder");
			felder[i].setDisable(true);
			felder[i].setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					if (methode.equalsIgnoreCase("doppelzug") && !startpunkt2schongesetzt) {
						startpunkt2 = ((Button) event.getSource());
						startpunkt2schongesetzt = true;
					}
					zielfeld = Integer.parseInt(((Button) event.getSource()).getId());
					zielButtonX = ((Button) event.getSource()).getLayoutX();
					zielButtonY = ((Button) event.getSource()).getLayoutY();
					// whoWon((Button) event.getSource());
					buttonIsPressed();

				}
			});

		}
		spielfeld.getChildren().addAll(felder);

	}

	/**
	 * Erstellt die jeweiligen Spielfiguren und gibt ihnen jeweils eine
	 * Startfunktion
	 */
	public void erstelleSpielfiguren() {

		spielfiguren = new Button[controller.usedColors().size()];
		ArrayList<Integer> startfelder = controller.getStartfelder();

		for (int i = 0; i < controller.usedColors().size(); i++) {
			SpielerFarben farbe = controller.usedColors().get(i);
			spielfiguren[i] = new Button();
			spielfiguren[i].setId(farbe.toString());
			spielfiguren[i].setVisible(true);
			spielfiguren[i].setLayoutX(sucheButtonMitID(startfelder.get(i)).getLayoutX() - 5);
			spielfiguren[i].setLayoutY(sucheButtonMitID(startfelder.get(i)).getLayoutY() - 50);
			spielfiguren[i].getStyleClass().addAll("spielerButtons", farbe.toString());
			spielfiguren[i].setTooltip(new Tooltip(controller.spielergebundeneInformationen(farbe)));

		}
		if (Main.dc.getRegelwerkTyp().equalsIgnoreCase("Spezial Sonder Edition")) {

			ArrayList<Integer> zufallsZivilist = new ArrayList<>();
			for (int i = 0; i < 6; i++) {
				zufallsZivilist.add(i);
			}

			int zufall = (int) (Math.random() * 5);
			int zivipos = controller.erhalteZiviPositionen("Zivi1");
			zivilist1.setId(String.valueOf("Zivilist1," + zivipos));
			zivilist1.setVisible(true);
			zivilist1.setLayoutX(sucheButtonMitID(zivipos).getLayoutX() - 5);

			zivilist1.setLayoutY(sucheButtonMitID(zivipos).getLayoutY() - 50);

			zivilist1.getStyleClass().addAll("spielerButtons", "zivilistenfigur" + zufallsZivilist.get(zufall));
			zufallsZivilist.remove(zufall);

			System.out.println("Bims beim 2ten Zivi");
			zufall = (int) (Math.random() * 4);
			zivipos = controller.erhalteZiviPositionen("Zivi2");
			zivilist2.setId(String.valueOf("Zivilist2," + zivipos));
			zivilist2.setVisible(true);
			zivilist2.setLayoutX(sucheButtonMitID(zivipos).getLayoutX() - 5);
			if (zufall == 1) {
				zivilist2.setLayoutY(sucheButtonMitID(zivipos).getLayoutY() - 70);
			} else {
				zivilist2.setLayoutY(sucheButtonMitID(zivipos).getLayoutY() - 50);
			}
			zivilist2.getStyleClass().addAll("spielerButtons", "zivilistenfigur" + zufallsZivilist.get(zufall));

			zufallsZivilist.clear();
			spielfeld.getChildren().add(zivilist1);
			spielfeld.getChildren().add(zivilist2);
		}
		spielfeld.getChildren().addAll(spielfiguren);

		this.spielablauf();
	}

	/**
	 * Zivilist wird ueberschrieben und neu angezeigt
	 * 
	 * @param zivilist
	 */
	public void generiereNeuenZivilisten(Button zivilistAlt) {

		Button zivilist = new Button();
		ArrayList<Integer> zufallsZivilist = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			zufallsZivilist.add(i);
		}
		int zivipos;
		int zufall = (int) (Math.random() * 5);
		if (zivilistAlt.getId().contains("Zivilist1,")) {
			zivipos = controller.erhalteZiviPositionen("Zivi1");
			zivilist.setId(String.valueOf("Zivilist1	," + zivipos));
		} else {
			zivipos = controller.erhalteZiviPositionen("Zivi2");
			zivilist.setId(String.valueOf("Zivilist2," + zivipos));
		}

		zivilist.setVisible(true);
		zivilist.setLayoutX(sucheButtonMitID(zivipos).getLayoutX() - 5);
		if (zufall == 1) {
			zivilist.setLayoutY(sucheButtonMitID(zivipos).getLayoutY() - 70);
		} else {
			zivilist.setLayoutY(sucheButtonMitID(zivipos).getLayoutY() - 50);
		}
		zivilist.getStyleClass().addAll("spielerButtons", "zivilistenfigur" + zufallsZivilist.get(zufall));
		zufallsZivilist.clear();
		spielfeld.getChildren().add(zivilist);
	}

	/**
	 * Bekommt die ID eines Knoten/Buttons das gesucht wird
	 * 
	 * @param id
	 * @return gibt dieses Feld zurueck
	 */
	public Button sucheButtonMitID(int id) {
		for (int i = 0; i < felder.length; i++) {
			if (felder[i].getId().equalsIgnoreCase(String.valueOf(id))) {
				return felder[i];
			}
		}
		return null;
	}
}
