package gui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class WilkommenController implements Initializable{
	/**
	 * FXML Komponente und String
	 */
	@FXML
	TextArea erklaerung;
	@FXML
	Button start;
	@FXML
	Stage window = new Stage();
	
	String erklaerungsText ="Willkommen bei Scotland Yard. \r\n" + 
			"Sie sind die Elite. Ihre Aufgabe ist es Mister X zu finden. \r\n" + 
			"Doch wo hat er sich versteckt? \r\n" + 
			"Gemeinsam finden Sie ihn. \r\n" + 
			"Mister X versucht sein Bestes bis zum Ende des Spiels unentdeckt zu bleiben. \r\n" + 
			"Schnappen koennen Sie ihn nur, wenn Sie auf das gleiche Feld kommen wie er.\r\n" +
			"Viel Erfolg!";
	String startText = "Los geht's!";
	/**
	 * Willkommens Pop Up mit kurzer Spielerklaerung
	 * erst nach dem auf Los geht's geclickt wird kann man 
	 * auf das Main Fenster zugreifen 
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		window.setTitle("Willkommen Fenster");
		erklaerung.setText(erklaerungsText);
		start.setText(startText);
		start.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				try {
					start.getScene().getWindow().hide();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
