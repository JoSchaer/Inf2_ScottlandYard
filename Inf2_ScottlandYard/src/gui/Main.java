package gui;
	
import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	/**
	 * Variablen
	 */
	public static Integer spielerAnzahl;
	public static Stage stage;
	public static DatenCenter dc;
	/**
	 * Started den StartBildschrim 
	 * wenn nicht kommt ein StackTrace mit allen 
	 * Exceptions
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			VBox root = (VBox)FXMLLoader.load(getClass().getResource("StartBildschirm.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("SpielVorbereitungCSS.css").toExternalForm());
			primaryStage.setFullScreen(true);;
			primaryStage.setScene(scene);
			primaryStage.initStyle(StageStyle.UNDECORATED);
			primaryStage.show();
			this.stage = primaryStage;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		dc = new DatenCenter();
		launch(args);
	}
	
}
