package regelwerk;

import java.io.Serializable;
import java.util.ArrayList;

import gameboard.GameBoard;
import gameboard.Kanten;
import gameboard.Knoten;
import spieler.Detektiv;
import spieler.MrX;
import spieler.Spieler;
import zusatsKlassen.VerkehrsFarben;

/**
 * Hier werden alle Regeln geprueft Spielleiter kann hier alles checken das
 * Regelwerk selbst kann nichts
 * 
 * @author celinaronjaschreiner
 *
 */
public class Regelwerk implements Serializable {
	/**
	 * Attribute
	 */
	protected MrX mrx;

	/**
	 * Konstruktor
	 * 
	 * @param mrx
	 */
	public Regelwerk(MrX mrx) {
		this.mrx = mrx;
	}

	/**
	 * Ueberprueft erst ob das Feld belegt ist, dann ob die Strecke die gefahren
	 * werden moechte fahrbar ist, und dann ob mit den vorhanden Karten des Spielers
	 * diese Strecke gefahren werden darf
	 * 
	 * @param aktSpieler
	 * @param startKnoten
	 * @param zielKnoten
	 * @param farbe
	 * @return true wenn der Spieler fahren darf
	 */
	public boolean kannFahren(Spieler aktSpieler, Knoten startKnoten, Knoten zielKnoten, VerkehrsFarben farbe) {
		boolean fahr = false;
		if (!feldBelegt(zielKnoten) || aktSpieler.getAktuellePosition() == this.mrx.getAktuellePosition()) {
			if( moeglicheStrecke(startKnoten, zielKnoten)) {
			  if(moeglichesVerkehrsmittel(startKnoten, zielKnoten, farbe)) {
					fahr = true;
				}
			 }else {
				 return false;
			 }
			 }else {
				 return false;
			}
		return fahr;
	}

	/**
	 * Ueberprueft ob alle Spieler noch Karten haben, und ob dort wo der Spieler
	 * steht er sich noch weiter bewegen kann so bald keiner mehr Karten hat/sich
	 * nicht bewegen kann wird der Wahrheitswert auf false gesetzt
	 * 
	 * @param spielerList
	 * @return Wahrheitswert ob einer der Detektive noch Karten hat
	 */
	public boolean koennenAllFahren(ArrayList<Spieler> spielerList, ArrayList<Knoten> alleKnotenVomGameboard) {
		boolean keinerKann = false;
		for (Spieler einzelnerSpieler : spielerList) {
			if (!hatnochKarten(einzelnerSpieler)) {
				if (!areyouMrX(einzelnerSpieler)) {
					if (!canMove(einzelnerSpieler,
							alleKnotenVomGameboard.get(einzelnerSpieler.getAktuellePosition() - 1)))
						keinerKann = true;
				}
			}
		}
		return keinerKann;
	}

	/**
	 * Ueberprueft ob das Feld belegt ist auf, dass der neue Spieler springen
	 * moechte
	 * 
	 * @return wahrheitswert ob das Feld belegt ist oder nicht wenn ja kommt belegt
	 *         als true zurueck wenn nicht kommt belegt als false zurueck und ist
	 *         somit anfahrbar
	 */
	public boolean feldBelegt(Knoten zielKnoten) {
		boolean belegt = false;
		if (zielKnoten.feldBelegt()) {
			belegt = true;
		} else if (mrx.getAktuellePosition() == zielKnoten.getKnotennummer()) {
			belegt = false;
		}
		return belegt;
	}

	/**
	 * Schaut wer gewonnen hat wenn ueber 22 runden gespielt wurden oder einer der
	 * Detektive keine Karten mehr hat - hat MrX gewonnen Hat einer der Detektive
	 * die gleiche Position wie MrX so haben die Detektive gewonnen
	 * 
	 * 1 = Mrx
	 * 2 = Dete
	 * 0 = keiner
	 * 
	 * @param spielerList
	 * @param rundenzahl
	 * @param detektiv
	 * @return true wenn MrX gewonnen hat
	 */
	public int hasWon(ArrayList<Spieler> spielerList, int rundenzahl, Detektiv detektiv, MrX mrx,
			ArrayList<Knoten> alleKnotenvomGameboard) {
		int noone = 0;
		int mrX = 1;
		int Detektiv = 2;

		if (22 < rundenzahl || koennenAllFahren(spielerList, alleKnotenvomGameboard)) {
			return mrX;
		} else if (mrx.getAktuellePosition() == detektiv.getAktuellePosition()) {
			return Detektiv;
		}
		return noone;
	}

	/**
	 * Schaut ob der Spieler MrX ist, wenn ja ist der Doppelzug erlaubt
	 * 
	 * @param spieler
	 * @return true wenn es MrX ist
	 */
	public boolean doppelzugMrX(Spieler spieler) {
		boolean gestattet = false;
		if (areyouMrX(spieler)) {
			gestattet = true;
		}
		return gestattet;
	}

	/**
	 * Hilfsmethode Schaut ob der Spieler MrX ist oder nicht und gibt den Wahrwert
	 * true zurueck wenn es MrX ist
	 * 
	 * @param spieler
	 * @return true wenn er MrX ist
	 */
	private boolean areyouMrX(Spieler spieler) {
		boolean MrX = false;
		if (spieler == mrx) {
			MrX = true;
		}
		return MrX;
	}

	/**
	 * Hilfsmethode von kannFahren, ueberprueft ob die gewollte Strecke existiert
	 * und befahren werden darf
	 * 
	 * @param startKnoten
	 * @param zielKnoten
	 * @return true wenn die Strecke existiert
	 */
	private boolean moeglicheStrecke(Knoten startKnoten, Knoten zielKnoten) {
		boolean connected = false;
		if (startKnoten.existiertVerbindung(zielKnoten)) {
			connected = true;
		}
		return connected;
	}

	/**
	 * Hilfsmethode von kannFahren, ueberprueft ob die gewollte Strecke mit den
	 * vorhandenen Verkehrskarten gefahren werden kann
	 * 
	 * @param startKnoten
	 * @param zielKnoten
	 * @param farbe
	 * @return true wenn die Karte vorhanden ist
	 */
	private boolean moeglichesVerkehrsmittel(Knoten startKnoten, Knoten zielKnoten, VerkehrsFarben farbe) {
		boolean fahrkarte = false;
		
		if(farbe.equals(VerkehrsFarben.SCHWARZ)) {
			return true;
		}
		for (Kanten aktKante : startKnoten.getMoeglicheFahrlinien(zielKnoten)) {
			if (aktKante.getFarbe().equals(farbe)) {
				fahrkarte = true;
			}
		}
		return fahrkarte;
	}

	/**
	 * Hilfsmethode von koennenAllFahren schaut ob die jeweiligen Spieler noch
	 * moegliche Farben besitzen
	 * 
	 * @param spieler
	 * @param farbe
	 * @return true wenn noch Karten vorhanden
	 */
	private boolean hatnochKarten(Spieler spieler) {
		boolean hatKarten = false;
		for (VerkehrsFarben aktFarbe : spieler.getTaxi()) {
			if (spieler.getTaxi().equals(aktFarbe)) {
				hatKarten = true;
			}
		}
		for (VerkehrsFarben aktFarbe : spieler.getBus()) {
			if (spieler.getBus().equals(aktFarbe)) {
				hatKarten = true;
			}
		}
		for (VerkehrsFarben aktFarbe : spieler.getuBahn()) {
			if (spieler.getuBahn().equals(aktFarbe)) {
				hatKarten = true;
			}
		}
		return hatKarten;
		/*
		 * if(spieler instanceof MrX) { if(((MrX)spieler).getBus().size() >0 ||
		 * ((MrX)spieler).getTaxi().size()>0 || ((MrX)spieler).getuBahn().size()>0 ||
		 * ((MrX)spieler).getBlackTickets().size()>0) { return true; } else { return
		 * false; } } else { if(spieler.getBus().size() >0 || spieler.getTaxi().size()>0
		 * || spieler.getuBahn().size()>0) { return true; } else { return false; } }
		 */

	}

	/**
	 * Hilfsmethode in der geschaut wird ob ein Spieler an dem Knoten an dem er
	 * steht mit den Kanten die er noch besitzt weiter fahren kann oder nicht
	 * 
	 * @param spieler
	 * @param spielerKnoten
	 * @return wahrheits wert true wenn er sich bewegen kann
	 */
	public boolean canMove(Spieler spieler, Knoten spielerKnoten) {
		boolean move = false;
		for (Kanten aktKante : spielerKnoten.getVerkehrslinien()) {
			for (VerkehrsFarben ticketFarbe : spieler.getTaxi()) {
				if (aktKante.getFarbe().equals(ticketFarbe)) {
					move = true;
					return move;
				}
			}
			for (VerkehrsFarben ticketFarbe : spieler.getBus()) {
				if (aktKante.getFarbe().equals(ticketFarbe)) {
					move = true;
					return move;
				}
			}
			for (VerkehrsFarben ticketFarbe : spieler.getuBahn()) {
				if (aktKante.getFarbe().equals(ticketFarbe)) {
					move = true;
					return move;
				}
			}
		}
		return move;
	}
	/**
 	 * 
	 * Prueft, ob der eingegebene Knoten tatsaechlich eine Verbindung mit dem
	 * Startknoten hat 
	 * @param startnummer
	 * @param zielnummer
	 * @param knotenGesamt
	 * @return wahrheitswert true wenn es ein moegliches ziel ist
	 */
	public boolean checkZielnummer(int startnummer, int zielnummer, ArrayList<Knoten> knotenGesamt) {
		for (Knoten aktKnoten : knotenGesamt.get(startnummer-1).getNachbarknoten()) {
			if (aktKnoten.getKnotennummer() == zielnummer) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Prueft, ob die Knoten mit der eingegebenen farbe befahren werden koennen.
	 * 
	 * @param startnummer
	 * @param zielnummer
	 * @param zugfarbe
	 * @return True wenn die Farbe passt. False wenn sie nicht passt
	 */
	public boolean checkVerbindungsfarbe(int startnummer, int zielnummer, VerkehrsFarben zugfarbe, Spieler spielerAnDerReihe, ArrayList<Knoten> knotenGesamt) {
		boolean startKnotenFarbe = false;
		boolean zielKnotenFarbe = false;
		ArrayList<Kanten> startknotenVerkehrslinien = knotenGesamt.get(startnummer-1).getVerkehrslinien();
		ArrayList<Kanten> zielKnotenVerkehrslinien = knotenGesamt.get(startnummer-1).getVerkehrslinien();
		if(spielerAnDerReihe instanceof MrX) {
			for (Kanten kanteStartknoten : startknotenVerkehrslinien) {
				if (kanteStartknoten.getFarbe().equals(zugfarbe) || zugfarbe.equals(VerkehrsFarben.SCHWARZ)) {
					startKnotenFarbe = true;
				}
			}
			for (Kanten kanteZielKnoten : zielKnotenVerkehrslinien) {
				if (kanteZielKnoten.getFarbe().equals(zugfarbe)||zugfarbe.equals(VerkehrsFarben.SCHWARZ) ) {
					zielKnotenFarbe = true;
				}
			}
		}
		
		else {
		for (Kanten kanteStartknoten : startknotenVerkehrslinien) {
			if (kanteStartknoten.getFarbe().equals(zugfarbe)) {
				startKnotenFarbe = true;
			}
		}
		for (Kanten kanteZielKnoten : zielKnotenVerkehrslinien) {
			if (kanteZielKnoten.getFarbe().equals(zugfarbe)) {
				zielKnotenFarbe = true;
			}
		}
		
		}
		if (startKnotenFarbe && zielKnotenFarbe) {
			return true;
		} else {
			return false;
		}
	}
	
}
