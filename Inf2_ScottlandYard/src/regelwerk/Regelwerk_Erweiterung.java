package regelwerk;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import gameboard.Kanten;
import gameboard.Knoten;
import spieler.MrX;
import spielleiter.Spielleiter;

public class Regelwerk_Erweiterung extends Regelwerk implements Serializable {

	public Regelwerk_Erweiterung(MrX mrx) {
		super(mrx);

	}

	/**
	 * Alle Moeglichen Bahnstationen an denen MrX gewesen sein kann Hilfsmethode
	 * fuer den zweiten Hinweis
	 * 
	 * @return alle Bahnstationen
	 */
	private ArrayList<Integer> getAlleBahnStationen() {
		ArrayList<Integer> bahnStationen = new ArrayList<>();
		bahnStationen.add(1);
		bahnStationen.add(46);
		bahnStationen.add(67);
		bahnStationen.add(89);
		bahnStationen.add(13);
		bahnStationen.add(79);
		bahnStationen.add(93);
		bahnStationen.add(111);
		bahnStationen.add(74);
		bahnStationen.add(128);
		bahnStationen.add(140);
		bahnStationen.add(185);
		bahnStationen.add(153);
		bahnStationen.add(163);

		return bahnStationen;
	}

	private static final long serialVersionUID = 1L;

	/**
	 * Schaut in welcher Region des Spielfelds sich MrX gerade aufhaelt
	 * 
	 * @param xAktuellerSpieler
	 * @param yAktuellerSpieler
	 * @param xMrX
	 * @param yMrX
	 * @return die Regionen aufgeteilt in Nord, Sued, Ost und West
	 */
	public String checkRegion(double xAktuellerSpieler, double yAktuellerSpieler, double xMrX, double yMrX) {
		if (xMrX < xAktuellerSpieler && yMrX > yAktuellerSpieler) {
			return "MrX befand sich vor 1 Runde Sued Westlich von dir";
		} else if (xMrX > xAktuellerSpieler && yMrX > yAktuellerSpieler) {
			return "MrX befand sich vor 1 Runde Sued Oestlich von dir";
		} else if (xMrX > xAktuellerSpieler && yMrX < yAktuellerSpieler) {
			return "MrX befand sich vor 1 Runde Nord Oestlich von dir";
		} else {
			return "MrX befand sich vor 1 Runde Nord Westlich von dir";
		}
	}

	/**
	 * Schaut wo MrX vor zwei Runden war und gibt dann die naechste Bahnstation
	 * zurueck
	 * 
	 * @param rundenzahl
	 * @return die Bahnstation in der Naehe von MrX von vor zwei Runden
	 */
	public int checkLokal(int rundenzahl) {

		int posMrX = mrx.getFahrtafel().getAlleFelder().get(rundenzahl - 2);

		int naechsteBahn = naechsteStation(posMrX);

		return naechsteBahn;

	}

	/**
	 * Sucht die genaue Position von MrX von vor drei Runden
	 * 
	 * @param rundenzahl
	 * @return die genau Position von MrX von vor drei Runden
	 */
	public int checkPraezise(int rundenzahl) {

		int genaueMrx;
		ArrayList<Integer> felder = mrx.getFahrtafel().getAlleFelder();
		genaueMrx = felder.get(rundenzahl - 3);

		return genaueMrx;

	}
	
	/**
	 * switch case die durchgeht welcher position welche station naheliegt
	 * @param posMrx
	 * @return nummer der n�chsten station
	 */

	public int naechsteStation(int posMrx) {
		int station = 0;
		switch (posMrx) {
		case 1:
			station = 1;
		case 2:
			station = 1;
			break;
		case 3:
			station = 13;
		case 4:
			station = 13;
		case 5:
			station = 13;
			break;
		case 6:
			station = 89;
		case 7:
			station = 89;
			break;
		case 8:
			station = 1;
		case 9:
			station = 1;
		case 10:
			station = 1;
			break;
		case 11:
			station = 13;
		case 12:
			station = 13;
		case 13:
			station = 13;
		case 14:
			station = 13;
		case 15:
			station = 13;
		case 16:
			station = 13;
			break;
		case 17:
			station = 89;
			break;
		case 18:
			station = 1;
		case 19:
			station = 1;
			break;
		case 20:
			station = 46;
			break;
		case 21:
		case 22:
			station = 13;
		case 23:
			station = 13;
		case 24:
			station = 13;
		case 25:
			station = 13;
		case 26:
			station = 13;
		case 27:
		case 28:
			station = 13;
			break;
		case 29:
			station = 89;
		case 30:
			station = 89;
			break;
		case 31:
			station = 46;
		case 32:
			station = 46;
		case 33:
			station = 46;
		case 34:
			station = 46;
			break;
		case 35:
			station = 79;
			break;
		case 36:
			station = 67;
			break;
		case 37:
			station = 13;
		case 38:
			station = 13;
			break;
		case 39:
			station = 67;
			break;
		case 40:
			station = 89;
		case 41:
			station = 89;
		case 42:
			station = 89;
			break;
		case 43:
			station = 74;
			break;
		case 44:
			station = 46;
		case 45:
			station = 46;
		case 46:
			station = 46;
		case 47:
			station = 46;
			break;
		case 48:
			station = 79;
			break;
		case 49:
			station = 67;
		case 50:
			station = 67;
		case 51:
			station = 67;
		case 52:
			station = 67;
			break;
		case 53:
			station = 89;
		case 54:
			station = 89;
		case 55:
			station = 89;
		case 56:
			station = 89;
			break;
		case 57:
			station = 74;
		case 58:
			station = 74;
		case 59:
			station = 74;
			break;
		case 60:
			station = 46;
		case 61:
			station = 46;
			break;
		case 62:
			station = 79;
		case 63:
			station = 79;
		case 64:
			station = 79;
			break;
		case 65:
			station = 67;
		case 66:
			station = 67;
		case 67:
			station = 67;
		case 68:
			station = 67;
			break;
		case 69:
			station = 89;
		case 70:
			station = 89;
		case 71:
			station = 89;
		case 72:
			station = 89;
			break;
		case 73:
			station = 74;
		case 74:
			station = 74;
		case 75:
			station = 74;
			break;
		case 76:
			station = 79;
		case 77:
			station = 79;
		case 78:
			station = 79;
		case 79:
			station = 79;
		case 80:
			station = 79;
			break;
		case 81:
			station = 67;
		case 82:
			station = 67;
		case 83:
			station = 67;
		case 84:
			station = 67;
		case 85:
			station = 67;
			break;
		case 86:
			station = 89;
		case 87:
			station = 89;
		case 88:
			station = 89;
		case 89:
			station = 89;
		case 90:
			station = 89;
		case 91:
			station = 89;
			break;
		case 92:
			station = 93;
		case 93:
			station = 93;
		case 94:
			station = 93;
		case 95:
			station = 93;
			break;
		case 96:
			station = 79;
		case 97:
			station = 79;
		case 98:
			station = 79;
			break;
		case 99:
			station = 111;
		case 100:
			station = 111;
			break;
		case 101:
			station = 67;
		case 102:
			station = 67;
		case 103:
			station = 67;
			break;
		case 104:
			station = 89;
		case 105:
			station = 89;
		case 106:
			station = 89;
		case 107:
			station = 89;
		case 108:
			station = 89;
			break;
		case 109:
			station = 111;
		case 110:
			station = 111;
		case 111:
			station = 111;
		case 112:
			station = 111;
		case 113:
			station = 111;
			break;
		case 114:
			station = 140;
			break;
		case 115:
			station = 89;
		case 116:
			station = 89;
		case 117:
			station = 89;
			break;
		case 118:
			station = 128;
		case 119:
			station = 128;
			break;
		case 120:
			station = 163;
		case 121:
			station = 163;
			break;
		case 122:
			station = 93;
			break;
		case 123:
			station = 163;
			break;
		case 124:
			station = 111;
		case 125:
			station = 111;
			break;
		case 126:
			station = 140;
		case 127:
			station = 140;
			break;
		case 128:
			station = 128;
		case 129:
			station = 128;
			break;
		case 130:
			station = 111;
		case 131:
			station = 111;
			break;
		case 132:
			station = 140;
		case 133:
			station = 140;
			break;
		case 134:
			station = 128;
		case 135:
			station = 128;
		case 136:
			station = 128;
			break;
		case 137:
			station = 163;
			break;
		case 138:
			station = 111;
			break;
		case 139:
			station = 140;
		case 140:
			station = 140;
			break;
		case 141:
			station = 128;
		case 142:
			station = 128;
		case 143:
			station = 128;
			break;
		case 144:
			station = 163;
		case 145:
			station = 163;
		case 146:
			station = 163;
		case 147:
			station = 163;
		case 148:
			station = 163;
		case 149:
			station = 163;
			break;
		case 150:
			station = 153;
		case 151:
			station = 153;
		case 152:
			station = 153;
		case 153:
			station = 153;
			break;
		case 154:
			station = 140;
		case 155:
			station = 140;
		case 156:
			station = 140;
		case 157:
			station = 140;
			break;
		case 158:
			station = 128;
			break;
		case 159:
			station = 185;
			break;
		case 160:
			station = 128;
		case 161:
			station = 128;
		case 162:
			station = 128;
			break;
		case 163:
			station = 163;
		case 164:
			station = 163;
		case 165:
			station = 163;
			break;
		case 166:
			station = 153;
		case 167:
			station = 153;
			break;
		case 168:
			station = 185;
		case 169:
			station = 185;
		case 170:
			station = 185;
			break;
		case 171:
			station = 128;
		case 172:
			station = 128;
		case 173:
			station = 128;
		case 174:
			station = 128;
		case 175:
			station = 128;
			break;
		case 176:
			station = 163;
		case 177:
			station = 163;
		case 178:
			station = 163;
		case 179:
			station = 163;
		case 180:
			station = 163;
			break;
		case 181:
			station = 153;
		case 182:
			station = 153;
		case 183:
			station = 153;
			break;
		case 184:
			station = 185;
		case 185:
			station = 185;
		case 186:
			station = 185;
		case 187:
			station = 185;
			break;
		case 188:
			station = 128;
			break;
		case 189:
			station = 163;
		case 190:
			station = 163;
		case 191:
			station = 163;
		case 192:
			station = 163;
			break;
		case 193:
			station = 153;
			break;
		case 194:
			station = 163;
			break;
		case 195:
			station = 153;
			break;
		case 196:
			station = 185;
		case 197:
			station = 185;
		case 198:
			station = 185;
			break;
		case 199:
			station = 128;
			break;
		}
		return station;
	}

}
