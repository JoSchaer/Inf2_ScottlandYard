
package gameboard;

import java.io.Serializable;
import java.util.ArrayList;

import zusatsKlassen.Ringpuffer;
import zusatsKlassen.VerkehrsFarben;

/**
 * Knoten sind die Haltestellen an denen die Spieler stehen bleiben koennen, es
 * braucht immer zwei Knoten fuer eine Kante
 * 
 * @author Jonathan
 *
 */
public class Knoten implements Serializable{
	/**
	 * Attribute
	 */
	private boolean belegt;
	private int knotenNummer;
	private ArrayList<Knoten> nachbarKnoten;
	private ArrayList<VerkehrsFarben> fahrbareLinien;
	private ArrayList<Kanten> verkehrslinien;
	
	/**
	 * Ein Knoten hat immer eine Zahl durch die er gefunden werden kann dabei kennt
	 * er seine Nachbarknoten, kennt die Linien die gefahren werden koennen, welche
	 * Verkehrsmittel genutzt werden muessen, und ob er belegt ist von einem Spieler
	 * oder nicht
	 * 
	 * @param id
	 */
	public Knoten(int id) {
		nachbarKnoten = new ArrayList<>();
		fahrbareLinien = new ArrayList<>();
		verkehrslinien = new ArrayList<>();
		belegt = false;
		this.knotenNummer = id;

	}

	/**
	 * Wenn ein Spieler auf diesem Knoten/Feld steht wird das Feld als belegt
	 * gezeigt/gemerkt
	 * 
	 * @param ja_nein
	 */
	public void setKnotenBelegt(boolean ja_nein) {
		
		this.belegt = ja_nein;
	}

	/**
	 * gibt dem Knoten eine Farbe fuer die Arten von Verkehrsmittel die hier
	 * gefahren werden koennen erst wird geschauen ob es bereits vorhanden ist, wenn
	 * nicht wird es neu verstellt mit der entsprechenden Farbe
	 * 
	 * @param neueFrbe
	 */
	public void addKnotenfarbe(VerkehrsFarben neueFrbe) {
		if (!fahrbareLinien.contains(neueFrbe)) {
			fahrbareLinien.add(neueFrbe);
		}
	}

	/**
	 * Fuegt die Verkehrsmittel hinzu wenn die Verkehrslinie noch nicht vorhanden
	 * ist
	 * 
	 * @param neueVerkehrslinie
	 */
	public void addVerkehrslinien(Kanten neueVerkehrslinie) {
		if (!verkehrslinien.contains(neueVerkehrslinie))
			verkehrslinien.add(neueVerkehrslinie);
	}

	/**
	 * bei einem neuen Knoten wird ein Nachbarknoten hinzugefuegt sollte dieser noch
	 * nicht vorhanden sein
	 * 
	 * @param neuerKnoten neuer Knoten der hinzugefuegt wird
	 */
	public void addKnotenBeiAddNachbarknoten(Knoten neuerKnoten) {
		if (!nachbarKnoten.contains(neuerKnoten))
			nachbarKnoten.add(neuerKnoten);
	}

	/**
	 * Setzt ein Feld auf Belegt wenn dort ein Spieler ist
	 * 
	 * @return Feld belegt
	 */
	public boolean feldBelegt() {
		return belegt;
	}

	/**
	 * Getter
	 * 
	 * @return die Werte zur Erzeugung von einem Knoten
	 */
	public int getKnotennummer() {
		return this.knotenNummer;
	}

	public ArrayList<VerkehrsFarben> getFarben() {
		return this.fahrbareLinien;
	}

	public ArrayList<Kanten> getVerkehrslinien() {
		return this.verkehrslinien;
	}

	public ArrayList<Knoten> getNachbarknoten() {
		return this.nachbarKnoten;
	}

	/**
	 * Gibt dem neu erstellten Knoten einen Nachbarknoten und fuegt diesem auch eine
	 * Farbe hinzu jeweilige erst Pruefung ob diese schon existieren
	 * 
	 * @param neuerKnoten
	 * @param verbindungsfarbe
	 */
	public void addNachbarKnoten(Knoten neuerKnoten, VerkehrsFarben verbindungsfarbe) {
		if (!nachbarKnoten.contains(neuerKnoten)) {
			nachbarKnoten.add(neuerKnoten);
		}
		neuerKnoten.addKnotenfarbe(verbindungsfarbe);
		neuerKnoten.addKnotenBeiAddNachbarknoten(this);

		Kanten neueKante = new Kanten(this, neuerKnoten, verbindungsfarbe);
		if (!verkehrslinien.contains(neueKante)) {
			verkehrslinien.add(neueKante);
		}
		neuerKnoten.addVerkehrslinien(neueKante);
	}

	/**
	 * Holt alle Ids der Nachbarknoten als String
	 * 
	 * @return ID des Knotens als String
	 */
	private String getnachbarIDs() {
		String back = "";
		for (Knoten nk : nachbarKnoten) {
			back += nk.getKnotennummer() + ", ";
		}
		return back;
	}
	/**
	 * Alle moeglichen Verbindungen von MrX
	 * @return alle moeglichen Verkehrsmittel die er nehmen kann
	 */
	private String getVerbindungsFarbenMRX() {
		String farbenUndVerkehrsmittel = "";
		for (VerkehrsFarben aktKante: this.fahrbareLinien) {
			if(aktKante.equals(VerkehrsFarben.WEISS)) {
				farbenUndVerkehrsmittel += "Sie koennen mit der Farbe WEISS - Taxi fahren.\n";
			}
			else if(aktKante.equals(VerkehrsFarben.GRUEN)) {
				farbenUndVerkehrsmittel += "Sie koennen mit der Farbe GRUEN - Bus fahren.\n";
			}
			else if(aktKante.equals(VerkehrsFarben.ROT)) {
				farbenUndVerkehrsmittel += "Sie koennen mit der Farbe ROT - U-Bahn fahren.\n";
			}
			else if(aktKante.equals(VerkehrsFarben.SCHWARZ)) {
				farbenUndVerkehrsmittel += "Sie koennen mit der Farbe SCHWARZ - Boot fahren, wenn Sie ein blackticket einsetzen.\n";
			}
		}
		return farbenUndVerkehrsmittel;
	}

	/**
	 * Ueberpruefung ob eine Verbindung zwischen einem Knoten existiert
	 * 
	 * @param knoten2
	 * @return Wahrheitswert true wenn die Verbindung existiert
	 */
	public boolean existiertVerbindung(Knoten knoten2) {
		if (nachbarKnoten.contains(knoten2)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * schaut nach den moeglichen Verkehrslinien des Knotens
	 * 
	 * @param knoten2
	 * @return mit welchen Verkehrsmitteln es moeglich ist zu fahren
	 */
	public ArrayList<Kanten> getMoeglicheFahrlinien(Knoten knoten2) {
		ArrayList<Kanten> moeglicheLinien = new ArrayList<>();
		for (Kanten kante : this.verkehrslinien) {
			if (kante.getAlleKnoten().contains(this) && kante.getAlleKnoten().contains(knoten2)) {
				moeglicheLinien.add(kante);
			}
		}
		return moeglicheLinien;

	}

	/**
	 * Gibt in einem String die Knoten nummer, den Nachbarknoten, die moeglichen
	 * Verkehrsmittel, sowie die ID des Nachbars und die Verkehrslinie
	 */
	
	public Object[] moveInfoMrX() {
		ArrayList<Object> nachbarknotenInfos = new ArrayList<>();
		ArrayList<Object> startknotenInfos = new ArrayList<>();
		Object[] rueckgabe = {startknotenInfos,nachbarknotenInfos};
		
		/*
		 * ID des Startknotens und alle seine Farben werden der Liste Hinzugef�gt
		 */
		startknotenInfos.add(this.getKnotennummer());
		startknotenInfos.addAll(fahrbareLinien);
		
		/*
		 * Alle Ids und Farben der Nachbarknoten werden hinzugef�gt
		 */
		for(Knoten aktKnoten : this.getNachbarknoten()) {
			 nachbarknotenInfos.add(aktKnoten.getKnotennummer());
			for(VerkehrsFarben aktFarbe: aktKnoten.getFarben()) {
				if(this.fahrbareLinien.contains(aktFarbe)) {
					nachbarknotenInfos.add(aktFarbe);
				}
			}
		}
		return rueckgabe;
	}
	/**
	 * String Ausgabe ueberschrieben von Object
	 */
	
	public Object[] moveInfo() {
		ArrayList<Object> nachbarknotenInfos = new ArrayList<>();
		ArrayList<Object> startknotenInfos = new ArrayList<>();
		Object[] rueckgabe = {startknotenInfos,nachbarknotenInfos};
		
		/*
		 * ID des Startknotens und alle seine Farben werden der Liste Hinzugef�gt
		 */
		startknotenInfos.add(this.getKnotennummer());
		startknotenInfos.addAll(fahrbareLinien);
		
		/*
		 * Alle Ids und Farben der Nachbarknoten werden hinzugef�gt
		 */
		for(Knoten aktKnoten : this.getNachbarknoten()) {
			 nachbarknotenInfos.add(aktKnoten.getKnotennummer());
			for(VerkehrsFarben aktFarbe: aktKnoten.getFarben()) {
				if(this.fahrbareLinien.contains(aktFarbe)) {
					nachbarknotenInfos.add(aktFarbe);
				}
			}
		}
		return rueckgabe;
	
	}
	/**
	 * String Ausgabe fuer MrX, er einen Doppelzug macht
	 * @return Textuelle Ausgabe
	 */
	public Object[] doppelzugInfo() {
		
		ArrayList<Object> nachbarknotenInfos = new ArrayList<>();
		ArrayList<Object> startknotenInfos = new ArrayList<>();
		ArrayList<Object[]>informationenZumZweitenZug = new ArrayList<>();
		Object[] rueckgabe = {startknotenInfos,nachbarknotenInfos,informationenZumZweitenZug};
		
		/*
		 * ID des Startknotens und alle seine Farben werden der Liste Hinzugef�gt
		 */
		startknotenInfos.add(this.getKnotennummer());
		startknotenInfos.addAll(fahrbareLinien);
		
		/*
		 * Alle Ids und Farben der Nachbarknoten werden hinzugef�gt
		 */
		for(Knoten aktKnoten : this.getNachbarknoten()) {
			 nachbarknotenInfos.add(aktKnoten.getKnotennummer());
			for(VerkehrsFarben aktFarbe: aktKnoten.getFarben()) {
				if(this.fahrbareLinien.contains(aktFarbe)) {
					nachbarknotenInfos.add(aktFarbe);
				}
			}
			informationenZumZweitenZug.add(moveOptions(aktKnoten));
		}
		return rueckgabe;
	}
	
	private Object[] moveOptions(Knoten knoten){
		ArrayList<Object> nachbarknotenInfos = new ArrayList<>();
		ArrayList<Object> startknotenInfos = new ArrayList<>();
		Object[] rueckgabe = {startknotenInfos,nachbarknotenInfos};
		
		/*
		 * ID des Startknotens und alle seine Farben werden der Liste Hinzugef�gt
		 */
		startknotenInfos.add(knoten.getKnotennummer());
		startknotenInfos.addAll(knoten.getFarben());
		
		/*
		 * Alle Ids und Farben der Nachbarknoten werden hinzugef�gt
		 */
		for(Knoten aktKnoten : knoten.getNachbarknoten()) {
			 nachbarknotenInfos.add(aktKnoten.getKnotennummer());
			for(VerkehrsFarben aktFarbe: aktKnoten.getFarben()) {
				if(this.fahrbareLinien.contains(aktFarbe)) {
					nachbarknotenInfos.add(aktFarbe);
				}
			}
		}
		return rueckgabe;
	}
}
