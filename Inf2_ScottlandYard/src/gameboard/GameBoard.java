package gameboard;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import vars.GameBoardLoader;
import vars.iDataAccess;
import zusatsKlassen.VerkehrsFarben;
/**
 * Erstellung des Gameboards mit Knoten und Kanten
 * @author Jonathan
 *
 */
public class GameBoard implements Serializable{
	/**
	 * Attribute
	 */
	private ArrayList<Knoten> knoten = new ArrayList<>();
	private iDataAccess dataAccess = new GameBoardLoader();
	/**
	 * default Konstruktor
	 */
	public GameBoard() {
		erstelleKnoten();
	}
	/**
	 * Durch geht die CSV und teilt sie in die einzelnen Knoten, dabei 
	 * hat am Ende jeder Knoten eine Zahl und weis mit welchem Verkehrsmittel
	 * er erreichbar ist z.B. T = Taxi. 
	 * Es werden die Exceptions abgefangen: wenn die CSV Datei nicht gefunden 
	 * wird und wenn ein InputOutput Fehler auftritt
	 */
	private void erstelleKnoten() {
		try {
			// komplette Knoten in einem String
			String alleKnoten = (String) dataAccess.load("Felder.csv").toString();

			// Es werden die einzelnen Stringzeilen in einem String Array gespeichert
			String[] zeilen = alleKnoten.split("\n");

			for (String einzelneZeile : zeilen) {
				// Einzelne Knoten werden mit IDs
				knoten.add(new Knoten(Integer.parseInt(einzelneZeile.split(":")[0])));
			}
			 // Jeder einzelne Knoten bekommt nun seine Nachbarknoten
			
			for (Knoten aktKnoten : knoten) {
				// Es werden nur die Daten aus der Zeile mit der Nummer des Knotens benoetigt
				String[] datenDerEinzelnenZeile = zeilen[aktKnoten.getKnotennummer() - 1].split(":");
				// An index 1 sind die Verkehrsanbindungen diese m�ssen wieder gesplittet werden
				String verkehrsdaten = datenDerEinzelnenZeile[1];

				/*
				 * Verkehrsdaten werden in einzelne Verkehrsmittel unterteilt Alle Punkte
				 * nacheinander Jeder Index enthaelt Daten eines einzelnen Punktes
				 */

				/*
				 * jeder index von alleVerkehrsmittelProPunkt sind alle erkehrsverbindungen
				 * eines einzelnen Punktes.
				 */
				String[] alleVerkehrsmittelProPunkt = verkehrsdaten.split("\n");
				/*
				 * Man geht nun ueber jeden Index, also ueber jeden Punkt und splittet die
				 * einzelnen verkehrsmittel wiederum
				 */
				for (String einzelnePunktVerbindungen : alleVerkehrsmittelProPunkt) {
					String[] einzelneVerkehrsmittelDesPunktes = einzelnePunktVerbindungen.split(";");
					// Beachten, das manche Punkte nur mit nem Taxi zufrieden sind
					// Je nach Laenge des Strings gibt es mehrere Verkehrsmittel
					for (int i = 0; i < einzelneVerkehrsmittelDesPunktes.length; i++) {
						String eindlichEinzelnesVerkehrsmittel = einzelneVerkehrsmittelDesPunktes[i];
						String[] knotenIDsAusVerkehrsmittelMitTyp = eindlichEinzelnesVerkehrsmittel.split("=");
						 // Wenn der Strig ein T enthaelt werden alle Taxiverbindungen angelegt
						if (knotenIDsAusVerkehrsmittelMitTyp[0].equals("T")) {
							String[] taxiIds = knotenIDsAusVerkehrsmittelMitTyp[1].split(",");
							for (int j = 0; j < taxiIds.length; j++) {
								aktKnoten.addNachbarKnoten(sucheKnotenMitID(Integer.parseInt(taxiIds[j])),
										VerkehrsFarben.WEISS);
							}
						}
						 //Enthaeltder String ein B werden alle Bus Verbindungen erstellt
						else if (knotenIDsAusVerkehrsmittelMitTyp[0].equals("B")) {
							String[] busIds = knotenIDsAusVerkehrsmittelMitTyp[1].split(",");
							for (int j = 0; j < busIds.length; j++) {
								aktKnoten.addNachbarKnoten(sucheKnotenMitID(Integer.parseInt(busIds[j])),
										VerkehrsFarben.GRUEN);
							}
						} 
						 //Enthaeltder String ein S werden alle U-Bahn Verbindungen erstellt
						else if (knotenIDsAusVerkehrsmittelMitTyp[0].equals("S")) {
							String[] ubahnIds = knotenIDsAusVerkehrsmittelMitTyp[1].split(",");
							for (int j = 0; j < ubahnIds.length; j++) {
								aktKnoten.addNachbarKnoten(sucheKnotenMitID(Integer.parseInt(ubahnIds[j])),
										VerkehrsFarben.ROT);
							}
						} 
						 // Enthaeltder String ein F werden alle Schiff Verbindungen erstellt
							else if (knotenIDsAusVerkehrsmittelMitTyp[0].equals("F")) {
							String[] ferryIds = knotenIDsAusVerkehrsmittelMitTyp[1].split(",");
							for (int j = 0; j < ferryIds.length; j++) {
								aktKnoten.addNachbarKnoten(sucheKnotenMitID(Integer.parseInt(ferryIds[j])),
										VerkehrsFarben.SCHWARZ);
							}
						}
					}

				}
			}

		} catch (FileNotFoundException e) {
			System.out.println("Datei konnte leider nicht gefunden werden");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Es wird die ID uebergehen nach der gesucht werden soll 
	 * @param knotenID
	 * @return Knoten mit der gegeben ID
	 */
	public Knoten sucheKnotenMitID(int knotenID) {
		return knoten.get(knotenID - 1);
	}
	/**
	 * gibt alle moeglichen Knoten 
	 * @return Knoten im Spiel
	 */
	public ArrayList<Knoten> getAlleKnoten() {
		return this.knoten;
	}
	
	
}