package gameboard;

import java.util.ArrayList;

import zusatsKlassen.SpielerFarben;

/**
 * Main Methode zum Test des GameLoaders
 * @author Jonathan
 *
 */
public class TestGameLoaderMain {

	public static void main(String[] args) {
		GameBoard gb = new GameBoard();

		SpielerFarben[] spielerFarben = SpielerFarben.values();
		ArrayList<SpielerFarben>alleFarbenInArrayList = new ArrayList<>();
		for(int g = 0; g<spielerFarben.length;g++) {
			alleFarbenInArrayList.add(spielerFarben[g]);
		}
		for(SpielerFarben farbe : alleFarbenInArrayList) {
			System.out.println(farbe.toString());
		}
	}

}
