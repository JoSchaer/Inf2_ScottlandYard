package gameboard;

import java.io.Serializable;
import java.util.ArrayList;

import zusatsKlassen.VerkehrsFarben;

/**
 * Erstellt Kanten
 * eine Kante besteht immer zwischen zwei Knoten 
 * eine Kante kann immer nur von bestimmten Verkehrsmitteln gefahren 
 * werden
 * @author Jonathan
 *
 */

public class Kanten implements Serializable {
	/**
	 * Attribute
	 */
	private ArrayList<Knoten> knotenPunkte;
	private VerkehrsFarben farbe;
	private int weight;
	/**
	 * Eine Kante besteht immer zwischen zwei Knoten und bekommt eine Farbe
	 * fuer die Verkehrsmittel mit denen man an dieser Kante entlang fahren 
	 * kann - Kante wird erstellt wenn alle drei Werte gegeben sind
	 * @param knoten1
	 * @param knoten2
	 * @param farbe
	 */
	public Kanten(Knoten knoten1, Knoten knoten2, VerkehrsFarben farbe) {
		knotenPunkte = new ArrayList<>();
		knotenPunkte.add(knoten1);
		knotenPunkte.add(knoten2);
		this.farbe = farbe;
		// je nach welcher Verkehrsverbindung bekommt die Kante ein Gewicht
		if(farbe.equals(VerkehrsFarben.WEISS)){
			setWeight(3);
		}else if(farbe.equals(VerkehrsFarben.GRUEN)){
			setWeight(2);
		}else if(farbe.equals(VerkehrsFarben.ROT)){
			setWeight(1);
		}else if(farbe.equals(VerkehrsFarben.SCHWARZ)){
			setWeight(4); // weiss noch nicht ob des schlau ist
		}
	}
	public int getWeight() {
		return weight;
	}
	//GValue for the AStar 
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	/**
	 * @return gibt jeweils den Knoten zurueck zwischen denen die Kante bestehen soll
	 */
	public Knoten getKnoten1() {
		return knotenPunkte.get(0);
	}

	public Knoten getKnoten2() {
		return knotenPunkte.get(1);
	}
	/** 
	 * @return Farbe des Verkehrsmittel mit dem gefahren werden darf
	 */
	public VerkehrsFarben getFarbe() {
		return farbe;
	}
	/**
	 * @return Punkte aller Knoten
	 */
	public ArrayList<Knoten> getAlleKnoten(){
		return this.knotenPunkte;
	}
}
